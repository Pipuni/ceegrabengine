﻿using System;

namespace ConstantsDataHandler
{
    public class Constants
    {
        public const String CON_OWNER_NAME = "Secvision Private Limited. ";
        public const String CON_OWNER_SHORTNAME = "Secvision. ";
        public const String CON_SYSTEM_NAME = "Hotel Booking System. ";

        /* LOGIN STATUS */
        public static String CON_LOGSTS_LOGOUT = "0";
        public static String CON_LOGSTS_LOGIN = "1";
        /* END LOGIN STATUS */

        /* Session */
        public static String SYS_SESSION_LOCSYSDATE = "KEY-LOCSYSDATE";
        public static String SYS_SESSION_LOGSTS = "KEY-LOGSTS";
        public static String SYS_SESSION_ERROR = "KEY-ERROR";
        public static String SYS_SESSION_LOCATIONCODE = "KEY-LOCATIONCODE";
        public static String SYS_SESSION_COMPANYCODE = "KEY-COMPANYCODE";
        public static String SYS_SESSION_USERINPUTINFO = "KEY-USERINPUTINFO";
        public static String SYS_SESSION_SELECTEDRMTB = "KEY-SELECTEDRMTB";
        public static String SYS_SESSION_PREFCURRENCY = "KEY-PREFCURRENCY";
        public static String SYS_SESSION_SELECTEDEXOPTRMTB = "KEY-SELECTEDEXOPTRMTB";
        public static String SYS_SESSION_ROOMTB = "KEY-ROOMTB";
        public static String SYS_SESSION_PICKUPTB = "KEY-PICKUPTB";
        public static String SYS_SESSION_CUSTRESTB = "KEY-CUSTRESTB";
        public static String SYS_SESSION_TAXTABLE = "KEY-TAXTB";

        /* End Session */

        /* PAGE PROPERTY INFORMATION */

        public const String CON_PAGE_TITLE = "PT";
        public const String CON_PAGE_REMARK = "PR";
        public const String CON_PAGE_NOTIFICATION = "PN";
        public const String CON_PAGE_INFORMATION = "PS";

        public const String CON_TOP_IMAGEFILE = "T";
        public const String CON_LEFT_IMAGEFILE = "L";
        public const String CON_RIGHT_IMAGEFILE = "R";
        public const String CON_BOTTOM_IMAGEFILE = "B";

        public const String CON_DOC_EMAIL = "BRESMAIL";
        public const String CON_DOC_MESSAGE = "BRESMAG";
        public const String CON_DOC_RESCONFIRMATION = "MRESCON";

        /**/

        public const int CON_HEIGHT = 15;
        public const int CON_WIDTH = 15;

        public const String CON_STATUS_ACTIVE = "A";
        public const String CON_STATUS_PENDING = "P";
        public const String CON_STATUS_ISRESERVED = "R";
        public const String CON_STATUS_SUBMITTED = "S";
        public const String CON_STATUS_REJECTED = "R";
        public const String CON_STATUS_DELETED = "D";
        public const String CON_STATUS_APPROVED = "A";
        public const String CON_STATUS_ISPAID = "P";

        public const String CON_YES = "Y";
        public const String CON_NO = "N";

        public static String MESSAGE_WARNING = "3";
        public static String MESSAGE_INFO = "2";
        public static String MESSAGE_SUCCESS = "1";
        public static String MESSAGE_ERROR = "0";

        public static String CON_MESSAGE_STRING = "";
        public static String CON_TEXT_EXIT = "Exit";

        public static String CON_NULL_TEXT = "";
        public static String CON_NULL_VALUE = "";

        public const Int32 CON_KILOBYTES = 1024;
        public const Int32 CON_MEGABYTES = 1048576;
        public const Int32 CON_MAX_FILE_SIZE = 209715200;

        public const String CON_STYLESHEET_PATH = "~/Resources/Styles/";
        public const String CON_ROOMIMAGE_PATH = "~/Resources/RoomImages/";
        public const String CON_LOGO_PATH = "~/Resources/Logo/";
        public const String CON_DOC_PATH = "~/Resources/Docs/";
        public const String CON_EXIT_URL = "/Main/logout.aspx";
        public const String CON_ERRORLOG_PATH = "~/Resources/Errorlog/";

        public static String CON_TAX_GROSS = "G";
        public static String CON_TAX_NETT = "N";
        public static String CON_TAX_GROSS_TYPE = "Gross";
        public static String CON_TT_TAX_AMOUNT = "Tax Amount";
        public static String CON_TAX_NETT_TYPE = "Nett";
        public static String CON_TAX_NETT_RW = "Nett Payable";

        public static String CON_RESCON_FILENAME = "RRESERVATION_CONFIRMATION_";
        public static String CON_RESINVOICE_FILENAME = "INVOICE_DETAILS_";
        public static int CON_ITRANS_TYPE = 2;
    }
}