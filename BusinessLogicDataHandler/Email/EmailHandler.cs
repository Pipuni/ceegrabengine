﻿using System;
using System.Net.Mail;
using System.Net.Mime;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;

namespace BusinessLogicDataHandler.Email
{
    public class EmailHandler
    {
        public Boolean SendReservationConfirmationEmail(string RESCODE, string SPDFRESFILENAME, string SPDFINVOICE, string EMAILTEXT, string LOCATION, string GUESTNAME, string GUESTEMAIL)
        {
            Boolean blSend = false;
            string sEmailBody = "";

            try
            {
                MailAddress mailAddressTo;
                MailAddress mailAddressFrom;
                sEmailBody = sEmailBody + "<br/>";
                sEmailBody = sEmailBody + "Hi " + GUESTNAME.ToString();
                sEmailBody = sEmailBody + "<br/>";
                sEmailBody = sEmailBody + "<br/>";
                sEmailBody = sEmailBody + " Your Reservation Code is " + RESCODE;
                sEmailBody = sEmailBody + "<br/>";
                sEmailBody = sEmailBody + "<br/>";
                sEmailBody = sEmailBody + EMAILTEXT.ToString();
                sEmailBody = sEmailBody + "<br/>";

                string sMailSubject = "Reservation Confirmation " + LOCATION;
                string sSenderName = LOCATION.ToString();
                string sSenderEmail = "support@secdenv.biz";
                mailAddressFrom = new MailAddress(sSenderEmail, sSenderName);
                mailAddressTo = new MailAddress(GUESTEMAIL);

                MailMessage mailMessage = new MailMessage(mailAddressFrom, mailAddressTo);
                Attachment attachpdfdata = new Attachment(SPDFINVOICE, MediaTypeNames.Application.Octet);
                Attachment attachpdfcondata = new Attachment(SPDFRESFILENAME, MediaTypeNames.Application.Octet);
                mailMessage.Attachments.Add(attachpdfdata);
                mailMessage.Attachments.Add(attachpdfcondata);
                //mailMessage.CC.Add(new MailAddress(RECEIVEREMAIL));
                mailMessage.Subject = sMailSubject;
                mailMessage.Body = sEmailBody;
                mailMessage.IsBodyHtml = true;
                //SmtpClient smtpClient = new SmtpClient("10.100.101.28", 25);
                SmtpClient smtpClient = new SmtpClient("secdenv.biz");

                smtpClient.Credentials = new System.Net.NetworkCredential()
                {
                    UserName = "noreply@secdenv.biz",
                    Password = "sam@911"
                };

                //smtpClient.EnableSsl = true;

                System.Net.ServicePointManager.ServerCertificateValidationCallback = delegate (object s, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors) { return true; };
                smtpClient.Send(mailMessage);
                blSend = true;
            }
            catch (Exception ex)
            {
                blSend = false;
                throw ex;
            }
            finally
            {
            }

            return blSend;
        }
    }
}