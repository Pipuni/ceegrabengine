﻿using BusinessLogicDataHandler.Clog;
using ConnectionDataHandler;
using ConstantsDataHandler;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Text;

namespace BusinessLogicDataHandler.Reservation
{
    public class ReservationDataHandler 
    {
        public string EnryptString(string password)
        {
            string strmsg = string.Empty;
            byte[] encode = new byte[password.Length];
            encode = Encoding.UTF8.GetBytes(password);
            strmsg = Convert.ToBase64String(encode);
            return strmsg;
        }

        public string DecryptString(string encryptpwd)
        {
            string decryptpwd = string.Empty;
            UTF8Encoding encodepwd = new UTF8Encoding();
            Decoder Decode = encodepwd.GetDecoder();
            byte[] todecode_byte = Convert.FromBase64String(encryptpwd);
            int charCount = Decode.GetCharCount(todecode_byte, 0, todecode_byte.Length);
            char[] decoded_char = new char[charCount];
            Decode.GetChars(todecode_byte, 0, todecode_byte.Length, decoded_char, 0);
            decryptpwd = new String(decoded_char);
            return decryptpwd;
        }

        public DataTable getTaxSeq(string COMPCODE, string LOCCODE, string TAXPGCODE)
        {
            DataTable SQLDT = new DataTable();

            try
            {

                using (SqlConnection con = new SqlConnection(DataAccess.GetConnectionString()))
                {
                    if (con.State != ConnectionState.Open) { con.Open(); }
                    string SQLString = @"SELECT TTM.TAXPGCODE,TTM.COMPCODE,TTM.LOCCODE,TTM.TAXCODE,TTM.SEQ , TT.TAXRATE, TT.TAXTYPE, TT.TAXAPPLIEDFOR from GLTAXTYPEMAP TTM" +
                                       " LEFT JOIN GLTAXTYPE TT ON TTM.TAXCODE = TT.TAXCODE WHERE TTM.COMPCODE =@COMPCODE AND TTM.LOCCODE = @LOCCODE AND TTM.TAXPGCODE =@TAXPGCODE AND TT.STS = @sts" +
                                       " ORDER BY TTM.SEQ";
                    using (SqlCommand SQLCommand = new SqlCommand(SQLString, con))
                    {
                        using (SqlDataAdapter sqlda = new SqlDataAdapter(SQLCommand))
                        {

                            SQLCommand.Parameters.AddWithValue("@COMPCODE", COMPCODE);
                            SQLCommand.Parameters.AddWithValue("@LOCCODE", LOCCODE);
                            SQLCommand.Parameters.AddWithValue("@TAXPGCODE", TAXPGCODE);
                            SQLCommand.Parameters.AddWithValue("@sts", Constants.CON_STATUS_ACTIVE);
                            sqlda.Fill(SQLDT);
                        }
                    }
                }

                return SQLDT;

            }
            catch (Exception ex)
            {
                CeegralogDataHandler.LogError(ex);
                throw ex;
            }
        }

        public DataTable calculateTaxAmount(string COMPCODE, string LOCCODE, string TAXPGCODE, double GROSSAMNTWITHOUTTAX, double GROSSAMNTWITHTAX)
        {
            double TOTALAMOUNT = GROSSAMNTWITHOUTTAX;
            DataTable taxseqdt = new DataTable();
            try
            {
                taxseqdt = getTaxSeq(COMPCODE, LOCCODE, TAXPGCODE);
                taxseqdt.Columns.Add("TAXAMOUNT", typeof(double));
                DataRow taxseqrw = taxseqdt.NewRow();
                taxseqrw["COMPCODE"] = COMPCODE;
                taxseqrw["LOCCODE"] = LOCCODE;
                taxseqrw["TAXTYPE"] = Constants.CON_TAX_GROSS_TYPE;
                taxseqrw["TAXAMOUNT"] = GROSSAMNTWITHOUTTAX + GROSSAMNTWITHTAX;
                taxseqdt.AcceptChanges();
                foreach (DataRow dataRow in taxseqdt.Rows)
                {
                    if (dataRow["TAXAPPLIEDFOR"].Equals(Constants.CON_TAX_GROSS))
                    {
                        double taxamount = GROSSAMNTWITHOUTTAX * (Convert.ToDouble(dataRow["TAXRATE"]) / 100);
                        TOTALAMOUNT = TOTALAMOUNT + taxamount;
                        dataRow["TAXAMOUNT"] = taxamount;
                        taxseqdt.AcceptChanges();
                    }
                    else if (dataRow["TAXAPPLIEDFOR"].Equals(Constants.CON_TAX_NETT))
                    {
                        double taxamount = TOTALAMOUNT * (Convert.ToDouble(dataRow["TAXRATE"]) / 100);
                        TOTALAMOUNT = TOTALAMOUNT + taxamount;
                        dataRow["TAXAMOUNT"] = taxamount;
                        taxseqdt.AcceptChanges();
                    }
                }
                taxseqdt.Rows.InsertAt(taxseqrw, 0);
                taxseqdt.Rows.Add(TAXPGCODE, COMPCODE, LOCCODE, null, null, null, Constants.CON_TT_TAX_AMOUNT, null, (TOTALAMOUNT - GROSSAMNTWITHOUTTAX)).AcceptChanges();
                taxseqdt.Rows.Add(TAXPGCODE, COMPCODE, LOCCODE, null, null, null, Constants.CON_TAX_NETT_RW, null, (TOTALAMOUNT + GROSSAMNTWITHTAX)).AcceptChanges();
                return taxseqdt;
            }
            catch (Exception ex)
            {
                CeegralogDataHandler.LogError(ex);
                throw ex;
            }
            finally
            {
                taxseqdt.Dispose();
                taxseqdt = null;
            }
        }

        public DataRow getcompany(string compcode, string sts)
        {
            DataTable SQLDT = new DataTable();
            DataRow dataRow = null;
            try
            {
                using (SqlConnection con = new SqlConnection(DataAccess.GetConnectionString()))
                {
                    if (con.State != ConnectionState.Open) { con.Open(); }
                    string SQLString = @" SELECT COMPCODE,COMPNAME,COMPADDRESS1,COMPADDRESS2,COMPADDRESS3,WEBSITE,HOTLINE,FAX,EMAIL,CONTATCPERSON,LOGOPATH,COUNTRY from COMPANY CP " +
                                   " INNER JOIN COUNTRY CT ON CT.COUNTRYCODE = CP.COUNTRYCODE  where COMPCODE = @compcode AND STS = @sts";
                    using (SqlCommand SQLCommand = new SqlCommand(SQLString, con))
                    {
                        using (SqlDataAdapter sqlda = new SqlDataAdapter(SQLCommand))
                        {

                            SQLCommand.Parameters.AddWithValue("@compcode", compcode);
                            SQLCommand.Parameters.AddWithValue("@sts", sts);
                            sqlda.Fill(SQLDT);
                            if (SQLDT.Rows.Count > 0)
                            {
                                dataRow = SQLDT.Rows[0];
                            }
                        }
                    }
                }
                
                return dataRow;
            }
            catch (Exception ex)
            {
                CeegralogDataHandler.LogError(ex);
                throw ex;
            }
        }

        public DataRow getLocationDetails(string compcode, string loccode, string sts)
        {
            DataRow dataRow = null;
            DataTable SQLDT = new DataTable();

            try
            {
                using (SqlConnection con = new SqlConnection(DataAccess.GetConnectionString()))
                {
                    if (con.State != ConnectionState.Open) { con.Open(); }
                    string SQLString = @" SELECT LOCATION,LOCADDRESS1,LOCADDRESS2,LOCADDRESS3,HOTLINE,FAX,EMAIL,PREFCURRENCY,COUNTRY,CONVERT(VARCHAR, SYSDATE, 23) AS SYSDATE, " +
                            " (SELECT ISNULL(DESCRIP,'') AS DESCRIP FROM MAP_PROPERTY_PAGE_INFO LPI  WHERE LPI.COMPCODE = LC.COMPCODE AND LPI.LOCCODE = LC.LOCCODE AND LPI.PROPERTYINFOCODE = '" + Constants.CON_PAGE_TITLE + "' AND LPI.STS = '" + sts + "') AS DESCRIP" +
                            " from LOCATIONS LC INNER JOIN LOCATION_SYSDATE LSD ON LSD.COMPCODE = LC.COMPCODE AND LSD.LOCCODE = LC.LOCCODE  " +
                            " INNER JOIN COUNTRY CR ON CR.COUNTRYCODE = LC.COUNTRYCODE WHERE LC.COMPCODE=@compcode AND LC.LOCCODE=@loccode AND LC.STS=@sts order by location";
                    using (SqlCommand SQLCommand = new SqlCommand(SQLString, con))
                    {
                        using (SqlDataAdapter sqlda = new SqlDataAdapter(SQLCommand))
                        {

                            SQLCommand.Parameters.AddWithValue("@compcode", compcode);
                            SQLCommand.Parameters.AddWithValue("@loccode", loccode);
                            SQLCommand.Parameters.AddWithValue("@sts", sts);
                            sqlda.Fill(SQLDT);
                            if (SQLDT.Rows.Count > 0)
                            {
                                dataRow = SQLDT.Rows[0];
                            }
                        }
                    }
                }
                
                return dataRow;
            }
            catch (Exception ex)
            {
                CeegralogDataHandler.LogError(ex);
                throw ex;
            }
        }

        public DataTable getPropertypageInfo(string compcode, string loccode, string sts)
        {
        
            DataTable SQLDT = new DataTable();

            try
            {
                using (SqlConnection con = new SqlConnection(DataAccess.GetConnectionString()))
                {
                    if (con.State != ConnectionState.Open) { con.Open(); }
                    string SQLString = @" SELECT REFCODE,PROPERTYINFOCODE,DESCRIP  FROM MAP_PROPERTY_PAGE_INFO   " +
                            " WHERE COMPCODE= @compcode AND LOCCODE = @loccode AND  STS= @sts order by PROPERTYINFOCODE";
                    using (SqlCommand SQLCommand = new SqlCommand(SQLString, con))
                    {
                        using (SqlDataAdapter sqlda = new SqlDataAdapter(SQLCommand))
                        {

                            SQLCommand.Parameters.AddWithValue("@compcode", compcode);
                            SQLCommand.Parameters.AddWithValue("@loccode", loccode);
                            SQLCommand.Parameters.AddWithValue("@sts", sts);
                            sqlda.Fill(SQLDT);
                        }
                    }
                }

                
                return SQLDT;
            }
            catch (Exception ex)
            {
                CeegralogDataHandler.LogError(ex);
                throw ex;
            }
        }

        public DataTable getroomimagepath(string compcode, string loccode, string sts)
        {
            DataTable SQLDT = new DataTable();

            try
            {
                using (SqlConnection con = new SqlConnection(DataAccess.GetConnectionString()))
                {
                    if (con.State != ConnectionState.Open) { con.Open(); }
                    string SQLString = @"SELECT RM.ROOMTYPECODE,RP.ROOMTYPE,RM.IMAGEPATH FROM ROOM_IMAGES RM LEFT JOIN ROOMTYPES RP on RP.ROOMTYPECODE = RM.ROOMTYPECODE " +
                            "WHERE RM.COMPCODE = @compcode AND RM.LOCCODE = @loccode AND RM.STS = @sts ORDER BY RM.ROOMTYPECODE";
                    using (SqlCommand SQLCommand = new SqlCommand(SQLString, con))
                    {
                        using (SqlDataAdapter sqlda = new SqlDataAdapter(SQLCommand))
                        {

                            SQLCommand.Parameters.AddWithValue("@compcode", compcode);
                            SQLCommand.Parameters.AddWithValue("@loccode", loccode);
                            SQLCommand.Parameters.AddWithValue("@sts", sts);
                            sqlda.Fill(SQLDT);
                        }
                    }
                }

                return SQLDT;
            }
            catch (Exception ex)
            {
                CeegralogDataHandler.LogError(ex);
                throw ex;
            }
        }

        public DataTable getCompanylocations(string compcode, string sts)
        {

            try
            {
                DataTable SQLDT = new DataTable();
                using (SqlConnection con = new SqlConnection(DataAccess.GetConnectionString()))
                {
                    if (con.State != ConnectionState.Open) { con.Open(); }
                    string SQLString = @" SELECT  LC.COMPCODE AS COMPCODE,LC.LOCCODE AS LOCCODE,LOCATION,CONCAT(LOCADDRESS1,',',LOCADDRESS2,',',LOCADDRESS3) AS ADDRESS,LOCADDRESS1,LOCADDRESS2,LOCADDRESS3,CITY,DISTRICT,HOTLINE,FAX,EMAIL,CONTATCPERSON,CONVERT(VARCHAR, SYSDATE, 23) AS SYSDATE ,CONCAT(LC.LOCCODE,'|',CONVERT(VARCHAR, SYSDATE, 23)) AS LOCSYSDATE,TIMEZONEINFO from LOCATIONS LC " +
                                        " INNER JOIN LOCATION_SYSDATE LSD ON LSD.COMPCODE = LC.COMPCODE AND LSD.LOCCODE = LC.LOCCODE WHERE LC.COMPCODE=@compcode AND LC.STS= @sts order by location";
                    using (SqlCommand SQLCommand = new SqlCommand(SQLString, con))
                    {
                        using (SqlDataAdapter sqlda = new SqlDataAdapter(SQLCommand))
                        {
                            SQLCommand.Parameters.AddWithValue("@compcode", compcode);
                            SQLCommand.Parameters.AddWithValue("@sts", sts);
                            sqlda.Fill(SQLDT);
                        }
                    }
                }

                return SQLDT;

            }
            catch (Exception ex)
            {
                CeegralogDataHandler.LogError(ex);
                throw ex;
            }
        }

        public DataTable getlocationexoptions(string compcode, string loccode, string roomtypecode, string sts, string year)
        {
            DataTable SQLDT = new DataTable();

            try
            {

                using (SqlConnection con = new SqlConnection(DataAccess.GetConnectionString()))
                {
                    if (con.State != ConnectionState.Open) { con.Open(); }
                    string SQLString = @"SELECT ROOMEXOPTIONCODE,RE.ROOMEXOPTIONPLANCODE,ROOMEXARATE,FIXEDORVALUE,ROOMTYPECODE,ROOMEXOPTION,NOOFUNITS FROM ROOMEXTRA_OPTIONS RE " +
                                        "LEFT JOIN ROOMEXTRA_OPTION_PLAN REP ON REP.ROOMEXOPTIONPLANCODE = RE.ROOMEXOPTIONPLANCODE " +
                                        "WHERE RE.COMPCODE = @compcode AND RE.CHANNELCODE = @loccode AND RE.STS = @sts AND RE.SEASONYEAR = @year AND REP.ROOMTYPECODE = @roomtypecode";
                    using (SqlCommand SQLCommand = new SqlCommand(SQLString, con))
                    using (SqlDataAdapter sqlda = new SqlDataAdapter(SQLCommand))
                    {

                        SQLCommand.Parameters.AddWithValue("@compcode", compcode);
                        SQLCommand.Parameters.AddWithValue("@loccode", loccode);
                        SQLCommand.Parameters.AddWithValue("@roomtypecode", roomtypecode);
                        SQLCommand.Parameters.AddWithValue("@year", year);
                        SQLCommand.Parameters.AddWithValue("@sts", sts);
                        SQLCommand.Parameters.AddWithValue("@STS", Constants.CON_STATUS_ACTIVE);
                        sqlda.Fill(SQLDT);
                    }
                }
                
                return SQLDT;
            }
            catch (Exception ex)
            {
                CeegralogDataHandler.LogError(ex);
                throw ex;
            } 
        }

        public DataTable getavlroomsnratesondate(string COMPCODE, string LOCCODE, string CHECKINDATE, string CHECKOUTDATE)
        {
            DataTable SQLDT = new DataTable();

            try
            {

                using (SqlConnection con = new SqlConnection(DataAccess.GetConnectionString()))
                {
                    if (con.State != ConnectionState.Open) { con.Open(); }
                    string SQLString = @"SELECT (SELECT TOP 1 RM.IMAGEPATH FROM ROOM_IMAGES RM where RM.ROOMTYPECODE = RA.ROOMTYPECODE) AS IMAGEPATH, RA.COMPCODE,RA.LOCCODE,RR.SEASONCODE,SEASONYEAR,ROOMRATECODE,RA.ROOMTYPECODE,ROOMTYPE,RA.ISTAXINCLUDE,CAST(ROOMRATE as NUMERIC(20,2)) AS ROOMRATE,RO.ROOMOPTION AS MEALPLAN,RO1.ROOMOPTION AS PAYOPTION,RO2.ROOMOPTION AS PAYCANCELOPTION,RO3.ROOMOPTION AS TAXDETAILS,RT.NOOFADULTS,RT.NOOFCHILDREN,RT.MAXOCCUPANCY," +
                    " (SELECT COUNT(ROOMNOCODE) AS TOT FROM  LOCATION_ROOMS LM" +
                    " WHERE LM.COMPCODE = RA.COMPCODE AND LM.LOCCODE = RA.LOCCODE" +
                    " AND LM.ROOMTYPECODE = RA.ROOMTYPECODE AND LM.STS = 'A') AS TOTROOMS," +
                    " (SELECT COUNT(DISTINCT ROOMNOCODE) AS TOT FROM  RESERVATION_CALENDAR RC" +
                    " WHERE RC.COMPCODE = RA.COMPCODE AND RC.LOCCODE = RA.LOCCODE" +
                    " AND RC.ROOMTYPECODE = RA.ROOMTYPECODE" +
                    " AND RC.RESDATE BETWEEN @CHECKINDATE AND @CHECKOUTDATE  AND RC.ISOCCUPIED = 'Y'  ) AS TOTRESROOMS" +
                    ",(SELECT PREFCURRENCY  from LOCATIONS where LOCCODE = @LOCCODE) AS PREFCURRENCY" +
                    " FROM ROOMRATE_PLAN RA INNER JOIN CALENDAR CA" +
                    " ON RA.COMPCODE = CA.COMPCODE AND RA.LOCCODE = CA.LOCCODE" +
                    " INNER JOIN ROOMRATES RR ON RR.COMPCODE = RA.COMPCODE AND  RR.LOCCODE = RA.LOCCODE AND RR.ROOMPLANCODE = RA.ROOMPLANCODE " +
                    " INNER JOIN  ROOMOPTIONS RO ON RO.ROOMOPTIONCODE = RA.MEALPLAN" +
                    " INNER JOIN ROOMOPTIONS RO1 ON RO1.ROOMOPTIONCODE = RA.PAYOPTION" +
                    " INNER JOIN  ROOMOPTIONS RO2 ON RO2.ROOMOPTIONCODE = RA.PAYCANCELOPTION" +
                    " INNER JOIN  ROOMOPTIONS RO3 ON RO3.ROOMOPTIONCODE = RA.ISTAXINCLUDE" +
                    " INNER JOIN  ROOMTYPES RT ON RT.COMPCODE = RA.COMPCODE" +
                    " AND RT.LOCCODE = RA.LOCCODE AND RA.ROOMTYPECODE = RT.ROOMTYPECODE" +
                    " INNER JOIN  CALENDAR_DETAILS CD ON CD.CALCODE = CA.CALCODE" +
                    " AND CD.SEASONCODE = RR.SEASONCODE AND CALDATE = @CHECKINDATE" +
                    " WHERE RA.COMPCODE = @COMPCODE AND RA.LOCCODE = @LOCCODE AND RA.STS = 'A' ORDER BY RA.ROOMTYPECODE";
                    using (SqlCommand SQLCommand = new SqlCommand(SQLString, con))
                    using (SqlDataAdapter sqlda = new SqlDataAdapter(SQLCommand))
                    {

                        SQLCommand.Parameters.AddWithValue("@COMPCODE", COMPCODE);
                        SQLCommand.Parameters.AddWithValue("@LOCCODE", LOCCODE);
                        SQLCommand.Parameters.AddWithValue("@CHECKINDATE", CHECKINDATE);
                        SQLCommand.Parameters.AddWithValue("@CHECKOUTDATE", CHECKOUTDATE);
                        sqlda.Fill(SQLDT);
                    }
                }
                
                return SQLDT;
            }
            catch (Exception ex)
            {
                CeegralogDataHandler.LogError(ex);
                throw ex;
            }
        }

        public DataTable getroomfactdetails(string compcode, string loccode)
        {
            DataTable SQLDT = new DataTable();

            try
            {

                using (SqlConnection con = new SqlConnection(DataAccess.GetConnectionString()))
                {
                    if (con.State != ConnectionState.Open) { con.Open(); }
                    string SQLString = @"SELECT MRF.COMPCODE, MRF.LOCCODE, MRF.ROOMTYPECODE , MRF.STS , RF.ROOMFACILITY " +
                    "FROM MAP_ROOM_FACILITIES MRF " +
                    "LEFT JOIN ROOM_FACILITIES RF ON MRF.FACCODE = RF.FACCODE " +
                    "WHERE MRF.COMPCODE = @compcode AND MRF.LOCCODE = @loccode AND MRF.STS = @sts" +
                    " ORDER BY MRF.ROOMTYPECODE";
                    using (SqlCommand SQLCommand = new SqlCommand(SQLString, con))
                    using (SqlDataAdapter sqlda = new SqlDataAdapter(SQLCommand))
                    {

                        SQLCommand.Parameters.AddWithValue("@compcode", compcode);
                        SQLCommand.Parameters.AddWithValue("@loccode", loccode);
                        SQLCommand.Parameters.AddWithValue("@sts", Constants.CON_STATUS_ACTIVE);
                        sqlda.Fill(SQLDT);
                    }
                }
                
                return SQLDT;
            }
            catch (Exception ex)
            {
                CeegralogDataHandler.LogError(ex);
                throw ex;
            }
        }

        public DataTable getcountrydetails()
        {
            DataTable SQLDT = new DataTable();

            try
            {

                using (SqlConnection con = new SqlConnection(DataAccess.GetConnectionString()))
                {
                    if (con.State != ConnectionState.Open) { con.Open(); }
                    string SQLString = "SELECT COUNTRY FROM COUNTRY";
                    using (SqlCommand SQLCommand = new SqlCommand(SQLString, con))
                    using (SqlDataAdapter sqlda = new SqlDataAdapter(SQLCommand))
                    {
                        sqlda.Fill(SQLDT);
                    }
                }

                return SQLDT;
            }
            catch (Exception ex)
            {
                CeegralogDataHandler.LogError(ex);
                throw ex;
            }
        }

        public DataTable getpaymentoptions(string compcode, string loccode, string sts)
        {
            DataTable SQLDT = new DataTable();

            try
            {

                using (SqlConnection con = new SqlConnection(DataAccess.GetConnectionString()))
                {
                    if (con.State != ConnectionState.Open) { con.Open(); }
                    string SQLString = @" SELECT CONCAT(RPO.PAYMENTCODE,'|',PAYMENTSTATUS) as PAYMENSTSTCODE ,PAYMENTMODE,ISNULL(ICONFILE,'') as ICONFILE from RESERVATION_PAYMENT_OPTIONS RPO " +
                                   " INNER JOIN MAP_LOCATION_PAYMENTOPTIONS MLP ON  rPO.PAYMENTCODE = MLP.PAYMENTCODE " +
                                   " WHERE MLP.COMPCODE = @compcode AND MLP.LOCCODE = @loccode AND RPO.STS=@sts AND  MLP.STS =@sts order by MLP.PAYMENTCODE ";
                    using (SqlCommand SQLCommand = new SqlCommand(SQLString, con))
                    using (SqlDataAdapter sqlda = new SqlDataAdapter(SQLCommand))
                    {
                        SQLCommand.Parameters.AddWithValue("@compcode", compcode);
                        SQLCommand.Parameters.AddWithValue("@loccode", loccode);
                        SQLCommand.Parameters.AddWithValue("@sts", sts);
                        sqlda.Fill(SQLDT);
                    }
                }
                
                return SQLDT;
            }
            catch (Exception ex)
            {
                CeegralogDataHandler.LogError(ex);
                throw ex;
            }
        }


        public DataTable getairportpickupoptions(string compcode, string loccode, string sts)
        {
            DataTable SQLDT = new DataTable();

            try
            {

                using (SqlConnection con = new SqlConnection(DataAccess.GetConnectionString()))
                {
                    if (con.State != ConnectionState.Open) { con.Open(); }
                    string SQLString = @"SELECT TRANSPORTMODECODE, TRANSPORTMODE,TRANSPORTRATE,DESCRIP,NOOFUNITS FROM LOCATION_TRANSPORTMODES WHERE COMPCODE=@compcode AND LOCCODE =@loccode  AND STS=@sts ";
                    using (SqlCommand SQLCommand = new SqlCommand(SQLString, con))
                    {
                        using (SqlDataAdapter sqlda = new SqlDataAdapter(SQLCommand))
                        {
                            SQLCommand.Parameters.AddWithValue("@compcode", compcode);
                            SQLCommand.Parameters.AddWithValue("@loccode", loccode);
                            SQLCommand.Parameters.AddWithValue("@sts", sts);
                            sqlda.Fill(SQLDT);
                        }
                    }
                }
                
                return SQLDT;
            }
            catch (Exception ex)
            {
                CeegralogDataHandler.LogError(ex);
                throw ex;
            }
        }

        public DataTable getRoomCodeNo(string COMPCODE, string LOCCODE, string CHECKINDATE, string CHECKOUTDATE, string ROOMTYPECODE, string ROOMCOUNT)
        {
            DataTable SQLDT = new DataTable();

            try
            {

                using (SqlConnection con = new SqlConnection(DataAccess.GetConnectionString()))
                {
                    if (con.State != ConnectionState.Open) { con.Open(); }
                    string SQLString = @"SELECT distinct top " + ROOMCOUNT + " ROOMNOCODE, ROOMTYPECODE " +
                    "FROM RESERVATION_CALENDAR where ROOMTYPECODE = @ROOMTYPECODE " +
                    "and COMPCODE = @COMPCODE and LOCCODE = @LOCCODE and RESDATE between @CHECKINDATE and @CHECKOUTDATE and ISOCCUPIED =@STS";
                    using (SqlCommand SQLCommand = new SqlCommand(SQLString, con))
                    {
                        using (SqlDataAdapter sqlda = new SqlDataAdapter(SQLCommand))
                        {
                            SQLCommand.Parameters.AddWithValue("@COMPCODE", COMPCODE);
                            SQLCommand.Parameters.AddWithValue("@LOCCODE", LOCCODE);
                            SQLCommand.Parameters.AddWithValue("@CHECKINDATE", CHECKINDATE);
                            SQLCommand.Parameters.AddWithValue("@CHECKOUTDATE", CHECKOUTDATE);
                            SQLCommand.Parameters.AddWithValue("@ROOMTYPECODE", ROOMTYPECODE);
                            SQLCommand.Parameters.AddWithValue("@STS", Constants.CON_NO);
                            sqlda.Fill(SQLDT);
                        }
                    }
                }
                
                return SQLDT;
            }
            catch (Exception ex)
            {
                CeegralogDataHandler.LogError(ex);
                throw ex;
            }
        }

        public String CreateReservation(string CHANNELCODE, string PYMODE, string PAYDATE, string NETAMT, string GROSSAMNT, string TAXAMNT, string COMPCODE, string LOCCODE,
            string GUESTREFCODE, string GUESTCODE, string NICPP, string TITLE, string FIRSTNAME, string LASTNAME,
            string EMAIL, string CONTACTNO, string COUNTRY, string LOCDATE, string STS, string ENUSER, string CHECKINDATE, string CHECKOUTDATE, string NOOFDAYS,
             string NOOFROOMS, string ADULTS, string CHILDREN, DataTable custreservermdt, DataTable taxbrkdn, DataTable selectedexoptTbcp, string RESDATE, string CLIENTRESDATE,DataTable selectedpickup)
        {
            string RESCODE = "";

            try
            {

                using (SqlConnection con = new SqlConnection(DataAccess.GetConnectionString()))
                {
                    if (con.State != ConnectionState.Open) { con.Open(); }
                    using (SqlCommand SQLCommand = new SqlCommand("sp_createreservation", con))
                    {
                        SQLCommand.CommandType = CommandType.StoredProcedure;
                        SQLCommand.Parameters.AddWithValue("@CHANNELCODE", CHANNELCODE.Trim());
                        SQLCommand.Parameters.AddWithValue("@COMPCODE", COMPCODE.Trim());
                        SQLCommand.Parameters.AddWithValue("@LOCCODE", LOCCODE.Trim());
                        SQLCommand.Parameters.AddWithValue("@CHECKINDATE", CHECKINDATE.Trim());
                        SQLCommand.Parameters.AddWithValue("@CHECKOUTDATE", CHECKOUTDATE.Trim());
                        SQLCommand.Parameters.AddWithValue("@NOOFDAYS", NOOFDAYS.Trim());
                        SQLCommand.Parameters.AddWithValue("@NOOFROOMS", NOOFROOMS.Trim());
                        SQLCommand.Parameters.AddWithValue("@ADULTS", ADULTS.Trim());
                        SQLCommand.Parameters.AddWithValue("@CHILDREN", CHILDREN.Trim());
                        SQLCommand.Parameters.AddWithValue("@GUESTREFCODE", GUESTREFCODE.Trim());
                        SQLCommand.Parameters.AddWithValue("@RESDATE", RESDATE.Trim());
                        SQLCommand.Parameters.AddWithValue("@PAYDATE", PAYDATE.Trim());
                        SQLCommand.Parameters.AddWithValue("@PYMODE", PYMODE.Trim());
                        SQLCommand.Parameters.AddWithValue("@GROSSAMT", GROSSAMNT);
                        SQLCommand.Parameters.AddWithValue("@TAXAMT", TAXAMNT);
                        SQLCommand.Parameters.AddWithValue("@NETAMT", NETAMT);
                        SQLCommand.Parameters.AddWithValue("@GUESTCODE", GUESTCODE.Trim());
                        SQLCommand.Parameters.AddWithValue("@NICPP", NICPP.Trim());
                        SQLCommand.Parameters.AddWithValue("@TITLE", TITLE.Trim());
                        SQLCommand.Parameters.AddWithValue("@FIRSTNAME", FIRSTNAME.Trim());
                        SQLCommand.Parameters.AddWithValue("@LASTNAME", LASTNAME.Trim());
                        SQLCommand.Parameters.AddWithValue("@EMAIL", EMAIL.Trim());
                        SQLCommand.Parameters.AddWithValue("@CONTACTNO", CONTACTNO.Trim());
                        SQLCommand.Parameters.AddWithValue("@COUNTRY", COUNTRY.Trim());
                        SQLCommand.Parameters.AddWithValue("@LOCDATE", LOCDATE.Trim());
                        SQLCommand.Parameters.AddWithValue("@CLIENTRESDATE", CLIENTRESDATE.Trim());
                        SQLCommand.Parameters.AddWithValue("@STS", STS.Trim());
                        SQLCommand.Parameters.AddWithValue("@ENUSER", ENUSER.Trim());
                        SQLCommand.Parameters.AddWithValue("@param", SqlDbType.Structured).Value = custreservermdt;
                        SQLCommand.Parameters.AddWithValue("@taxparam", SqlDbType.Structured).Value = taxbrkdn;
                        SQLCommand.Parameters.AddWithValue("@exoptionparam", SqlDbType.Structured).Value = selectedexoptTbcp;
                        SQLCommand.Parameters.AddWithValue("@pickupparam", SqlDbType.Structured).Value = selectedpickup;

                        SQLCommand.Parameters.Add("@RESCODE", SqlDbType.VarChar, 20);
                        SQLCommand.Parameters["@RESCODE"].Direction = ParameterDirection.Output;
                        SQLCommand.ExecuteNonQuery();

                        RESCODE = (string)SQLCommand.Parameters["@RESCODE"].Value;
                        SQLCommand.Parameters.Clear();

                        return RESCODE;
                    }
                }

                
            }
            catch (Exception ex)
            {
                CeegralogDataHandler.LogError(ex);
                throw ex;
            }
        }

        public Boolean updatereservation(string RESCODE, string STS)
        {
            Boolean isupdate = false;

            try
            {
                using (SqlConnection con = new SqlConnection(DataAccess.GetConnectionString()))
                {
                    if (con.State != ConnectionState.Open) { con.Open(); }
                    using (SqlCommand SQLCommand = new SqlCommand("sp_updatereservation", con))
                    {
                        SQLCommand.CommandType = CommandType.StoredProcedure;
                        SQLCommand.Parameters.AddWithValue("@RESCODE", RESCODE.Trim());
                        SQLCommand.Parameters.AddWithValue("@STS", STS.Trim());
                        SQLCommand.ExecuteNonQuery();

                        isupdate = true;
                    }
                }

                return isupdate;
            }
            catch (Exception ex)
            {
                CeegralogDataHandler.LogError(ex);
                throw ex;
            }
        }

        public DataRow getReservationDetails(string RESCODE)
        {
            DataRow dataRow = null;
            DataTable SQLDT = new DataTable();

            try
            {

                using (SqlConnection con = new SqlConnection(DataAccess.GetConnectionString()))
                {
                    if (con.State != ConnectionState.Open) { con.Open(); }
                    string SQLString = @"SELECT RS.COMPCODE AS COMPCODE,RS.LOCCODE AS LOCCODE,CHECKINTIME,CHECKOUTTIME,AGEPOLICY,PETPOLICY,SMOKINGPOLICY,PARTYPOLICY,PAYCANCELPOLICY,PAYMETHODPOLICY,CONTATCPERSON,CO.COUNTRY AS COUNTRY,PREFCURRENCY,RP.TRNO AS TRNO,LOCATION,LOCADDRESS1,LOCADDRESS2,LOCADDRESS3,HOTLINE,FAX,LO.EMAIL AS LOEMAIL,PAYMENTMODE,ICONFILE,GROSSAMT,TAXAMT,NETAMT,RS.RESCODE AS RESCODE,RS.GUESTREFCODE AS GUESTREFCODE,NICPP,CONCAT(TITLE,' ',FIRSTNAME,' ',LASTNAME) AS GUESTNAME,GS.EMAIL AS EMAIL,CONTACTNO,GS.COUNTRY AS COUNTRY,CHECKINDATE,CHECKOUTDATE,NOOFDAYS,NOOFROOMS,GUESTS,ADULTS,CHILDREN,RS.STS AS STS,RS.ENUSER AS ENUSER,RS.ENDATE AS ENDATE,RS.CLIENTRESDATE AS CLIENTRESDATE,RS.LOCDATE AS LOCDATE " +
                                        " FROM  RESERVATION RS INNER JOIN GUESTS GS ON  RS.GUESTREFCODE = GS.GUESTREFCODE INNER JOIN LOCATIONS LO ON LO.COMPCODE = RS.COMPCODE AND LO.LOCCODE = RS.LOCCODE INNER JOIN RESERVATION_PAYMENTS RP ON RP.COMPCODE = RS.COMPCODE AND RP.LOCCODE = RS.LOCCODE AND RP.RESCODE = RS.RESCODE INNER JOIN RESERVATION_PAYMENT_OPTIONS RPO ON RPO.PAYMENTCODE = RP.PYMODE INNER JOIN COUNTRY CO ON CO.COUNTRYCODE = LO.COUNTRYCODE " +
                                        " INNER JOIN LOCATION_POLICY LP ON LP.COMPCODE = RS.COMPCODE AND LP.LOCCODE = RS.LOCCODE WHERE RS.RESCODE = @RESCODE";
                    using (SqlCommand SQLCommand = new SqlCommand(SQLString, con))
                    {
                        using (SqlDataAdapter sqlda = new SqlDataAdapter(SQLCommand))
                        {
                            SQLCommand.Parameters.AddWithValue("@RESCODE", RESCODE);
                            sqlda.Fill(SQLDT);
                            if (SQLDT.Rows.Count > 0)
                            {
                                dataRow = SQLDT.Rows[0];
                            }
                        }
                    }
                }

                return dataRow;
            }
            catch (Exception ex)
            {
                CeegralogDataHandler.LogError(ex);
                throw ex;
            }
        }

        public DataTable getReservationBillDetails(string RESCODE)
        {

            DataTable SQLDT = new DataTable();

            try
            {

                using (SqlConnection con = new SqlConnection(DataAccess.GetConnectionString()))
                {
                    if (con.State != ConnectionState.Open) { con.Open(); }
                    string SQLString = @" SELECT  RD.RESCODE AS RESCODE,RD.TRNO AS TRNO,ROOMTYPE,ROOMNO,RD.ROOMRATE AS ROOMRATE,CHECKINDATE,CHECKOUTDATE,RO.ROOMOPTION AS ROOMOPTION " +
                                   " FROM RESERVATION_DETAILS RD INNER JOIN ROOMTYPES RT ON RD.COMPCODE = RT.COMPCODE AND RD.LOCCODE = RT.LOCCODE " +
                                   " AND RD.ROOMTYPECODE = RT.ROOMTYPECODE INNER JOIN LOCATION_ROOMS LR ON RD.COMPCODE = LR.COMPCODE AND  RD.LOCCODE = LR.LOCCODE  " +
                                   " AND RD.ROOMTYPECODE = LR.ROOMTYPECODE  AND RD.ROOMNOCODE = LR.ROOMNOCODE INNER JOIN ROOMRATES RR ON RR.COMPCODE = RD.COMPCODE AND RR.LOCCODE = RD.LOCCODE " +
                                   " AND RR.ROOMRATECODE = RD.ROOMRATECODE  INNER JOIN ROOMRATE_PLAN RP ON RP.COMPCODE = RD.COMPCODE AND RP.LOCCODE = RD.LOCCODE AND RP.ROOMPLANCODE = RR.ROOMPLANCODE " +
                                   " INNER JOIN ROOMOPTIONS RO ON RO.ROOMOPTIONCODE = RP.MEALPLAN WHERE  RD.RESCODE = @RESCODE";

                    using (SqlCommand SQLCommand = new SqlCommand(SQLString, con))
                    {
                        using (SqlDataAdapter sqlda = new SqlDataAdapter(SQLCommand))
                        {
                            SQLCommand.Parameters.AddWithValue("@RESCODE", RESCODE);
                            sqlda.Fill(SQLDT);
                        }
                    }
                }

                return SQLDT;
            }
            catch (Exception ex)
            {
                CeegralogDataHandler.LogError(ex);
                throw ex;
            }
             
        }

        public DataTable getReservationExoptDetails(string RESCODE)
        {
            DataTable SQLDT = new DataTable();

            try
            {
                using (SqlConnection con = new SqlConnection(DataAccess.GetConnectionString()))
                {
                    if (con.State != ConnectionState.Open) { con.Open(); }
                    string SQLString = @"select ROE.ROOMEXOPTION,REE.NOOFUNITS,REE.UNITRATE,REE.RATE  from RESERVATION_EXTRAOPTIONS REE  " +
                    "left join ROOMEXTRA_OPTION_PLAN ROE on ROE.ROOMEXOPTIONPLANCODE = REE.ROOMEXOPTIONPLANCODE where RESCODE = @RESCODE";
                    using (SqlCommand SQLCommand = new SqlCommand(SQLString, con))
                    {
                        using (SqlDataAdapter sqlda = new SqlDataAdapter(SQLCommand))
                        {
                            SQLCommand.Parameters.AddWithValue("@RESCODE", RESCODE);
                            sqlda.Fill(SQLDT);
                        }
                    }
                }
                
                return SQLDT;
            }
            catch (Exception ex)
            {
                CeegralogDataHandler.LogError(ex);
                throw ex;
            }
        }

        public DataTable getReservationBillpaydetails(string RESCODE)
        {

            DataTable SQLDT = new DataTable();

            try
            {

                using (SqlConnection con = new SqlConnection(DataAccess.GetConnectionString()))
                {
                    if (con.State != ConnectionState.Open) { con.Open(); }
                    string SQLString = " SELECT  TM.TAXTYPE AS TAXTYPE,RDP.TAXRATE AS TAXRATE,AMOUNT   FROM RESERVATION_PAYMENT_DETAILS  RDP INNER JOIN GLTAXTYPE TM " +
                                   " ON  RDP.COMPCODE = TM.COMPCODE AND RDP.LOCCODE = TM.LOCCODE AND RDP.TAXCODE = TM.TAXCODE  " +
                                   " WHERE RDP.RESCODE = @RESCODE";
                    using (SqlCommand SQLCommand = new SqlCommand(SQLString, con))
                    {
                        using (SqlDataAdapter sqlda = new SqlDataAdapter(SQLCommand))
                        {
                            SQLCommand.Parameters.AddWithValue("@RESCODE", RESCODE);
                            sqlda.Fill(SQLDT);
                        }
                    }
                }
                return SQLDT;
            }
            catch (Exception ex)
            {
                CeegralogDataHandler.LogError(ex);
                throw ex;
            }
        }

        public String ViewResdetailsForInvoice(string RESCODE, int NOOFDAYS, string PREFCURRENCY)
        {
            string strroom = "";
            DataTable dtroom = new DataTable();

            try
            {
                strroom += "<Table style='border-collapse:collapse;border:none;width:100%;margin-bottom:2%'>";
                strroom += "<tr style='background-color:#CCC'>";
                strroom += "<td style='width:40%;text-align:left;border:1px solid #888;padding:.5%;border-spacing:0'> DESCRIPTION </td>";
                strroom += "<td style='width:20%;text-align:right;border:1px solid #888;padding:.5%;border-spacing:0'> UNIT COST </td>";
                strroom += "<td style='width:20%;text-align:right;border:1px solid #888;padding:.5%;border-spacing:0'> UITS </td>";
                strroom += "<td style='width:30%;text-align:right;border:1px solid #888;padding:.5%;border-spacing:0'> AMOUNT </td></tr>";
                dtroom = getReservationBillDetails(RESCODE);
                for (int i = 0; i < dtroom.Rows.Count; i++)
                {
                    string ROOMTYPE = dtroom.Rows[i]["ROOMTYPE"].ToString();
                    string ROOMNO = dtroom.Rows[i]["ROOMNO"].ToString();
                    double ROOMRATE = double.Parse(dtroom.Rows[i]["ROOMRATE"].ToString());
                    string ROOMOPTION = dtroom.Rows[i]["ROOMOPTION"].ToString();
                    string CHECKINDATE = dtroom.Rows[i]["CHECKINDATE"].ToString();
                    string CHECKOUTDATE = dtroom.Rows[i]["CHECKOUTDATE"].ToString();
                    string ROOMPRICE = Math.Round((ROOMRATE * NOOFDAYS)).ToString("#,##0.00");
                    strroom += "<tr><td style='width:40%;text-align:left;border-collapse:collapse;border:1px solid #888;padding:.5%;border-spacing:0'>" + ROOMTYPE + " Room No. " + ROOMNO + " : " + ROOMOPTION + "</td>";
                    strroom += "<td style='width:20%;text-align:right;border-collapse:collapse;border:1px solid #888;padding:.5%;border-spacing:0'>" + Math.Round(ROOMRATE).ToString("#,##0.00") + "</td>";
                    strroom += "<td style='width:20%;text-align:right;border-collapse:collapse;border:1px solid #888;padding:.5%;border-spacing:0'> 1 X " + NOOFDAYS + " Day/s </td>";
                    strroom += "<td style='width:30%;text-align:right;border-collapse:collapse;border:1px solid #888;padding:.5%;border-spacing:0'>" + PREFCURRENCY + " " + ROOMPRICE + "</td></tr>";
                }

                strroom += "</Table>";

                return strroom;
            }
            catch (Exception ex)
            {
                CeegralogDataHandler.LogError(ex);
                throw ex;
            }
        }

        public String ViewExoptionsForInvoice(string RESCODE, string PREFCURRENCY)
        {
            string strroom = "";
            DataTable dtroom = new DataTable();

            try
            {
                dtroom = getReservationExoptDetails(RESCODE);
                if (dtroom.Rows.Count > 0)
                {
                    strroom += "<Table style='border-collapse:collapse;border:none;width:100%;margin-bottom:2%'>";
                    strroom += "<tr style='background-color:#CCC'>";
                    strroom += "<td style='width:40%;text-align:left;border:1px solid #888;padding:.5%;border-spacing:0'> ROOM EXTRA OPTIONS </td>";
                    strroom += "<td style='width:20%;text-align:right;border:1px solid #888;padding:.5%;border-spacing:0'> UNIT COST </td>";
                    strroom += "<td style='width:20%;text-align:right;border:1px solid #888;padding:.5%;border-spacing:0'> UNITS </td>";
                    strroom += "<td style='width:30%;text-align:right;border:1px solid #888;padding:.5%;border-spacing:0'> AMOUNT </td></tr>";
                    for (int i = 0; i < dtroom.Rows.Count; i++)
                    {
                        string ROOMEXOPTION = dtroom.Rows[i]["ROOMEXOPTION"].ToString();
                        double UNITRATE = double.Parse(dtroom.Rows[i]["UNITRATE"].ToString());
                        string NOOFUNITS = dtroom.Rows[i]["NOOFUNITS"].ToString();
                        double RATE = double.Parse(dtroom.Rows[i]["RATE"].ToString());
                        strroom += "<tr><td style='width:40%;text-align:left;border-collapse:collapse;border:1px solid #888;padding:.5%;border-spacing:0'>" + ROOMEXOPTION + "</td>";
                        strroom += "<td style='width:20%;text-align:right;border-collapse:collapse;border:1px solid #888;padding:.5%;border-spacing:0'>" + Math.Round(UNITRATE).ToString("#,##0.00") + "</td>";
                        strroom += "<td style='width:20%;text-align:right;border-collapse:collapse;border:1px solid #888;padding:.5%;border-spacing:0'> " + NOOFUNITS + "</td>";
                        strroom += "<td style='width:30%;text-align:right;border-collapse:collapse;border:1px solid #888;padding:.5%;border-spacing:0'>" + PREFCURRENCY + " " + RATE + "</td></tr>";
                    }

                    strroom += "</Table>";
                }

                return strroom;
            }
            catch (Exception ex)
            {
                CeegralogDataHandler.LogError(ex);
                throw ex;
            }
        }

        public String ViewSummaryForInvoice(string RESCODE, double GROSSAMT, double TAXAMT, double NETAMT, string PREFCURRENCY)
        {
            string strsummery = "";
            DataTable dtroom = new DataTable();

            try
            {
                strsummery += "<Table style='border - collapse:collapse; border: none; width: 100%; margin - bottom:2 %;'><tr>";
                strsummery += "<td style='width:50%;text-align:right'>SUB TOTAL </td>";
                strsummery += "<td style='width:50%;text-align:right'>" + PREFCURRENCY + " " + Math.Round(GROSSAMT).ToString("#,##0.00") + "</td></tr>";

                dtroom = getReservationBillpaydetails(RESCODE);
                for (int i = 0; i < dtroom.Rows.Count; i++)
                {
                    string TAXTYPE = dtroom.Rows[i]["TAXTYPE"].ToString();
                    string TAXRATE = dtroom.Rows[i]["TAXRATE"].ToString();
                    double AMOUNT = double.Parse(dtroom.Rows[i]["AMOUNT"].ToString());

                    strsummery += "<tr><td style='width:20%;text-align:right'>" + TAXTYPE + " [ " + TAXRATE + " ] " + "</td>";
                    strsummery += "<td style='width:10%;text-align:right'>" + PREFCURRENCY + " " + Math.Round(AMOUNT).ToString("#,##0.00") + "</td></tr>";
                }

                strsummery += "<tr><td style='width:20%;text-align:right'>TOTAL TAX </td>";
                strsummery += "<td style='width:10%;text-align:right'>" + PREFCURRENCY + " " + Math.Round(TAXAMT).ToString("#,##0.00") + "</td></tr>";

                strsummery += "<tr><td style='width:20%;text-align:right'>TOTAL PAYABLE</td>";
                strsummery += "<td style='width:10%;text-align:right'>" + PREFCURRENCY + " " + Math.Round(NETAMT).ToString("#,##0.00") + "</td></tr>";

                strsummery += "</Table>";

                return strsummery;
            }
            catch (Exception ex)
            {
                CeegralogDataHandler.LogError(ex);
                throw ex;
            }
        }

        public String getPageDocDetails(string COMPCODE, string LOCCODE, string PAGEDOCCODE)
        {
            DataRow datarow = null;
            String strmessage = "";
            DataTable SQLDT = new DataTable();

            try
            {

                using (SqlConnection con = new SqlConnection(DataAccess.GetConnectionString()))
                {
                    if (con.State != ConnectionState.Open) { con.Open(); }
                    string SQLString = " SELECT PAGEDOCCODE,PAGESALUTATION,PAGETITLE,PAGEINTRO,PAGEBODY,PAGECLOSE,PAGESIGNATURE " +
                                    " FROM MAP_PROPERTY_PAGE_DOC WHERE COMPCODE = '" + COMPCODE + "' AND LOCCODE = '" + LOCCODE + "' AND PAGEDOCCODE ='" + PAGEDOCCODE + "' AND STS = '" + Constants.CON_STATUS_ACTIVE + "'";
                    using (SqlCommand SQLCommand = new SqlCommand(SQLString, con))
                    {
                        using (SqlDataAdapter sqlda = new SqlDataAdapter(SQLCommand))
                        {
                            sqlda.Fill(SQLDT);
                            if (SQLDT.Rows.Count > 0)
                            {
                                datarow = SQLDT.Rows[0];
                            }
                        }
                    }
                }

                if (datarow != null)
                {
                    strmessage += datarow["PAGESALUTATION"].ToString() + "<br><br>";
                    strmessage += datarow["PAGETITLE"].ToString() + "<br><br>";
                    strmessage += datarow["PAGEINTRO"].ToString() + "<br>";
                    strmessage += datarow["PAGEBODY"].ToString() + "<br>";
                    strmessage += datarow["PAGECLOSE"].ToString() + "<br><br>";
                    strmessage += datarow["PAGESIGNATURE"].ToString() + "<br><br>";
                }
                

                return strmessage;
            }
            catch (Exception ex)
            {
                CeegralogDataHandler.LogError(ex);
                throw ex;
            }
        }

        public String ViewResMessage(string COMPCODE, string LOCCODE, string GUESTNAME, string LOCATION, string HOTLINE, string CONTATCPERSON, string COUNTRY)
        {
            string strmessage = "";

            try
            {
                string PAGEDOCCODE = "BRESMAG";
                strmessage = getPageDocDetails(COMPCODE, LOCCODE, PAGEDOCCODE);

                return strmessage;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public DataRow getguestDetails(string NICPP)
        {
            DataTable SQLDT = new DataTable();
            DataRow dataRow = null;

            try
            {

                using (SqlConnection con = new SqlConnection(DataAccess.GetConnectionString()))
                {
                    if (con.State != ConnectionState.Open) { con.Open(); }
                    string SQLString = @"SELECT GUESTREFCODE,GUESTCODE,NICPP,TITLE,FIRSTNAME,LASTNAME,EMAIL,CONTACTNO,COUNTRY,STS FROM GUESTS  WHERE NICPP = @NICPP";
                    using (SqlCommand SQLCommand = new SqlCommand(SQLString, con))
                    {
                        using (SqlDataAdapter sqlda = new SqlDataAdapter(SQLCommand))
                        {
                            SQLCommand.Parameters.AddWithValue("@NICPP", NICPP);
                            sqlda.Fill(SQLDT);
                            if (SQLDT.Rows.Count > 0)
                            {
                                dataRow = SQLDT.Rows[0];
                            }
                        }
                    }
                }
                
                return dataRow;
            }
            catch (Exception ex)
            {
                CeegralogDataHandler.LogError(ex);
                throw ex;
            }
        }

        public DataTable getPagelayoutDetails(string COMPCODE, string LOCCODE)
        {
            DataTable SQLDT = new DataTable();

            try
            {

                using (SqlConnection con = new SqlConnection(DataAccess.GetConnectionString()))
                {
                    if (con.State != ConnectionState.Open) { con.Open(); }
                    string SQLString = @"SELECT  DISPLAYLOCATION,IMAGEPATH,PROPIMAGE   FROM  PROPERTY_PAGE_LAYOUT_DETAILS PLD INNER JOIN MAP_PROPERTY_PAGE_LAYOUT MPL " +
                                   " ON MPL.PROPLAYOUTCODE = PLD.PROPLAYOUTCODE AND  MPL.PROPLAYOUTSUBCODE = PLD.PROPLAYOUTSUBCODE WHERE MPL.COMPCODE = @COMPCODE  AND MPL.LOCCODE = @LOCCODE AND  MPL.STS = @STS ORDER BY DISPLAYLOCATION";
                    using (SqlCommand SQLCommand = new SqlCommand(SQLString, con))
                    {
                        using (SqlDataAdapter sqlda = new SqlDataAdapter(SQLCommand))
                        {
                            SQLCommand.Parameters.AddWithValue("@COMPCODE", COMPCODE);
                            SQLCommand.Parameters.AddWithValue("@LOCCODE", LOCCODE);
                            SQLCommand.Parameters.AddWithValue("@STS", Constants.CON_STATUS_ACTIVE);
                            sqlda.Fill(SQLDT);
                        }
                    }
                }
                
                return SQLDT;
            }
            catch (Exception ex)
            {
                CeegralogDataHandler.LogError(ex);
                throw ex;
            }
        }

        public DataTable getPageTemplate(string COMPCODE, string LOCCODE)
        {
            DataTable SQLDT = new DataTable();

            try
            {

                using (SqlConnection con = new SqlConnection(DataAccess.GetConnectionString()))
                {
                    if (con.State != ConnectionState.Open) { con.Open(); }
                    string SQLString = @"SELECT  DISPLAYLOCATION,IMAGEPATH,PROPIMAGE  FROM  PROPERTY_PAGE_LAYOUT_DETAILS PLD INNER JOIN MAP_PROPERTY_PAGE_LAYOUT MPL " +
                                   " ON MPL.PROPLAYOUTCODE = PLD.PROPLAYOUTCODE AND  MPL.PROPLAYOUTSUBCODE = PLD.PROPLAYOUTSUBCODE WHERE MPL.COMPCODE = @COMPCODE AND MPL.LOCCODE = @LOCCODE AND  MPL.STS = @STS AND MPL.ISDEFAULT = @DEFAULT ";
                    using (SqlCommand SQLCommand = new SqlCommand(SQLString, con))
                    {
                        using (SqlDataAdapter sqlda = new SqlDataAdapter(SQLCommand))
                        {
                            SQLCommand.Parameters.AddWithValue("@COMPCODE", COMPCODE);
                            SQLCommand.Parameters.AddWithValue("@LOCCODE", LOCCODE);
                            SQLCommand.Parameters.AddWithValue("@STS", Constants.CON_STATUS_ACTIVE);
                            SQLCommand.Parameters.AddWithValue("@DEFAULT", Constants.CON_YES);
                            sqlda.Fill(SQLDT);
                        }
                    }
                }

                return SQLDT;
            }
            catch (Exception ex)
            {
                CeegralogDataHandler.LogError(ex);
                throw ex;
            }
        }
    }
}