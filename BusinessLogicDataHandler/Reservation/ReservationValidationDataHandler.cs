﻿using ConstantsDataHandler;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace BusinessLogicDataHandler.Reservation
{
    public class ReservationValidationDataHandler
    {
        // assign the propery info - code by thanuja
        public string[] Assignpropertyinfo(DataTable dtpage)
        {
            string[] strdata = new string[2];
            for (int i = 0; i < dtpage.Rows.Count; i++)
            {
                string PROPERTYINFOCODE = dtpage.Rows[i]["PROPERTYINFOCODE"].ToString();
                string DESCRIP = dtpage.Rows[i]["DESCRIP"].ToString();
                if (PROPERTYINFOCODE == Constants.CON_PAGE_NOTIFICATION)
                {
                    strdata[0] = DESCRIP.ToString();
                }
                else if (PROPERTYINFOCODE == Constants.CON_PAGE_INFORMATION)
                {
                    strdata[1] += DESCRIP.ToString() + "</br>";
                }
                else if (PROPERTYINFOCODE == Constants.CON_PAGE_REMARK)
                {
                    strdata[2] += DESCRIP.ToString() + "</br>";
                }
            }
            return strdata;
        }

        // refine room information data table - add room faciliets to the data table
        public DataTable Refineroomsdt(DataTable roomdt, DataTable roomfacdt)
        {
            List<string> objlistItem = new List<string>();

            foreach (DataRow dataRow in roomdt.Rows)
            {
                dataRow["TOTAVAILABLEROOMS"] = Convert.ToInt32(dataRow["TOTROOMS"].ToString()) - Convert.ToInt32(dataRow["TOTRESROOMS"].ToString());
                dataRow["TOTAVAILABLEROOMDESCRIP"] = dataRow["TOTAVAILABLEROOMS"].ToString() + " room / rooms available";
                if (Convert.ToInt32(dataRow["TOTAVAILABLEROOMS"]) == 0)
                {
                    dataRow.Delete();
                }
            }

            roomdt.AcceptChanges();
            for (int i = 0; i <= roomdt.Rows.Count - 1; i++)
            {
                for (int j = 0; j <= roomfacdt.Rows.Count - 1; j++)
                {
                    if (roomdt.Rows[i]["ROOMTYPECODE"].ToString().Equals(roomfacdt.Rows[j]["ROOMTYPECODE"].ToString()))
                    {
                        objlistItem.Add(roomfacdt.Rows[j]["ROOMFACILITY"].ToString());
                    }

                    string combindedString = string.Join("\t\u2022\t", objlistItem.ToArray());
                    roomdt.Rows[i]["ROOMFACILITY"] = combindedString;
                }
                objlistItem.Clear();
            }
            roomdt.AcceptChanges();

            return roomdt;
        }

        // selected room info summary table is set according to the room type code
        // TODO:set the selected room info summary table according to the room rate code
        public DataTable setselectedRoomTable(DataTable roomdetTb, DataTable selectedRoomTb, DataTable SERDET
            , string selectedroomtype, string selectedroomAvNo, string roomtypeCd, string selectedroomNum, string maxoccupancy, string selectedroomrt, string roomexoptrt, string TaxCode, string ROOMRATECODE)
        {
            // find the room rate code is exist in the data table
            DataRow fdRow = selectedRoomTb.Rows.Find(ROOMRATECODE);


            if (fdRow != null)
            {
                fdRow["selectRoomCount"] = roomdetTb.AsEnumerable()
                                   .Where(y => y.Field<string>("roomrtCdID") == ROOMRATECODE)
                                    .Sum(x => x.Field<int>("roomCount"))
                                           .ToString();

                if (fdRow["selectRoomCount"].Equals(0))
                {
                    fdRow.Delete();
                }
                else
                {
                    fdRow["selectRoomrt"] = roomdetTb.AsEnumerable()
                                   .Where(y => y.Field<string>("roomrtCdID") == ROOMRATECODE)
                                    .Sum(x => ((x.Field<int>("roomCount") * x.Field<double>("roomrt")) + x.Field<double>("roomexoptrt")) * Convert.ToDouble(SERDET.Rows[0]["NOOFDAYS"]))
                                           .ToString();
                    fdRow["roomrtwithtx"] = roomdetTb.AsEnumerable()
                                  .Where(y => y.Field<string>("roomrtCdID") == ROOMRATECODE && y.Field<string>("taxCode") == "ROP00014")
                                   .Sum(x => ((x.Field<int>("roomCount") * x.Field<double>("roomrt")) + x.Field<double>("roomexoptrt")) * Convert.ToDouble(SERDET.Rows[0]["NOOFDAYS"]))
                                          .ToString();
                    fdRow["roomrtwithouttx"] = roomdetTb.AsEnumerable()
                                  .Where(y => y.Field<string>("roomrtCdID") == ROOMRATECODE && y.Field<string>("taxCode") == "ROP00013")
                                   .Sum(x => ((x.Field<int>("roomCount") * x.Field<double>("roomrt")) + x.Field<double>("roomexoptrt")) * Convert.ToDouble(SERDET.Rows[0]["NOOFDAYS"]))
                                          .ToString();
                    fdRow["maxoccupancy"] = roomdetTb.AsEnumerable()
                                  .Where(y => y.Field<string>("roomrtCdID") == ROOMRATECODE)
                                   .Sum(x => x.Field<int>("maximumoccupancy") * x.Field<int>("roomCount"))
                                          .ToString();

                    if (Convert.ToInt32(fdRow["selectRoomCount"].ToString()) > Convert.ToInt32(selectedroomAvNo))
                    {
                        fdRow["maxNumReach"] = "only " + selectedroomAvNo + " rooms left";
                    }
                    else
                    {
                        fdRow["maxNumReach"] = null;
                    }
                }
            }
            else
            {
                DataRow cusdtrow = selectedRoomTb.NewRow();

                cusdtrow["selectRmTyp"] = selectedroomtype;
                cusdtrow["selectRmTypcd"] = roomtypeCd;
                cusdtrow["maxoccupancy"] = Convert.ToInt32(selectedroomNum) * Convert.ToInt32(maxoccupancy);
                cusdtrow["roomrtwithtx"] = 0;
                cusdtrow["roomrtwithouttx"] = 0;
                cusdtrow["selectroomratecd"] = ROOMRATECODE;
                if (Convert.ToInt32(selectedroomNum) > Convert.ToInt32(selectedroomAvNo))
                {
                    cusdtrow["maxNumReach"] = "only " + selectedroomAvNo + " rooms left";
                }
                else
                {
                    cusdtrow["maxNumReach"] = null;
                }

                if (TaxCode.Equals("ROP00014"))
                {
                    cusdtrow["roomrtwithtx"] = ((Convert.ToInt32(selectedroomNum) * Convert.ToDouble(selectedroomrt)) + Convert.ToDouble(roomexoptrt)) * Convert.ToDouble(SERDET.Rows[0]["NOOFDAYS"]);
                }
                else if (TaxCode.Equals("ROP00013"))
                {
                    cusdtrow["roomrtwithouttx"] = ((Convert.ToInt32(selectedroomNum) * Convert.ToDouble(selectedroomrt)) + Convert.ToDouble(roomexoptrt)) * Convert.ToDouble(SERDET.Rows[0]["NOOFDAYS"]);
                }
                cusdtrow["selectRoomrt"] = ((Convert.ToInt32(selectedroomNum) * Convert.ToDouble(selectedroomrt)) + Convert.ToDouble(roomexoptrt)) * Convert.ToDouble(SERDET.Rows[0]["NOOFDAYS"]);
                cusdtrow["selectRoomCount"] = Convert.ToInt32(selectedroomNum);
                if (cusdtrow["selectRoomCount"].Equals(0))
                {
                    cusdtrow.Delete();
                }
                else
                {
                    selectedRoomTb.Rows.Add(cusdtrow);
                }
            }
            selectedRoomTb.AcceptChanges();
            return selectedRoomTb;
        }

        // calculate total amount
        // the total amount is calculated according to the tax inclusive and exclusive info provided with the meal plan option
        // calucalte tax amount
        public DataTable calculatetotamount(DataTable selectedRoomTb, string RESCOMPCODE, string LOCCODE)
        {
            ReservationDataHandler eReservationDataHandler = new ReservationDataHandler();

            double totAmntwithtax = 0;
            double totAmntwithouttax = 0;

            totAmntwithtax = selectedRoomTb.AsEnumerable()
                               .Sum(x => x.Field<double>("roomrtwithtx"));

            totAmntwithouttax = selectedRoomTb.AsEnumerable()
                               .Sum(x => x.Field<double>("roomrtwithouttx"));
            DataTable taxTb = eReservationDataHandler.calculateTaxAmount(RESCOMPCODE, LOCCODE, "RS", totAmntwithouttax, totAmntwithtax);
            return taxTb;
        }

        // when the confirm button is selected find the room no code and set it with the data table that is passed to the database
        public DataTable setRoomCodeAllocation(DataTable roomdetTb, DataTable SERDET, string LOCCODE, string RESCOMPCODE)
        {
            ReservationDataHandler reservationDataHandler = new ReservationDataHandler();

            DataTable custreservermdt = new DataTable();
            custreservermdt.Columns.Add("COMPCODE", typeof(string));
            custreservermdt.Columns.Add("LOCCODE", typeof(string));
            custreservermdt.Columns.Add("RESCODE", typeof(string));
            custreservermdt.Columns.Add("ROOMRATECODE", typeof(string));
            custreservermdt.Columns.Add("ROOMTYPECODE", typeof(string));
            custreservermdt.Columns.Add("ROOMNOCODE", typeof(string));
            custreservermdt.Columns.Add("ROOMPRICE", typeof(decimal));
            custreservermdt.Columns.Add("CHECKINDATE", typeof(string));
            custreservermdt.Columns.Add("CHECKOUTDATE", typeof(string));
            custreservermdt.Columns.Add("GUESTREFCODE", typeof(string));
            custreservermdt.Columns.Add("STS", typeof(string));
            custreservermdt.PrimaryKey = new DataColumn[] { custreservermdt.Columns["ROOMNOCODE"] };
            foreach (DataRow row in roomdetTb.Rows)
            {
                if (Convert.ToInt32(row["roomCount"].ToString()) != 0)
                {
                    string tot = roomdetTb.AsEnumerable()
                                   .Where(y => y.Field<string>("roomtypCode") == row["roomtypCode"].ToString())
                                    .Sum(x => x.Field<int>("roomCount"))
                                           .ToString();
                    DataTable dataTable = reservationDataHandler.getRoomCodeNo(RESCOMPCODE, LOCCODE, SERDET.Rows[0]["CHECKINDATE"].ToString(), DateTime.Parse(SERDET.Rows[0]["CHECKOUTDATE"].ToString()).AddDays(-1).ToString(), row["roomtypCode"].ToString(), tot);

                    for (int i = 0; i <= Convert.ToInt32(row["roomCount"].ToString()) - 1; i++)
                    {
                        for (int j = 0; j < dataTable.Rows.Count; j++)
                        {
                            bool a = custreservermdt.Rows.Contains(dataTable.Rows[j]["ROOMNOCODE"].ToString());
                            if (!a)
                            {
                                if (dataTable.Rows[j]["ROOMTYPECODE"].ToString() == row["roomtypCode"].ToString())
                                {
                                    DataRow dr = custreservermdt.NewRow();
                                    dr["COMPCODE"] = RESCOMPCODE;
                                    dr["LOCCODE"] = LOCCODE;
                                    dr["RESCODE"] = "";
                                    dr["ROOMRATECODE"] = row["roomrtCdID"].ToString();
                                    dr["ROOMTYPECODE"] = row["roomtypCode"].ToString();
                                    dr["ROOMNOCODE"] = dataTable.Rows[j]["ROOMNOCODE"].ToString();
                                    dr["ROOMPRICE"] = Convert.ToDecimal(row["roomrt"]);
                                    dr["CHECKINDATE"] = SERDET.Rows[0]["CHECKINDATE"].ToString();
                                    dr["CHECKOUTDATE"] = SERDET.Rows[0]["CHECKOUTDATE"].ToString();
                                    dr["GUESTREFCODE"] = "";
                                    dr["STS"] = Constants.CON_STATUS_PENDING;

                                    custreservermdt.Rows.Add(dr);
                                    break;
                                }
                            }
                        }
                    }
                }
            }

            return custreservermdt;
        }

        // validate reservaton
        public bool validateReservation(DataTable custreservermdt)
        {
            if (custreservermdt.Rows.Count > 0)
            {
                return true;
            }
            return false;
        }

    }
}