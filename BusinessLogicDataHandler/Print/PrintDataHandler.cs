﻿using BusinessLogicDataHandler.Clog;
using ConnectionDataHandler;
using System;
using System.Data;
using System.Data.SqlClient;

namespace BusinessLogicDataHandler.Print
{
    public class PrintDataHandler 
    {
        public DataTable getdocsforinvoice(string refno, string docsts)
        {

            DataTable SQLDT = new DataTable();

            try
            {

                using (SqlConnection con = new SqlConnection(DataAccess.GetConnectionString()))
                {
                    if (con.State != ConnectionState.Open) { con.Open(); }
                    string SQLString = @" Select MM.idno as idno,MM.doccode as doccode,MM.refno as refno,DT.docname as docname,MM.extn as extn,CASE WHEN MM.extn IS NULL THEN 'N' ELSE 'Y' END as isreceived,CONVERT(varchar(19), MM.endate, 106) as endate " +
                                   " from DOCTYPES DT  left OUTER JOIN " +
                                   " (SELECT idno,refno,endate,extn,doccode " +
                                   " FROM DATAUPLOAD DL  WHERE refno = @refno and docsts = @docsts  ) as MM  ON MM.doccode = DT.doccode ";
                    using (SqlCommand SQLCommand = new SqlCommand(SQLString, con))
                    {
                        using (SqlDataAdapter sqlda = new SqlDataAdapter(SQLCommand))
                        {
                            SQLCommand.Parameters.AddWithValue("@refno", refno);
                            SQLCommand.Parameters.AddWithValue("@docsts", docsts);
                            sqlda.Fill(SQLDT);
                        }
                    }
                }
                
                return SQLDT;
            }
            catch (Exception ex)
            {
                CeegralogDataHandler.LogError(ex);
                throw ex;
            }
        }

        public DataTable getapplicant(string refno, string massts)
        {

            DataTable SQLDT = new DataTable();

            try
            {

                using (SqlConnection con = new SqlConnection(DataAccess.GetConnectionString()))
                {
                    if (con.State != ConnectionState.Open) { con.Open(); }
                    string SQLString = @" SELECT refno,appname,name,nicbr,CONVERT(VARCHAR(19),ISNULL(expdate,GETDATE()),106) as expdate ,remarks " +
                                   " from MASTER MA INNER JOIN APPTYPES AT ON MA.appcode = AT.appcode " +
                                   " where refno =@refno and massts=@massts";
                    using (SqlCommand SQLCommand = new SqlCommand(SQLString, con))
                    {
                        using (SqlDataAdapter sqlda = new SqlDataAdapter(SQLCommand))
                        {
                            SQLCommand.Parameters.AddWithValue("@refno", refno);
                            SQLCommand.Parameters.AddWithValue("@massts", massts);
                            sqlda.Fill(SQLDT);
                        }
                    }
                }
                
                return SQLDT;
            }
            catch (Exception ex)
            {
                CeegralogDataHandler.LogError(ex);
                throw ex;
            }
        }
    }
}