﻿using BusinessLogicDataHandler.Clog;
using ConnectionDataHandler;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace BusinessLogicDataHandler.PaymentGateway
{
    public class PaymentGatewayDataHandler 
    {
        public Dictionary<string, string> getYears()
        {
            int smaxyear = DateTime.Today.AddYears(5).Year;
            int sminyear = DateTime.Today.AddYears(0).Year;
            Dictionary<string, string> yearlist = new Dictionary<string, string>();
            yearlist.Add("", "");
            for (int i = sminyear; i <= smaxyear; i++)
            {
                yearlist.Add(i.ToString(), i.ToString());
            }
            return yearlist;
        }

        public Dictionary<string, string> getMonths()
        {
            Dictionary<string, string> monthlist = new Dictionary<string, string>();
            monthlist.Add("", "");
            monthlist.Add("01", "01");
            monthlist.Add("02", "02");
            monthlist.Add("03", "03");
            monthlist.Add("04", "04");
            monthlist.Add("05", "05");
            monthlist.Add("06", "06");
            monthlist.Add("07", "07");
            monthlist.Add("08", "08");
            monthlist.Add("09", "09");
            monthlist.Add("10", "10");
            monthlist.Add("11", "11");
            monthlist.Add("12", "12");
            return monthlist;
        }

        public DataRow getcompany(string compcode, string sts)
        {
            DataRow dataRow = null;
            DataTable SQLDT = new DataTable();

            try
            {

                using (SqlConnection con = new SqlConnection(DataAccess.GetConnectionString()))
                {
                    if (con.State != ConnectionState.Open) { con.Open(); }
                    string SQLString = " SELECT COMPCODE,COMPNAME from COMPANY where COMPCODE = '" + compcode + "' AND STS = '" + sts + "'";
                    using (SqlCommand SQLCommand = new SqlCommand(SQLString, con))
                    {
                        using (SqlDataAdapter sqlda = new SqlDataAdapter(SQLCommand))
                        {
                            sqlda.Fill(SQLDT);
                            if (SQLDT.Rows.Count > 0)
                            {
                                dataRow = SQLDT.Rows[0];
                            }
                        }
                    }
                }
                
                return dataRow;
            }
            catch (Exception ex)
            {
                CeegralogDataHandler.LogError(ex);
                throw ex;
            }
        }

        public Boolean createCardDetail(string TRNO, string CARDNUMBER, string CARDHOLDERNAME, string CARDEXPIRY, string CARDCVC, string LOCDATE, string STS)
        {
            Boolean isupdate = false;

            try
            {

                using (SqlConnection con = new SqlConnection(DataAccess.GetConnectionString()))
                {
                    if (con.State != ConnectionState.Open) { con.Open(); }
                    using (SqlCommand SQLCommand = new SqlCommand("sp_createpaymentcarddetail", con))
                    {
                        SQLCommand.CommandType = CommandType.StoredProcedure;
                        SQLCommand.Parameters.AddWithValue("@TRNO", TRNO.Trim());
                        SQLCommand.Parameters.AddWithValue("@CARDNUMBER", CARDNUMBER.Trim());
                        SQLCommand.Parameters.AddWithValue("@CARDHOLDERNAME", CARDHOLDERNAME.Trim());
                        SQLCommand.Parameters.AddWithValue("@CARDEXPIRY", CARDEXPIRY.Trim());
                        SQLCommand.Parameters.AddWithValue("@CARDCVC", CARDCVC.Trim());
                        SQLCommand.Parameters.AddWithValue("@LOCDATE", LOCDATE.Trim());
                        SQLCommand.Parameters.AddWithValue("@STS", STS.Trim());
                        SQLCommand.ExecuteNonQuery();
                        SQLCommand.Parameters.Clear();

                        isupdate = true;
                    }
                }


                return isupdate;
            }
            catch (Exception ex)
            {
                CeegralogDataHandler.LogError(ex);
                throw ex;
            }
        }

        public DataRow getmerchantdetails(string COMPCODE, string LOCCODE, string STS, string PAYMENTCODE)
        {
            DataRow dataRow = null;
            DataTable SQLDT = new DataTable();

            try
            {

                using (SqlConnection con = new SqlConnection(DataAccess.GetConnectionString()))
                {
                    if (con.State != ConnectionState.Open) { con.Open(); }
                    string SQLString = " SELECT  MERCHANTID, MERCHANT, CURRCODE from MAP_LOCATION_PAYMENTGATEWAYS " +
                                   "  where COMPCODE = '" + COMPCODE + "' AND LOCCODE = '" + LOCCODE + "' AND PAYMENTCODE = '" + PAYMENTCODE + "'  AND STS = '" + STS + "'";
                    using (SqlCommand SQLCommand = new SqlCommand(SQLString, con))
                    {
                        using (SqlDataAdapter sqlda = new SqlDataAdapter(SQLCommand))
                        {
                            sqlda.Fill(SQLDT);
                            if (SQLDT.Rows.Count > 0)
                            {
                                dataRow = SQLDT.Rows[0];
                            }
                        }
                    }
                }
                
                return dataRow;
            }
            catch (Exception ex)
            {
                CeegralogDataHandler.LogError(ex);
                throw ex;
            }
        }

        public Boolean CreateResResponse(string RESCODE, string action, string maskedaccno, string bankrefid, string currcode, string IPGtransacid, string langcode, string merchantrefid,
            string merchanvar1, string merchanvar2, string merchanvar3, string merchanvar4, string transactionamt, string transactionsts, string failedreason, string cardholname)
        {
            Boolean isupdate = false;

            try
            {

                using (SqlConnection con = new SqlConnection(DataAccess.GetConnectionString()))
                {
                    if (con.State != ConnectionState.Open) { con.Open(); }
                    using (SqlCommand SQLCommand = new SqlCommand("sp_createresresponse", con))
                    {
                        SQLCommand.CommandType = CommandType.StoredProcedure;
                        SQLCommand.Parameters.AddWithValue("@RESCODE", RESCODE.Trim());
                        SQLCommand.Parameters.AddWithValue("@RESMERCHANTID", merchantrefid.Trim());
                        SQLCommand.Parameters.AddWithValue("@RESIGPTRNID", IPGtransacid.Trim());
                        SQLCommand.Parameters.AddWithValue("@BANKREFID", bankrefid.Trim());
                        SQLCommand.Parameters.AddWithValue("@MASKEDACCNO", maskedaccno.Trim());
                        SQLCommand.Parameters.AddWithValue("@TRAMOUNT", transactionamt.Trim());
                        SQLCommand.Parameters.AddWithValue("@LANGCODE", langcode.Trim());
                        SQLCommand.Parameters.AddWithValue("@TRACTIONTAG", action.Trim());
                        SQLCommand.Parameters.AddWithValue("@CARDHOLDERNAME", cardholname.Trim());
                        SQLCommand.Parameters.AddWithValue("@TRSTATUS", transactionsts.Trim());
                        SQLCommand.Parameters.AddWithValue("@CURRCODE", currcode.Trim());
                        SQLCommand.Parameters.AddWithValue("@RESVAR1", merchanvar1.Trim());
                        SQLCommand.Parameters.AddWithValue("@RESVAR2", merchanvar2.Trim());
                        SQLCommand.Parameters.AddWithValue("@RESVAR3", merchanvar3.Trim());
                        SQLCommand.Parameters.AddWithValue("@RESVAR4", merchanvar4.Trim());
                        SQLCommand.Parameters.AddWithValue("@FAILEDREASON", failedreason.Trim());

                        SQLCommand.ExecuteNonQuery();
                        SQLCommand.Parameters.Clear();

                        isupdate = true;
                    }
                }

                return isupdate;
            }
            catch (Exception ex)
            {
                CeegralogDataHandler.LogError(ex);
                throw ex;
            }
        }
    }
}