﻿using System;

namespace BusinessLogicDataHandler.PaymentGateway
{
    public class NTBPaymentGatewayDataHandler
    {
        private string PTInvoice = "";
        private string EncryptedInvoice = "";
        private int iTransType = 0;
        private string MerchantID = "";
        private string transcationamnt = "";
        private string currencycode = "";
        private string rescode = "";
        private string languagecode = "";
        private string returnurl = "";
        private string merchantvar1 = "";
        private string merchantvar2 = "";
        private string merchantvar3 = "";
        private string merchantvar4 = "";
        private string IPGServer = "";
        private string IPGClientService_IP = "";
        private string IPGClientService_Port = "";

        public NTBPaymentGatewayDataHandler(int iTransType, string MerchantID, decimal transcationamnt, string currencycode, string rescode, string languagecode, string returnurl,
            string merchantvar1, string merchantvar2, string merchantvar3, string merchantvar4, string IPGClientService_IP, string IPGClientService_Port)
        {
            this.currencycode = currencycode;
            this.iTransType = iTransType;
            this.MerchantID = MerchantID;
            this.transcationamnt = transcationamnt.ToString();
            this.rescode = rescode;
            this.languagecode = languagecode;
            this.returnurl = returnurl;
            this.merchantvar1 = merchantvar1;
            this.merchantvar2 = merchantvar2;
            this.merchantvar3 = merchantvar3;
            this.merchantvar4 = merchantvar4;
            this.IPGClientService_IP = IPGClientService_IP;
            this.IPGClientService_Port = IPGClientService_Port;
        }

        public string EncryptData()
        {
            string uri = "";
            PTInvoice = "<req>";
            PTInvoice += "<mer_id>" + this.MerchantID + "</mer_id>";
            PTInvoice += "<mer_txn_id>" + this.rescode + "</mer_txn_id>";
            PTInvoice += "<action>SaleTxnGroup</action>";
            PTInvoice += "<txn_amt>" + this.transcationamnt + "</txn_amt>";
            PTInvoice += "<cur>" + this.currencycode + "</cur>";
            PTInvoice += "<lang>" + this.languagecode + "</lang>";

            PTInvoice += "<ret_url>" + "http://" + this.IPGClientService_IP + ":" + this.IPGClientService_Port + this.returnurl + "</ret_url>";

            if (this.merchantvar1.Trim().Length > 0)
                PTInvoice += "<mer_var1>" + this.merchantvar1 + "</mer_var1>";

            if (this.merchantvar2.Trim().Length > 0)
                PTInvoice += "<mer_var2>" + this.merchantvar2 + "</mer_var2>";

            if (this.merchantvar3.Trim().Length > 0)
                PTInvoice += "<mer_var3>" + this.merchantvar3 + "</mer_var3>";

            if (this.merchantvar4.Trim().Length > 0)
                PTInvoice += "<mer_var4>" + this.merchantvar4 + "</mer_var4>";

            PTInvoice += "</req>";

            try
            {
                IShroff objIShroff = new IShroff();

                //Un comment and edit below 2 lines if your security keys are present in a custom folder
                //other than <iPay Client Service>\keys

                //string strKeySet_1 = WebConfigurationManager.AppSettings["IPGKeyPath_1"];
                //objIShroff.setSecurityKeysPath(strKeySet_1);

                bool bResult = objIShroff.setPlainTextInvoice(PTInvoice);
                if (bResult)
                {
                    EncryptedInvoice = objIShroff.getEncryptedInvoice();
                    uri = "?txtPlainInvoice=" + PTInvoice + "&encryptedInvoicePay=" + EncryptedInvoice;
                }
                else
                {
                }
            }
            catch (Exception Ex)
            {
            }

            return uri;
        }
    }
}