﻿using ConstantsDataHandler;
using System;

namespace ISECBENG.Main
{
    public partial class logout : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                lblcopyright.Text = "Copyright © " + DateTime.Today.Year.ToString() + " " + Constants.CON_OWNER_NAME + " All Rights Reserved ";
                getmessgae();
            }

            //    public static String SYS_SESSION_LOCSYSDATE = "KEY-LOCSYSDATE";
            //public static String SYS_SESSION_LOGSTS = "KEY-LOGSTS";
            //public static String SYS_SESSION_COMPANYSHORTCODE = "KEY-COMPANYSHORTCODE";

            Session[Constants.SYS_SESSION_LOGSTS] = "";
            Session[Constants.SYS_SESSION_LOCSYSDATE] = "";
            Session[Constants.SYS_SESSION_USERINPUTINFO] = "";
            Session[Constants.SYS_SESSION_SELECTEDRMTB] = "";
            Session[Constants.SYS_SESSION_ROOMTB] = "";
            Session[Constants.SYS_SESSION_CUSTRESTB] = "";
            Session[Constants.SYS_SESSION_TAXTABLE] = "";
            Session.Abandon();
        }

        private void getmessgae()
        {
            txterrmessage.Text = "You have been successfully logged out!";
        }

        protected void btnlogin_Click(object sender, EventArgs e)
        {
            Response.Redirect("/Main/login.aspx", false);
        }
    }
}