﻿<%@ page language="C#" autoeventwireup="true" codebehind="logout.aspx.cs" inherits="ISECBENG.Main.logout" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Secvision - Hotel Reservation System</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Design & Developed by Secvision">
    <link href="../Styles/StyleMain.css" rel="stylesheet" type="text/css" />
    <link href="../Styles/StyleError.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript">
        function CloseWindow() {
            document.forms[0].target = "_blank";
            window.close();
        }
    </script>
</head>
<body style="margin: 0px">
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server">
        </asp:ScriptManager>
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
                <div class="logoarea">
                    <img src="../Images/Icons/product.png" alt="" />
                </div>
                <div class="errorbanner">
                    <asp:Label ID="lblerrmessage" runat="server" Visible="False"></asp:Label>
                </div>
                <div class="cracccontainer">
                    <div class="errorbox">
                        <asp:ValidationSummary ID="vssummary" runat="server" ValidationGroup="vgsum" CssClass="valsummary" />
                    </div>
                    <div class="craccbox">
                        <div class="craccboxtop">
                            <img src="../Images/Icons/users.png" alt="" style="vertical-align: middle" />
                            Logout
                        </div>
                        <div class="craccboxmiddle">
                            <div class="errorpage">
                                <asp:TextBox ID="txterrmessage" runat="server" TextMode="MultiLine"
                                    CssClass="errortextmsg" ReadOnly="True"></asp:TextBox>
                                <p>
                                    <asp:Button ID="btnlogin" runat="server" Text="Back to Login Page" CssClass="btn"
                                        OnClick="btnlogin_Click" />
                                    <asp:Button ID="btnexit" runat="server" Text="Exit" CssClass="btn" OnClientClick="CloseWindow()" />
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                <div align="center">
                    <asp:UpdateProgress ID="UpdateProgress1" runat="server">
                        <ProgressTemplate>
                            <img src="../Images/Icons/Loading.gif" alt="" />
                        </ProgressTemplate>
                    </asp:UpdateProgress>
                </div>
                <div class="footer">
                    <asp:Label ID="lblcopyright" runat="server" Text=""></asp:Label>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </form>
</body>
</html>