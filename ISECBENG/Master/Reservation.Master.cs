﻿using BusinessLogicDataHandler.Clog;
using BusinessLogicDataHandler.Reservation;
using ConstantsDataHandler;
using System;
using System.Configuration;
using System.Data;

namespace ISECBENG.Master
{
    public partial class Reservation : System.Web.UI.MasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                loadStyleSheet();
                getCompanyinfo();
                getOwnerinfo();
            }
        }

        private void getOwnerinfo()
        {
            lblcompany.Text = "@ copyright " + DateTime.Today.Year.ToString() + " " + Constants.CON_OWNER_NAME;
            lblproduct.Text = Constants.CON_SYSTEM_NAME;
            hlwebsite.NavigateUrl = ConfigurationManager.AppSettings["WebsiteKey"];
        }

        private void getCompanyinfo()
        {
            ReservationDataHandler eReservationDataHandler = new ReservationDataHandler();

            try
            {
                string COMPCODE = (string)Session[Constants.SYS_SESSION_COMPANYCODE];
                string LOCCODE = (string)Session[Constants.SYS_SESSION_LOCATIONCODE];
                DataRow dtdis = eReservationDataHandler.getcompany(COMPCODE, Constants.CON_STATUS_ACTIVE);
                if (dtdis != null)
                {
                    lblcompanyname.Text = dtdis["COMPNAME"].ToString();
                    lbllocation.Text = dtdis["COMPNAME"].ToString();
                    lbladdress.Text = dtdis["COMPADDRESS1"].ToString() + ", " + dtdis["COMPADDRESS2"].ToString() + ", " + dtdis["COMPADDRESS3"].ToString() + ", " + dtdis["COUNTRY"].ToString();
                    lblweb.Text = dtdis["WEBSITE"].ToString();
                    lblemail.Text = dtdis["EMAIL"].ToString();
                    lblhotline.Text = dtdis["HOTLINE"].ToString();
                    lblfax.Text = dtdis["FAX"].ToString();
                    imglogo.ImageUrl = dtdis["LOGOPATH"].ToString();

                    DataRow dtlocdis = eReservationDataHandler.getLocationDetails(COMPCODE, LOCCODE, Constants.CON_STATUS_ACTIVE);
                    if (dtlocdis != null)
                    {
                        lbltitle.Text = dtlocdis["DESCRIP"].ToString();
                        lbllocation.Text = dtlocdis["LOCATION"].ToString();
                        lbladdress.Text = dtlocdis["LOCADDRESS1"].ToString() + ", " + dtlocdis["LOCADDRESS2"].ToString() + ", " + dtlocdis["LOCADDRESS3"].ToString() + ", " + dtlocdis["COUNTRY"].ToString();
                        lblemail.Text = dtlocdis["EMAIL"].ToString();
                        lblhotline.Text = dtlocdis["HOTLINE"].ToString();
                        lblfax.Text = dtlocdis["FAX"].ToString();
                    }
                }
                else
                {
                    lbllocation.Text = "";
                    lbladdress.Text = "";
                    lblfax.Text = "";
                    lblhotline.Text = "";
                    lblweb.Text = "";
                    lblemail.Text = "";
                    lbltitle.Text = "";
                }
            }
            catch (Exception ex)
            {
                CeegralogDataHandler.LogError(ex);
                throw ex;
            }
            finally
            {
                eReservationDataHandler = null;
            }
        }

        private void loadStyleSheet()
        {
            string LOCCODE = (string)Session[Constants.SYS_SESSION_LOCATIONCODE];
            lnkResStyleSheet.Href = Constants.CON_STYLESHEET_PATH + LOCCODE + ".css";
        }
    }
}