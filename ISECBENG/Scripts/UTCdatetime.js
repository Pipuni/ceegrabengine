function getUTCdatetime() {
    var today = new Date();
    var utcatetimeString = today.getFullYear() + "-" + appendLeadingZeroes(today.getMonth() + 1) + "-" + appendLeadingZeroes(today.getDate()) + " " + appendLeadingZeroes(today.getHours()) + ":" + appendLeadingZeroes(today.getMinutes()) + ":" + appendLeadingZeroes(today.getSeconds()) + ":" + appendLeadingZeroes(today.getMilliseconds());
    return utcatetimeString;
}

function appendLeadingZeroes(n) {
    if (n <= 9) {
        return "0" + n;
    }
    return n
}