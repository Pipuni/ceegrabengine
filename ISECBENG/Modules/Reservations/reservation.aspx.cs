﻿// Author - Agarshan
using BusinessLogicDataHandler.PaymentGateway;
using BusinessLogicDataHandler.Reservation;
using ConstantsDataHandler;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ISECBENG.Modules.Reservations
{
    public partial class reservation : System.Web.UI.Page
    {
        private DataTable selectedRoomTb;
        private DataTable selectedexoptTb;
        private static string RESCOMPCODE = "";
        public string GatewayUrl = "";

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                string COMPCODE = "CP200000021";
                string LOCCODE = "AAALALC0017";
                //string COMPCODE = Request.QueryString["COMPCODE"].ToString();
                //string LOCCODE = Request.QueryString["LOCCODE"].ToString();

                Session[Constants.SYS_SESSION_SELECTEDRMTB] = null;
                Session[Constants.SYS_SESSION_SELECTEDEXOPTRMTB] = null;
                Session[Constants.SYS_SESSION_COMPANYCODE] = COMPCODE.ToString();
                Session[Constants.SYS_SESSION_LOCATIONCODE] = LOCCODE.ToString();

                getlocations();
                getPropertypageInfo();
                bindImageSlider();
            }
            demo.Attributes.Add("readonly", "readonly");
        }

        // get the guest details when the NIC is typed
        //? IS this needed? - remove once the development is done
        private void getGuestDetails()
        {
            ReservationDataHandler eReservationDataHandler = new ReservationDataHandler();

            try
            {
                string NICPP = txtppno.Text.ToString();
                DataRow dtr = eReservationDataHandler.getguestDetails(NICPP);
                if (dtr != null)
                {
                    txtppno.Text = dtr["NICPP"].ToString();
                    txtemail.Text = dtr["EMAIL"].ToString();
                    txtfirstname.Text = dtr["FIRSTNAME"].ToString();
                    txtlastname.Text = dtr["LASTNAME"].ToString();
                    txtmobile.Text = dtr["CONTACTNO"].ToString();
                    ddlcountry.SelectedValue = dtr["COUNTRY"].ToString();
                    ddltitle.SelectedValue = dtr["TITLE"].ToString();
                }
            }
            catch (Exception ex)
            {
                Constants.CON_MESSAGE_STRING = ex.Message;
                SysErrors.AppError.PopulateError(Constants.MESSAGE_WARNING, Constants.CON_MESSAGE_STRING, lblerrmessage);
            }
            finally
            {
                eReservationDataHandler = null;
            }
        }

        // get propery page info - code by thanuja
        private void getPropertypageInfo()
        {
            ReservationDataHandler eReservationDataHandler = new ReservationDataHandler();
            ReservationValidationDataHandler reservationValidationDataHandler = new ReservationValidationDataHandler();
            DataTable dtpage = new DataTable();
            pnlstickyarea.Visible = false;
            try
            {
                ltlpagenote.Text = "";
                ltlpageremark.Text = "";
                string COMPCODE = (string)Session[Constants.SYS_SESSION_COMPANYCODE];
                string LOCCODE = (string)Session[Constants.SYS_SESSION_LOCATIONCODE];
                dtpage = eReservationDataHandler.getPropertypageInfo(COMPCODE, LOCCODE, Constants.CON_STATUS_ACTIVE);

                if (dtpage.Rows.Count > 0)
                {
                    pnlstickyarea.Visible = true;
                    string[] strdata = reservationValidationDataHandler.Assignpropertyinfo(dtpage);
                    ltlpagenote.Text = strdata[0].ToString();
                    ltlpageremark.Text = strdata[1].ToString();
                    ltlpageinfo.Text = strdata[2].ToString();
                }
            }
            catch (Exception ex)
            {
                Constants.CON_MESSAGE_STRING = ex.Message;
                SysErrors.AppError.PopulateError(Constants.MESSAGE_WARNING, Constants.CON_MESSAGE_STRING, lblerrmessage);
            }
            finally
            {
                eReservationDataHandler = null;
                reservationValidationDataHandler = null;
            }
        }

        //to get the company loctaions
        private void getlocations()
        {
            ReservationDataHandler eReservationDataHandler = new ReservationDataHandler();
            DataTable dtdis = new DataTable();
            ListItem objlistItem = null;
            ddllocations.Items.Clear();

            try
            {
                objlistItem = new ListItem();
                objlistItem.Text = Constants.CON_NULL_TEXT;
                objlistItem.Value = Constants.CON_NULL_VALUE;
                ddllocations.Items.Add(objlistItem);

                RESCOMPCODE = (string)Session[Constants.SYS_SESSION_COMPANYCODE];
                dtdis = eReservationDataHandler.getCompanylocations(RESCOMPCODE, Constants.CON_STATUS_ACTIVE);
                for (int i = 0; i < dtdis.Rows.Count; i++)
                {
                    objlistItem = new ListItem();
                    objlistItem.Text = dtdis.Rows[i]["LOCATION"].ToString();
                    objlistItem.Value = dtdis.Rows[i]["LOCSYSDATE"].ToString();
                    ddllocations.Items.Add(objlistItem);
                }
            }
            catch (Exception ex)
            {
                Constants.CON_MESSAGE_STRING = ex.Message;
                SysErrors.AppError.PopulateError(Constants.MESSAGE_WARNING, Constants.CON_MESSAGE_STRING, lblerrmessage);
            }
            finally
            {
                dtdis.Dispose();
                dtdis = null;
                eReservationDataHandler = null;
            }
        }

        // Search button clicked
        // Retrieved room information for the particular location is set to a datatable and bind to the gridview
        // Calendar validation is done to restrict selecting past dates.
        // TODO:  validate calendar to select minmum 2 days
        // Get room facilities, refine and bind it with the room information
        protected void btnadd_Click(object sender, EventArgs e)
        {
            Session[Constants.SYS_SESSION_SELECTEDRMTB] = null;
            Session[Constants.SYS_SESSION_SELECTEDEXOPTRMTB] = null;
            roomSelectedGV.Visible = false;
            roomReservationGV.DataSource = "";
            totalAmntId.Visible = false;
            totalAmntId.Text = "";
            totalamountTaxLabel.Visible = false;
            custInfoDisplay.Visible = false;
            pnlstickyarea.Visible = false;
            pnlratedetails.Visible = false;
            SysErrors.AppError.ClearError(lblerrmessage);
            DataTable dtres = new DataTable();

            dtres.Columns.Add("COMPCODE", typeof(string));
            dtres.Columns.Add("LOCCODE", typeof(string));
            dtres.Columns.Add("CHECKINDATE", typeof(string));
            dtres.Columns.Add("CHECKOUTDATE", typeof(string));
            dtres.Columns.Add("NOOFDAYS", typeof(string));
            dtres.Columns.Add("NOOFROOMS", typeof(string));
            dtres.Columns.Add("ADULTS", typeof(string));
            dtres.Columns.Add("CHILDREN", typeof(string));
            Session[Constants.SYS_SESSION_USERINPUTINFO] = null;

            ReservationDataHandler reservationDataHandler = new ReservationDataHandler();
            ReservationValidationDataHandler reservationValidationDataHandler = new ReservationValidationDataHandler();

            try
            {
                string[] LOCSYSDATE = ddllocations.SelectedItem.Value.ToString().Split('|');
                string LOCCODE = LOCSYSDATE[0].ToString();
                string CHECKINDATE = hffromdate.Value.ToString();
                string CHECKOUTDATE = hftodate.Value.ToString();
                string NOOFDAYS = (DateTime.Parse(CHECKOUTDATE) - DateTime.Parse(CHECKINDATE)).TotalDays.ToString();
                string NOOFROOMS = txtrooms.Text.ToString();
                string ADULTS = txtadults.Text.ToString();
                string CHILDREN = txtchildren.Text.ToString();

                DateTime CHECKINDT = DateTime.Parse(CHECKINDATE);
                DateTime CHECKOUTDT = DateTime.Parse(CHECKOUTDATE);
                DateTime TDDT = DateTime.Now;

                if (TDDT.Date > CHECKINDT.Date || TDDT.Date > CHECKOUTDT.Date)
                {
                    SysErrors.AppError.PopulateError(Constants.MESSAGE_WARNING, "Please Select Future Dates.", lblerrmessage);
                    return;
                }
                DataRow dr = dtres.NewRow();
                dr["COMPCODE"] = RESCOMPCODE;
                dr["LOCCODE"] = LOCCODE;
                dr["CHECKINDATE"] = CHECKINDATE;
                dr["CHECKOUTDATE"] = CHECKOUTDATE;
                dr["NOOFDAYS"] = NOOFDAYS;
                dr["NOOFROOMS"] = NOOFROOMS;
                dr["ADULTS"] = ADULTS;
                dr["CHILDREN"] = CHILDREN;
                dtres.Rows.Add(dr);
                Session[Constants.SYS_SESSION_USERINPUTINFO] = dtres;

                DataTable roomdt = new DataTable();
                DataTable roomfacdt = new DataTable();

                roomdt = reservationDataHandler.getavlroomsnratesondate(RESCOMPCODE, LOCCODE, CHECKINDATE, CHECKOUTDATE);
                roomfacdt = reservationDataHandler.getroomfactdetails(RESCOMPCODE, LOCCODE);

                roomdt.Columns.Add("ROOMFACILITY");
                roomdt.Columns.Add("TOTAVAILABLEROOMS");
                roomdt.Columns.Add("TOTAVAILABLEROOMDESCRIP");

                Session[Constants.SYS_SESSION_PREFCURRENCY] = roomdt.Rows[0]["PREFCURRENCY"].ToString();

                roomdt = reservationValidationDataHandler.Refineroomsdt(roomdt, roomfacdt);

                roomReservationGV.DataSource = roomdt;
                pnlroomDetail.Visible = true;
                roomReservationGV.DataBind();
            }
            catch (Exception ex)
            {
                Constants.CON_MESSAGE_STRING = ex.Message;
                SysErrors.AppError.PopulateError(Constants.MESSAGE_WARNING, Constants.CON_MESSAGE_STRING, lblerrmessage);
            }
            finally
            {
                dtres.Dispose();
                dtres = null;
                reservationValidationDataHandler = null;
                reservationDataHandler = null;
                //eRoomsDataHandler = null;
                ScriptManager.RegisterClientScriptBlock(this.Page, typeof(Page), "script", "window.setTimeout(function() { document.getElementById('" + lblerrmessage.ClientID + "').style.display = 'none' },5000);", true);
            }
        }

        // on room is selected
        // method is called to set the summary of the selected room inforamtion
        protected void ddlTotRooms_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            DataTable roomdetTb = new DataTable();

            try
            {
                roomdetTb = setroomTableDetails();
                GridViewRow gvr = (GridViewRow)(((Control)sender).NamingContainer);
                setSelectedRoomTableDetails(gvr, roomdetTb);
            }
            catch (Exception ex)
            {
                Constants.CON_MESSAGE_STRING = ex.Message;
                SysErrors.AppError.PopulateError(Constants.MESSAGE_WARNING, Constants.CON_MESSAGE_STRING, lblerrmessage);
            }
            finally
            {
                roomdetTb.Dispose();
                roomdetTb = null;
            }
        }

        // on room is selected the room details is captured and bind to the datatable to show it as a summary grid
        // TODO: each selection should identified according to the meal plan
        // now the selection is identified according to the room types
        private void setSelectedRoomTableDetails(GridViewRow gvr, DataTable roomdetTb)
        {
            ReservationValidationDataHandler reservationValidationDataHandler = new ReservationValidationDataHandler();

            DataTable SERDET = (DataTable)Session[Constants.SYS_SESSION_USERINPUTINFO];

            // room details user selected from the drop down list
            HyperLink selectedroomimage = (HyperLink)gvr.FindControl("popupRmImg");
            DropDownList selectedroomNum = (DropDownList)gvr.FindControl("ddlTotRooms");
            Label selectedroomtype = (Label)gvr.FindControl("roomtypeID");
            Label roomtypeCd = (Label)gvr.FindControl("roomtypeCodeID");
            Label selectedroomAvNo = (Label)gvr.FindControl("roomAvNoID");
            Label roomrtCdID = (Label)gvr.FindControl("roomrtCdID");
            Label roomexoptrt = (Label)gvr.FindControl("lblexrmrate");
            Label TaxCode = (Label)gvr.FindControl("taxCodeID");
            Label maxoccupancy = (Label)gvr.FindControl("maxoccupancyId");
            Label selectedroomrt = (Label)gvr.FindControl("roomrtID");
            Label lblexrmrate = (Label)gvr.FindControl("lblexrmrate");
            Label roomratecode = (Label)gvr.FindControl("roomrtCdID");
            Button EXOPTBUTTON = (Button)gvr.FindControl("btnShow");

            // show room ex option button
            if (selectedroomNum.SelectedValue.Equals("0"))
            {
                EXOPTBUTTON.Visible = false;
            }
            else
            {
                EXOPTBUTTON.Visible = true;
            }

            // get total selected room count
            int totalroom = roomdetTb.AsEnumerable()
            .Sum(x => x.Field<int>("roomCount"));

            // validate room selection according to the user input info
            validateRoomSelection(roomdetTb, roomrtCdID.Text, selectedroomNum, totalroom, EXOPTBUTTON);

            // if the selected room data table is null or if its in the session
            if ((DataTable)Session[Constants.SYS_SESSION_SELECTEDRMTB] == null)
            {
                roomSelectedGV.Visible = true;
                totalAmntId.Text = "";
                totalAmntId.Visible = true;
                totalamountTaxLabel.Visible = true;
                selectedRoomTb = new DataTable();
                selectedRoomTb.Columns.Add("selectRmTyp", typeof(string));
                selectedRoomTb.Columns.Add("selectRmTypcd", typeof(string));
                selectedRoomTb.Columns.Add("selectRoomCount", typeof(int));
                selectedRoomTb.Columns.Add("maxNumReach", typeof(string));
                selectedRoomTb.Columns.Add("maxoccupancy", typeof(int));
                selectedRoomTb.Columns.Add("selectRoomrt", typeof(double));
                selectedRoomTb.Columns.Add("roomrtwithtx", typeof(double));
                selectedRoomTb.Columns.Add("roomrtwithouttx", typeof(double));
                selectedRoomTb.Columns.Add("selectroomratecd", typeof(string));
                selectedRoomTb.PrimaryKey = new DataColumn[] { selectedRoomTb.Columns["selectroomratecd"] };
            }
            else
            {
                selectedRoomTb = (DataTable)Session[Constants.SYS_SESSION_SELECTEDRMTB];
            }

            // add the newly selected details to the selected room info summary
            selectedRoomTb = reservationValidationDataHandler.setselectedRoomTable(roomdetTb, selectedRoomTb, SERDET, selectedroomtype.Text,
            selectedroomAvNo.Text, roomtypeCd.Text, selectedroomNum.SelectedItem.Value.ToString(), maxoccupancy.Text, selectedroomrt.Text, roomexoptrt.Text, TaxCode.Text, roomratecode.Text);

            // if the selected room count is zero, hide the rate panel details
            if (selectedRoomTb.Rows.Count == 0)
            {
                pnlratedetails.Visible = false;
            }
            else
            {
                pnlratedetails.Visible = true;
            }
            // add the selected room info to the session
            Session[Constants.SYS_SESSION_SELECTEDRMTB] = selectedRoomTb;

            // bind the data table to the grid view
            roomSelectedGV.DataSource = selectedRoomTb;
            roomSelectedGV.DataBind();

            // calucalate total amount
            calculateamount();
        }

        // Calculate total amount
        private void calculateamount()
        {
            ReservationValidationDataHandler reservationValidationDataHandler = new ReservationValidationDataHandler();
            custInfoDisplay.Visible = false;

            string[] LOCSYSDATE = ddllocations.SelectedItem.Value.ToString().Split('|');
            string LOCCODE = LOCSYSDATE[0].ToString();

            pnlratedetails.Visible = true;

            int totrooms = selectedRoomTb.AsEnumerable()
            .Sum(x => x.Field<int>("selectRoomCount"));
            Session[Constants.SYS_SESSION_SELECTEDRMTB] = selectedRoomTb;
            roomSelectedGV.DataSource = selectedRoomTb;
            roomSelectedGV.DataBind();
            if (totrooms > 0)
            {
                DataTable taxTb = reservationValidationDataHandler.calculatetotamount(selectedRoomTb, RESCOMPCODE, LOCCODE);
                string totamnt = (from DataRow taxrw in taxTb.Rows
                                  where (string)taxrw["TAXTYPE"] == Constants.CON_TAX_NETT_RW
                                  select (double)taxrw["TAXAMOUNT"]).FirstOrDefault().ToString();
                totalAmntId.Text = String.Format("{0:n}", Convert.ToDecimal(totamnt)) + " " + (string)Session[Constants.SYS_SESSION_PREFCURRENCY] + " ";
                Session[Constants.SYS_SESSION_TAXTABLE] = taxTb;
                roomSelectedGV.Visible = true;
                totalAmntId.Visible = true;
                totalamountTaxLabel.Visible = true;
            }
            else
            {
                roomSelectedGV.Visible = false;
                totalAmntId.Text = "";
                totalAmntId.Visible = false;
                totalamountTaxLabel.Visible = false;
            }
        }

        // retrieve all the information of the selected rooms from the grid
        private DataTable setroomTableDetails()
        {
            DataTable roomdetTb = new DataTable();
            roomdetTb.Columns.Add("roomtyp", typeof(string));
            roomdetTb.Columns.Add("roomAvNo", typeof(int));
            roomdetTb.Columns.Add("roomCount", typeof(int));
            roomdetTb.Columns.Add("roomtypcode", typeof(string));
            roomdetTb.Columns.Add("roomrt", typeof(double));
            roomdetTb.Columns.Add("roomexoptrt", typeof(double));
            roomdetTb.Columns.Add("roomrtCdID", typeof(string));
            roomdetTb.Columns.Add("maximumoccupancy", typeof(int));
            roomdetTb.Columns.Add("taxCode", typeof(string));
            roomdetTb.PrimaryKey = new DataColumn[] { roomdetTb.Columns["roomrtCdID"] };

            Session[Constants.SYS_SESSION_ROOMTB] = null;
            foreach (GridViewRow row in roomReservationGV.Rows)
            {
                DropDownList duty = (DropDownList)row.FindControl("ddlTotRooms");
                Label roomtype = (Label)row.FindControl("roomtypeID");
                Label roomtypeCd = (Label)row.FindControl("roomtypeCodeID");
                Label roomrt = (Label)row.FindControl("roomrtID");
                Label roomexoptrt = (Label)row.FindControl("lblexrmrate");
                Label roomAvNo = (Label)row.FindControl("roomAvNoID");
                Label roomrtCdID = (Label)row.FindControl("roomrtCdID");
                Label taxCode = (Label)row.FindControl("taxCodeID");
                Label maxoccupancy = (Label)row.FindControl("maxoccupancyId");

                DataRow dtrow = roomdetTb.NewRow();
                dtrow["roomtyp"] = roomtype.Text;
                dtrow["roomtypcode"] = roomtypeCd.Text;
                dtrow["roomAvNo"] = roomAvNo.Text;
                dtrow["roomrtCdID"] = roomrtCdID.Text;
                dtrow["roomrt"] = Convert.ToDouble(roomrt.Text);
                dtrow["roomexoptrt"] = Convert.ToDouble(roomexoptrt.Text);
                dtrow["roomCount"] = duty.SelectedItem.Value.ToString();
                dtrow["Taxcode"] = taxCode.Text.ToString();
                dtrow["maximumoccupancy"] = maxoccupancy.Text.ToString();

                roomdetTb.Rows.Add(dtrow);
            }
            Session[Constants.SYS_SESSION_ROOMTB] = roomdetTb;
            return roomdetTb;
        }

        // on room is selected set the no of nights to the selected room summary grid
        protected void roomSelectedGV_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Header)
            {
                DataTable SERDET = (DataTable)Session[Constants.SYS_SESSION_USERINPUTINFO];
                e.Row.Cells[2].Text = "RATE FOR " + SERDET.Rows[0]["NOOFDAYS"].ToString().Trim() + " NIGHTS";
            }
        }

        // validate how many rooms are allowed to select from the room information table according the number of rooms user input
        //
        private void validateRoomSelection(DataTable roomdetTb, string roomrtCdIDtxt, DropDownList selectedroomNum, int totalroom, Button EXOPTBUTTON)
        {
            DataTable SERDET = (DataTable)Session[Constants.SYS_SESSION_USERINPUTINFO];

            if (Convert.ToInt32(SERDET.Rows[0]["NOOFROOMS"].ToString()) < totalroom)
            {
                DataRow fdchngeRow = roomdetTb.Rows.Find(roomrtCdIDtxt);
                fdchngeRow["roomCount"] = "0";
                selectedroomNum.SelectedValue = "0";
                EXOPTBUTTON.Visible = false;
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('You are allowed to select only " + SERDET.Rows[0]["NOOFROOMS"].ToString() + " rooms')", true);
            }
        }

        // on reserve button is clicked
        // validation is done for maximum occupancy, no of rooms selected.
        // country details is retrived to let the user to select the country
        // payment options to the particular location is retrieved
        // tax table is retrieved from the session and bind the to the tax table grid
        protected void reserveBtn_Click(object sender, EventArgs e)
        {
            ReservationValidationDataHandler reservationValidationDataHandler = new ReservationValidationDataHandler();
            DataTable selectedRmtb = (DataTable)Session[Constants.SYS_SESSION_SELECTEDRMTB];
            DataTable roomdetTb = (DataTable)Session[Constants.SYS_SESSION_ROOMTB];
            DataTable SERDET = (DataTable)Session[Constants.SYS_SESSION_USERINPUTINFO];

            try
            {
                if (roomdetTb != null)
                {
                    int totalroom = roomdetTb.AsEnumerable()
                    .Sum(x => x.Field<int>("roomCount"));

                    bool reservevalid = getreserveValidation(totalroom, selectedRmtb, SERDET);

                    if (reservevalid)
                    {
                        string[] LOCSYSDATE = ddllocations.SelectedItem.Value.ToString().Split('|');
                        string LOCCODE = LOCSYSDATE[0].ToString();
                        DataTable custreservermdt = reservationValidationDataHandler.setRoomCodeAllocation(roomdetTb, SERDET, LOCCODE, RESCOMPCODE);
                        Session[Constants.SYS_SESSION_CUSTRESTB] = custreservermdt;
                        setCountryDetails();
                        setpaymentoptions();
                        lblprefcurrency.Text = (string)Session[Constants.SYS_SESSION_PREFCURRENCY];
                        custInfoDisplay.Visible = true;
                        DataTable taxtable = (DataTable)Session[Constants.SYS_SESSION_TAXTABLE];

                        foreach (DataRow taxrw in taxtable.Rows)
                        {
                            if (Convert.ToDouble(taxrw["TAXAMOUNT"]) == 0)
                            {
                                taxrw.Delete();
                            }
                        }
                        taxtable.AcceptChanges();
                        taxtableGV.DataSource = taxtable;
                        taxtableGV.DataBind();
                    }
                }
                else
                {
                    Constants.CON_MESSAGE_STRING = "Select Rooms to Reserve ";
                    SysErrors.AppError.PopulateError(Constants.MESSAGE_WARNING, Constants.CON_MESSAGE_STRING, lblerrmessage);
                }
            }
            catch (Exception ex)
            {
                Constants.CON_MESSAGE_STRING = ex.Message;
                SysErrors.AppError.PopulateError(Constants.MESSAGE_WARNING, Constants.CON_MESSAGE_STRING, lblerrmessage);
            }
            finally
            {
                roomdetTb = null;
                selectedRmtb = null;
                SERDET = null;
                reservationValidationDataHandler = null;
            }
        }

        // set airport pick up details that are available to the selected location
        private void setairportpickup()
        {
            ReservationDataHandler reservationDataHandler = new ReservationDataHandler();
            DataTable airportpickupdetails = new DataTable();
            try
            {
                string COMPCODE = (string)Session[Constants.SYS_SESSION_COMPANYCODE];
                string LOCCODE = (string)Session[Constants.SYS_SESSION_LOCATIONCODE];
                airportpickupdetails = reservationDataHandler.getairportpickupoptions(COMPCODE, LOCCODE, Constants.CON_STATUS_ACTIVE);
                airportpickupGV.DataSource = "";
                airportpickupGV.DataSource = airportpickupdetails;
                airportpickupGV.DataBind();
                lblpickupcharges.Text = "0.00";
                lblpickupcurrency.Text = "";
            }
            catch (Exception ex)
            {
                Constants.CON_MESSAGE_STRING = ex.Message;
                SysErrors.AppError.PopulateError(Constants.MESSAGE_WARNING, Constants.CON_MESSAGE_STRING, lblerrmessage);
            }
            finally
            {
                airportpickupdetails.Dispose();
                airportpickupdetails = null;
                reservationDataHandler = null;
            }
        }

        // Set the payment opt when reserve button is clicked
        private void setpaymentoptions()
        {
            ReservationDataHandler eReservationDataHandler = new ReservationDataHandler();
            DataTable dtpayopt = new DataTable();
            ListItem objlistItem = null;
            ddlpaymentoptions.Items.Clear();

            try
            {
                objlistItem = new ListItem();
                objlistItem.Text = Constants.CON_NULL_TEXT;
                objlistItem.Value = Constants.CON_NULL_VALUE;
                ddlpaymentoptions.Items.Add(objlistItem);
                string COMPCODE = (string)Session[Constants.SYS_SESSION_COMPANYCODE];
                string LOCCODE = (string)Session[Constants.SYS_SESSION_LOCATIONCODE];
                dtpayopt = eReservationDataHandler.getpaymentoptions(COMPCODE, LOCCODE, Constants.CON_STATUS_ACTIVE);
                for (int i = 0; i < dtpayopt.Rows.Count; i++)
                {
                    objlistItem = new ListItem();
                    objlistItem.Text = dtpayopt.Rows[i]["PAYMENTMODE"].ToString();
                    objlistItem.Value = dtpayopt.Rows[i]["PAYMENSTSTCODE"].ToString();
                    ddlpaymentoptions.Items.Add(objlistItem);
                }
            }
            catch (Exception ex)
            {
                Constants.CON_MESSAGE_STRING = ex.Message;
                SysErrors.AppError.PopulateError(Constants.MESSAGE_WARNING, Constants.CON_MESSAGE_STRING, lblerrmessage);
            }
            finally
            {
                dtpayopt.Dispose();
                dtpayopt = null;
                eReservationDataHandler = null;
            }
        }

        // Validation is done on selected room count.
        private bool getreserveValidation(int totalroom, DataTable selectedRmtb, DataTable SERDET)
        {
            if (Convert.ToInt32(SERDET.Rows[0]["NOOFROOMS"].ToString()) != totalroom)
            {
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('You have to select " + SERDET.Rows[0]["NOOFROOMS"].ToString() + " rooms')", true);

                return false;
            }

            if (selectedRmtb != null)
            {
                foreach (DataRow datarow in selectedRmtb.Rows)
                {
                    if (!datarow.IsNull("maxNumReach"))
                    {
                        return false;
                    }
                }
            }
            else
            {
                return false;
            }

            string totmaxoccupancy = selectedRmtb.AsEnumerable()
            .Sum(x => x.Field<int>("maxoccupancy"))
            .ToString();
            if (Convert.ToInt32(totmaxoccupancy) < (Convert.ToInt32(SERDET.Rows[0]["ADULTS"].ToString()) + Convert.ToInt32(SERDET.Rows[0]["CHILDREN"].ToString())))
            {
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('You have reached maximum occupancy')", true);

                return false;
            }

            return true;
        }

        // country details to be selected by the user
        private void setCountryDetails()
        {
            ReservationDataHandler reservationDataHandler = new ReservationDataHandler();
            DataTable countryDetails = new DataTable();
            try
            {
                countryDetails = reservationDataHandler.getcountrydetails();
                ddlcountry.DataTextField = "COUNTRY";
                ddlcountry.DataValueField = "COUNTRY";
                ddlcountry.DataSource = countryDetails;
                ddlcountry.DataBind();

                payDate.Text = DateTime.Now.ToString("yyyy-MM-dd");
            }
            catch (Exception ex)
            {
                Constants.CON_MESSAGE_STRING = ex.Message;
                SysErrors.AppError.PopulateError(Constants.MESSAGE_WARNING, Constants.CON_MESSAGE_STRING, lblerrmessage);
            }
            finally
            {
                countryDetails.Dispose();
                countryDetails = null;
                reservationDataHandler = null;
            }
        }

        // On search button is clicked the room information is grouped by room type and bind to the room info grid
        protected void reservationGV_DataBound(object sender, EventArgs e)
        {
            for (int rowIndex = roomReservationGV.Rows.Count - 2;
            rowIndex >= 0; rowIndex--)
            {
                GridViewRow gvRow = roomReservationGV.Rows[rowIndex];
                GridViewRow gvPreviousRow = roomReservationGV.Rows[rowIndex + 1];
                Label thermtyp = gvRow.FindControl("roomtypeID") as Label;
                Label prermtyp = gvPreviousRow.FindControl("roomtypeID") as Label;
                int cellCount = 0;
                if (thermtyp.Text == prermtyp.Text)
                {
                    if (gvPreviousRow.Cells[cellCount].RowSpan < 2)
                    {
                        gvRow.Cells[cellCount].RowSpan = 2;
                    }
                    else
                    {
                        gvRow.Cells[cellCount].RowSpan =
                        gvPreviousRow.Cells[cellCount].RowSpan + 1;
                    }
                    gvPreviousRow.Cells[cellCount].Visible = false;
                }
            }
        }

        // Bind room extra option information to the room information summary grid
        protected void roomSelectedGV_DataBound(object sender, EventArgs e)
        {
            try
            {
                DataTable selectxoptTb = (DataTable)Session[Constants.SYS_SESSION_SELECTEDEXOPTRMTB];
                foreach (GridViewRow roomSelectedGVrw in roomSelectedGV.Rows)
                {
                    GridView selectedroomexoptionGV = (roomSelectedGVrw.FindControl("selectedroomexoptionGV") as GridView);
                    Label ROOMRATECODE = roomSelectedGVrw.FindControl("selectroomratecd") as Label;
                    if (selectxoptTb != null)
                    {
                        DataTable tblFiltered = null;
                        var rows = selectxoptTb.AsEnumerable().Where(row => row.Field<String>("ROOMRATECODE") == ROOMRATECODE.Text)
                        .OrderByDescending(row => row.Field<String>("ROOMRATECODE"));

                        if (rows.Any())
                        {
                            tblFiltered = rows.CopyToDataTable();
                            selectedroomexoptionGV.DataSource = tblFiltered;
                            selectedroomexoptionGV.DataBind();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Session[Constants.SYS_SESSION_ERROR] = ex.Message.ToString() + Environment.NewLine + " Unable to complete the reservation. Please contact hotel management.";
                Response.Redirect("~/Modules/Results/errorpage.aspx", false);
            }
            finally
            {
            }
        }

        // on reservation row databound add the no of room selected typed by the user to the drop down list
        protected void reservationGV_row_DataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                DataTable SERDET = (DataTable)Session[Constants.SYS_SESSION_USERINPUTINFO];
                ListItem objlistItem = null;
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    DropDownList ddlTotRooms = (e.Row.FindControl("ddlTotRooms") as DropDownList);
                    Button EXOPTBUTTON = e.Row.FindControl("btnShow") as Button;
                    EXOPTBUTTON.Visible = false;

                    for (int i = 0; i <= Convert.ToInt32(SERDET.Rows[0]["NOOFROOMS"].ToString()); i++)
                    {
                        objlistItem = new ListItem();
                        objlistItem.Text = i.ToString();
                        objlistItem.Value = i.ToString();
                        ddlTotRooms.Items.Add(objlistItem);
                    }
                }
            }
            catch (Exception ex)
            {
                Session[Constants.SYS_SESSION_ERROR] = ex.Message.ToString() + Environment.NewLine + " Unable to complete the reservation. Please contact hotel management.";
                Response.Redirect("~/Modules/Results/errorpage.aspx", false);
            }
            finally
            {
            }
        }

        // on reservaiton confirmation button is clicked
        // all the data tables are manupilated in a way to pass it to the database
        // tax table is manipulated
        // if the reservation is success, accroding to the selected payment option, page is redirected to the prior pages.

        protected void confirm_Click(object sender, EventArgs e)
        {
            ReservationDataHandler eReservationDataHandler = new ReservationDataHandler();
            string enRESCODE = "";
            Session[Constants.SYS_SESSION_ERROR] = "";

            try
            {
                string[] LOCSYSDATE = ddllocations.SelectedItem.Value.ToString().Split('|');
                string LOCCODE = LOCSYSDATE[0].ToString();
                string SYSDATE = LOCSYSDATE[1].ToString();
                string ENUSER = "";
                string GUESTREFCODE = "";
                string GUESTCODE = "";
                string COUNTRY = ddlcountry.SelectedItem.Value.ToString();
                string NICPP = txtppno.Text.ToString();
                string TITLE = ddltitle.SelectedItem.Value.ToString();
                string FIRSTNAME = txtfirstname.Text.ToString();
                string LASTNAME = txtlastname.Text.ToString();
                string EMAIL = txtemail.Text.ToString();
                string CONTACTNO = txtmobile.Text.ToString();
                string[] PAYMENSTSTCODE = ddlpaymentoptions.SelectedItem.Value.ToString().Split('|');
                string PYMODE = PAYMENSTSTCODE[0].ToString();
                string STS = PAYMENSTSTCODE[1].ToString();
                string PAYDATE = payDate.Text;
                string GROSSAMNT = "";
                string TOTTAXAMNT = "0";
                string NETAMNT = "";
                HiddenField UTCtime = (HiddenField)Page.Master.FindControl("hfutctime");
                string LOCDATE = UTCtime.Value.ToString();

                DataTable SERDET = (DataTable)Session[Constants.SYS_SESSION_USERINPUTINFO];
                DataTable custreservermdt = (DataTable)Session[Constants.SYS_SESSION_CUSTRESTB];
                DataTable taxtb = (DataTable)Session[Constants.SYS_SESSION_TAXTABLE];
                DataTable selectedexoptTb = (DataTable)Session[Constants.SYS_SESSION_SELECTEDEXOPTRMTB];
                DataTable selectedpickup = (DataTable)Session[Constants.SYS_SESSION_PICKUPTB];
                DataTable taxtbcp = taxtb.Copy();
                DataTable selectedexoptTbcp = null;
                if (selectedexoptTb != null)
                {
                    selectedexoptTbcp = selectedexoptTb.Copy();
                    selectedexoptTbcp.Columns.Remove("ROOMRATECODE");
                    selectedexoptTbcp.Columns.Remove("ROOMEXOPTION");
                    selectedexoptTbcp.Columns.Remove("FIXEDORVALUE");
                    selectedexoptTbcp.AcceptChanges();
                }
                else
                {
                    selectedexoptTbcp = null;
                }

                if(selectedpickup!= null)
                {

                    for(int i = 0; i < selectedpickup.Rows.Count; i++)
                    {
                        if (selectedpickup.Rows[i]["NOOFUNITS"].Equals("0"))
                        {
                            selectedpickup.Rows[i].Delete();
                        }
                    }
                    selectedpickup.Columns.Remove("DESCRIP");
                    selectedpickup.AcceptChanges();
                }

                foreach (DataRow taxrw in taxtbcp.Rows)
                {
                    if (taxrw["TAXTYPE"].Equals(Constants.CON_TAX_GROSS_TYPE))
                    {
                        GROSSAMNT = taxrw["TAXAMOUNT"].ToString();
                        taxrw.Delete();
                    }
                    else if (taxrw["TAXTYPE"].Equals(Constants.CON_TT_TAX_AMOUNT))
                    {
                        TOTTAXAMNT = taxrw["TAXAMOUNT"].ToString();
                        taxrw.Delete();
                    }
                    else if (taxrw["TAXTYPE"].Equals(Constants.CON_TAX_NETT_RW))
                    {
                        NETAMNT = taxrw["TAXAMOUNT"].ToString();
                        taxrw.Delete();
                    }
                }
                taxtbcp.Columns.Remove("SEQ");
                taxtbcp.Columns.Remove("TAXTYPE");
                taxtbcp.Columns.Remove("TAXAPPLIEDFOR");
                taxtbcp.AcceptChanges();

                ReservationValidationDataHandler reservationValidationDataHandler = new ReservationValidationDataHandler();
                Boolean isvalid = reservationValidationDataHandler.validateReservation(custreservermdt);
                string RESCODE = "";
                if (isvalid)
                {
                    RESCODE = eReservationDataHandler.CreateReservation(LOCCODE, PYMODE, PAYDATE, NETAMNT, GROSSAMNT, TOTTAXAMNT, RESCOMPCODE, LOCCODE, GUESTREFCODE,
               GUESTCODE, NICPP, TITLE, FIRSTNAME, LASTNAME, EMAIL, CONTACTNO, COUNTRY, LOCDATE, STS, ENUSER, SERDET.Rows[0]["CHECKINDATE"].ToString(),
               SERDET.Rows[0]["CHECKOUTDATE"].ToString(), SERDET.Rows[0]["NOOFDAYS"].ToString(),
               SERDET.Rows[0]["NOOFROOMS"].ToString(), SERDET.Rows[0]["ADULTS"].ToString(), SERDET.Rows[0]["CHILDREN"].ToString(),
               custreservermdt, taxtbcp, selectedexoptTbcp, SYSDATE.ToString(), DateTime.Now.ToString("yyyy-MM-dd"),selectedpickup);
                }
                else
                {
                    Constants.CON_MESSAGE_STRING = "ERROR OCCURED!";
                    SysErrors.AppError.PopulateError(Constants.MESSAGE_WARNING, Constants.CON_MESSAGE_STRING, lblerrmessage);
                    return;
                }

                if (RESCODE != "")
                {
                    enRESCODE = eReservationDataHandler.EnryptString(RESCODE);
                    if (STS.ToString().Equals(Constants.CON_STATUS_ISPAID))
                    {
                        if (PYMODE.Equals("PP"))
                        {
                            PaypalDataHandler paypalpayment = new PaypalDataHandler();
                            string redirecturl = paypalpayment.PayWithPayPal(NETAMNT, TITLE, FIRSTNAME + " " + LASTNAME, CONTACTNO, EMAIL, "USD", COUNTRY, enRESCODE.ToString());

                            Response.Redirect(redirecturl, false);
                        }
                        else if (PYMODE.Equals("MS") || PYMODE.Equals("VS"))
                        {
                        }
                        else if (PYMODE.Equals("AE"))
                        {
                            //NTBPaymentGatewayDataHandler nTBPaymentGatewayDataHandler = new NTBPaymentGatewayDataHandler(1, "RAVANALKR", decimal.Parse("1.00"), "LKR", RESCODE, "eng", "", "", "", "", "");
                            //string uri = nTBPaymentGatewayDataHandler.EncryptData();
                            Response.Redirect("~/Modules/PaymentGateway/NTBPaymentGateway/ntbreqmessage.aspx?RESCODE=" + RESCODE.ToString() + "&PAYMENTCODE=AE&TRANSACTIONAMOUNT=1.00", false);
                        }
                        else
                        {
                            Response.Redirect("../PaymentGateway/paymentgateway.aspx?RESCODE=" + enRESCODE.ToString(), false);
                        }
                    }
                    else if (STS.ToString().Equals(Constants.CON_STATUS_ISRESERVED))
                    {
                        Response.Redirect("~/Modules/Results/resconfirmation.aspx?RESCODE=" + enRESCODE.ToString(), false);
                    }
                    else
                    {
                        Session[Constants.SYS_SESSION_ERROR] = "Unknown reason, reservation completed ? , Please contact hotel management.";
                        Response.Redirect("~/Modules/Results/resconfirmation.aspx?RESCODE=" + enRESCODE.ToString(), false);
                    }
                }
                else
                {
                    Session[Constants.SYS_SESSION_ERROR] = "Unable to complete the reservation. Please contact hotel management.";
                    Response.Redirect("~/Modules/Results/errorpage.aspx", false);
                }
            }
            catch (Exception ex)
            {
                Session[Constants.SYS_SESSION_ERROR] = ex.Message.ToString() + Environment.NewLine + " Unable to complete the reservation. Please contact hotel management.";
                Response.Redirect("~/Modules/Results/errorpage.aspx", false);
            }
            finally
            {
                eReservationDataHandler = null;
            }
        }

        // on NIC text is changed get guest details
        //? remove in future
        protected void txtppno_TextChanged(object sender, EventArgs e)
        {
            try
            {
                getGuestDetails();
            }
            catch (Exception ex)
            {
                Session[Constants.SYS_SESSION_ERROR] = ex.Message.ToString() + Environment.NewLine + " Unable to complete the reservation. Please contact hotel management.";
                Response.Redirect("~/Modules/Results/errorpage.aspx", false);
            }
        }

        // room extra option row command popoulate the room extra option data table

        protected void roomReservationGV_rmexopt_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "Show")
                {
                    int RowIndex = Convert.ToInt32((e.CommandArgument).ToString());
                    Button btn = (Button)roomReservationGV.Rows[RowIndex].FindControl("btnShow");
                    Button btnclose = (Button)roomReservationGV.Rows[RowIndex].FindControl("btnClose");
                    Panel roomexpnl = (Panel)roomReservationGV.Rows[RowIndex].FindControl("roomexpnl");

                    if (btn.Text == "Show Extra Options")
                    {
                        ReservationDataHandler eReservationDataHandler = new ReservationDataHandler();
                        GridView gv = (GridView)roomReservationGV.Rows[RowIndex].FindControl("roomexoptionGV");
                        string LOCCODE = (string)Session[Constants.SYS_SESSION_LOCATIONCODE];
                        string ROOMTYPECODE = (((Label)roomReservationGV.Rows[RowIndex].FindControl("roomtypeCodeID")).Text).ToString();
                        string roomrt = (((Label)roomReservationGV.Rows[RowIndex].FindControl("roomrtID")).Text).ToString();
                        roomexpnl.Visible = true;
                        DataTable roomexoptdt = new DataTable();
                        string YEAR = DateTime.Now.Year.ToString();
                        roomexoptdt = eReservationDataHandler.getlocationexoptions(RESCOMPCODE, LOCCODE, ROOMTYPECODE, Constants.CON_STATUS_ACTIVE, YEAR);
                        roomexoptdt.Columns.Add("ExRate");
                        foreach (DataRow rmexoptrw in roomexoptdt.Rows)
                        {
                            if (rmexoptrw["FIXEDORVALUE"].Equals("V"))
                            {
                                double total = Convert.ToDouble(roomrt) * 0.01 * Convert.ToDouble(rmexoptrw["ROOMEXARATE"]);
                                rmexoptrw["ExRate"] = total.ToString("N2");
                            }
                            else
                            {
                                rmexoptrw["ExRate"] = Convert.ToDouble(rmexoptrw["ROOMEXARATE"]).ToString("N2");
                            }
                        }

                        roomexoptdt.AcceptChanges();
                        gv.DataSource = roomexoptdt;
                        gv.DataBind();
                        btn.Text = "Hide Extra Options";
                    }
                    else if (btn.Text == "Hide Extra Options")
                    {
                        GridView gv = (GridView)roomReservationGV.Rows[RowIndex].FindControl("roomexoptionGV");

                        long id = long.Parse(e.CommandArgument.ToString());
                        roomexpnl.Visible = false;
                        gv.DataSource = null;
                        gv.DataBind();
                        btn.Text = "Show Extra Options";
                    }
                    else if (btnclose.Text == "X")
                    {
                        GridView gv = (GridView)roomReservationGV.Rows[RowIndex].FindControl("roomexoptionGV");
                        long id = long.Parse(e.CommandArgument.ToString());
                        roomexpnl.Visible = false;
                        gv.DataSource = null;
                        gv.DataBind();
                        btn.Text = "Show Extra Options";
                    }
                }
            }
            catch (Exception ex)
            {
                Session[Constants.SYS_SESSION_ERROR] = ex.Message.ToString() + Environment.NewLine + " Unable to complete the reservation. Please contact hotel management.";
                Response.Redirect("~/Modules/Results/errorpage.aspx", false);
            }
            finally
            {
            }
        }

        // on room extra option table is selected
        protected void ddlroomexopt_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                GridViewRow roomexoptGvrw = (GridViewRow)((Control)sender).NamingContainer;
                GridView roomexoptGv = (GridView)(roomexoptGvrw.Parent.Parent);
                GridViewRow roomresvGv = (GridViewRow)(roomexoptGv.NamingContainer);
                Label ROOMRATECODE = (Label)roomresvGv.FindControl("roomrtCdID");

                double totexoprate = 0;
                double exoprate = 0;

                foreach (GridViewRow grow in roomexoptGv.Rows)
                {
                    DropDownList ddl = (DropDownList)grow.FindControl("ddlroomexopt");
                    exoprate = Convert.ToDouble(grow.Cells[4].Text) * Convert.ToDouble(ddl.SelectedValue);
                    totexoprate = totexoprate + exoprate;

                    setselectedexoptionTable(grow, exoprate, ddl.SelectedValue, ROOMRATECODE.Text);
                }

                Label lblexrmrate = (Label)roomresvGv.FindControl("lblexrmrate");
                lblexrmrate.Text = totexoprate.ToString();
                DataTable roomdetTb = new DataTable();
                roomdetTb = setroomTableDetails();

                setSelectedRoomTableDetails(roomresvGv, roomdetTb);
                calculateamount();
            }
            catch (Exception ex)
            {
                Session[Constants.SYS_SESSION_ERROR] = ex.Message.ToString() + Environment.NewLine + " Unable to complete the reservation. Please contact hotel management.";
                Response.Redirect("~/Modules/Results/errorpage.aspx", false);
            }
            finally
            {
            }
        }

        // set the selected room extra options to the session
        private void setselectedexoptionTable(GridViewRow gvr, double exoprate, string noofunits, string ROOMRATECODE)
        {
            string ROOMEXOPTIONCODE = gvr.Cells[0].Text;

            if (Session[Constants.SYS_SESSION_SELECTEDEXOPTRMTB] == null)
            {
                selectedexoptTb = new DataTable();
                selectedexoptTb.Columns.Add("ROOMTYPECODE", typeof(string));
                selectedexoptTb.Columns.Add("ROOMEXOPTIONPLANCODE", typeof(string));
                selectedexoptTb.Columns.Add("ROOMEXOPTIONCODE", typeof(string));
                selectedexoptTb.Columns.Add("NOOFUNITS", typeof(string));
                selectedexoptTb.Columns.Add("UNITRATE", typeof(string));
                selectedexoptTb.Columns.Add("EXOPTRATE", typeof(string));
                selectedexoptTb.Columns.Add("ROOMEXOPTION", typeof(string));
                selectedexoptTb.Columns.Add("FIXEDORVALUE", typeof(string));
                selectedexoptTb.Columns.Add("ROOMRATECODE", typeof(string));
            }
            else
            {
                selectedexoptTb = (DataTable)Session[Constants.SYS_SESSION_SELECTEDEXOPTRMTB];
            }
            //DataRow fdRow = selectedexoptTb.Rows.Find(ROOMEXOPTIONCODE);
            DataRow fdRow = selectedexoptTb.AsEnumerable().SingleOrDefault(r => r.Field<string>("ROOMEXOPTIONCODE") == ROOMEXOPTIONCODE && r.Field<string>("ROOMRATECODE") == ROOMRATECODE);
            if (fdRow != null)
            {
                if (exoprate > 0)
                {
                    fdRow["NOOFUNITS"] = noofunits;
                    fdRow["EXOPTRATE"] = exoprate;
                }
                else
                {
                    fdRow.Delete();
                }
            }
            else
            {
                if (exoprate > 0)
                {
                    DataRow cusdtrow = selectedexoptTb.NewRow();
                    cusdtrow["NOOFUNITS"] = noofunits;
                    cusdtrow["EXOPTRATE"] = exoprate;
                    cusdtrow["ROOMEXOPTIONCODE"] = ROOMEXOPTIONCODE;
                    cusdtrow["ROOMEXOPTIONPLANCODE"] = gvr.Cells[1].Text; ;
                    cusdtrow["ROOMTYPECODE"] = gvr.Cells[7].Text;
                    cusdtrow["UNITRATE"] = gvr.Cells[5].Text;
                    cusdtrow["ROOMEXOPTION"] = gvr.Cells[2].Text;
                    cusdtrow["FIXEDORVALUE"] = gvr.Cells[6].Text;
                    cusdtrow["ROOMRATECODE"] = ROOMRATECODE;
                    selectedexoptTb.Rows.Add(cusdtrow);
                }
            }
            Session[Constants.SYS_SESSION_SELECTEDEXOPTRMTB] = selectedexoptTb;
        }

        // on room extra option row data bound - set room extra option drop down list count according to the no of units
        protected void roomexoptionGV_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                ListItem objlistItem = null;
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    DropDownList ddlroomexopt = (e.Row.FindControl("ddlroomexopt") as DropDownList);
                    string noofunits = e.Row.Cells[3].Text.ToString();

                    for (int i = 0; i <= Convert.ToInt32(noofunits); i++)
                    {
                        objlistItem = new ListItem();
                        objlistItem.Text = i.ToString();
                        objlistItem.Value = i.ToString();
                        ddlroomexopt.Items.Add(objlistItem);
                    }
                }
            }
            catch (Exception ex)
            {
                Session[Constants.SYS_SESSION_ERROR] = ex.Message.ToString() + Environment.NewLine + " Unable to complete the reservation. Please contact hotel management.";
                Response.Redirect("~/Modules/Results/errorpage.aspx", false);
            }
            finally
            {
            }
        }

        // room image slider
        // TODO: properly integrate the room images.
        public void bindImageSlider()
        {
            string COMPCODE = (string)Session[Constants.SYS_SESSION_COMPANYCODE];
            string LOCCODE = (string)Session[Constants.SYS_SESSION_LOCATIONCODE];
            ReservationDataHandler reservationDataHandler = new ReservationDataHandler();
            DataTable roomimagedt = reservationDataHandler.getroomimagepath(COMPCODE, LOCCODE, Constants.CON_STATUS_ACTIVE);
            roomimagesGv.DataSource = roomimagedt;
            roomimagesGv.DataBind();
        }

        //! why this is empty?
        protected void roomimagesGv_RowDataBound(object sender, GridViewRowEventArgs e)
        {
        }

        //! should change this also
        // TODO: add try and catch statement
        protected void roomimagesGv_DataBound(object sender, EventArgs e)
        {
            List<string> imagelist = new List<string>();

            for (int i = 1; i < roomimagesGv.Rows.Count; i++)
            {
                if (!roomimagesGv.Rows[i].Cells[0].Text.Equals(roomimagesGv.Rows[i - 1].Cells[0].Text))
                {
                    imagelist.Add(roomimagesGv.Rows[i].Cells[1].Text);
                }
            }

            DirectoryInfo di = new DirectoryInfo(Server.MapPath("../../Resources/Roomimages").ToString());
            var aa = di.GetFiles();
            var b = (from a in aa
                     select new
                     {
                         img = a
                     }).ToList();
            foreach (GridViewRow roomimagesGvrw in roomimagesGv.Rows)
            {
                DataList roomimagesdl = roomimagesGvrw.FindControl("roomimagesdl") as DataList;
                roomimagesdl.DataSource = imagelist;
                roomimagesdl.DataBind();
            }
        }

        // on Close button is clicked
        // TODO: clear all the client details when the close button is clicked.

        protected void btnclientclose_Click(object sender, EventArgs e)
        {
            custInfoDisplay.Visible = false;
        }

        protected void chkbxpickup_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                if (chkbxpickup.Checked)
                {
                    pnlairportpickup.Visible = true;
                    setairportpickup();
                }
                else
                {
                    pnlairportpickup.Visible = false;
                }
            }
            catch (Exception ex)
            {
                Session[Constants.SYS_SESSION_ERROR] = ex.Message.ToString() + Environment.NewLine + " Unable to complete the reservation. Please contact hotel management.";
                Response.Redirect("~/Modules/Results/errorpage.aspx", false);
            }
            finally
            {
            }
        }

        // on airport pick up dropdown selection changes set the details to a data table
        protected void ddlairportpickup_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                GridViewRow gvr = (GridViewRow)(((Control)sender).NamingContainer);
                decimal totpickupamount = setpickupTableDetails(gvr);
                lblpickupcharges.Text = totpickupamount.ToString();
                lblpickupcurrency.Text = (string)Session[Constants.SYS_SESSION_PREFCURRENCY];
            }
            catch (Exception ex)
            {
                Constants.CON_MESSAGE_STRING = ex.Message;
                SysErrors.AppError.PopulateError(Constants.MESSAGE_WARNING, Constants.CON_MESSAGE_STRING, lblerrmessage);
            }
            finally
            {
            }
        }

        // set airport pick up charges
        private decimal setpickupTableDetails(GridViewRow gvr)
        {
            DropDownList ddlairportpickup1 = (DropDownList)gvr.FindControl("ddlairportpickup");
            DataTable pickuptabledt = new DataTable();
            pickuptabledt.Columns.Add("TRANSPORTMODECODE", typeof(string));
            pickuptabledt.Columns.Add("TRANSPORTMODE", typeof(string));
            pickuptabledt.Columns.Add("TRANSPORTRATE", typeof(string));
            pickuptabledt.Columns.Add("DESCRIP", typeof(string));
            pickuptabledt.Columns.Add("NOOFUNITS", typeof(string));
            pickuptabledt.Columns.Add("RATE", typeof(string));
            pickuptabledt.PrimaryKey = new DataColumn[] { pickuptabledt.Columns["TRANSPORTMODECODE"] };

            Session[Constants.SYS_SESSION_PICKUPTB] = null;
            decimal totpickupamount = 0;
            foreach (GridViewRow row in airportpickupGV.Rows)
            {
                DataRow dtrow = pickuptabledt.NewRow();
                DropDownList ddlairportpickup = (DropDownList)row.FindControl("ddlairportpickup");
                dtrow["TRANSPORTMODECODE"] = row.Cells[0].Text.ToString();
                dtrow["TRANSPORTMODE"] = row.Cells[1].Text.ToString();
                dtrow["TRANSPORTRATE"] = row.Cells[2].Text.ToString();
                dtrow["DESCRIP"] = row.Cells[3].Text.ToString();
                dtrow["NOOFUNITS"] = ddlairportpickup.SelectedValue.ToString();
                dtrow["RATE"] = (decimal.Parse(dtrow["TRANSPORTRATE"].ToString()) * decimal.Parse( ddlairportpickup.SelectedValue)).ToString();
                totpickupamount = totpickupamount + decimal.Parse(dtrow["RATE"].ToString());

                pickuptabledt.Rows.Add(dtrow);
            }
            Session[Constants.SYS_SESSION_PICKUPTB] = pickuptabledt;
            return totpickupamount;
        }

        protected void airportpickupGV_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                ListItem objlistItem = null;
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    DropDownList ddlairportpickup = (e.Row.FindControl("ddlairportpickup") as DropDownList);
                    string noofunits = e.Row.Cells[4].Text.ToString();

                    for (int i = 0; i <= Convert.ToInt32(noofunits); i++)
                    {
                        objlistItem = new ListItem();
                        objlistItem.Text = i.ToString();
                        objlistItem.Value = i.ToString();
                        ddlairportpickup.Items.Add(objlistItem);
                    }
                }
            }
            catch (Exception ex)
            {
                Session[Constants.SYS_SESSION_ERROR] = ex.Message.ToString() + Environment.NewLine + " Unable to complete the reservation. Please contact hotel management.";
                Response.Redirect("~/Modules/Results/errorpage.aspx", false);
            }
            finally
            {
            }
        }
    }
}