﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master/Reservation.Master" AutoEventWireup="true" CodeBehind="reservation.aspx.cs" Inherits="ISECBENG.Modules.Reservations.reservation" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../../Styles/Stylelightpick.css" rel="stylesheet" type="text/css" />
    <link href="../../Styles/StyleSticky.css" rel="stylesheet" type="text/css" />
    <link href="../../Styles/StyleModel.css" rel="stylesheet" type="text/css" />
    <script src="../../Scripts/moment-with-locales.js" type="text/javascript"></script>
    <script src="../../Scripts/lightpick.js" type="text/javascript"></script>
    <script src="../../Scripts/jquery.min.js" type="text/javascript"></script>
    <script type="text/javascript">
        function DoPostBack() {
            __doPostBack("txtppno", "TextChanged");
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="CP1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnablePageMethods="true">
    </asp:ScriptManager>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <div id="#alati"  class="errorbanner">
                <asp:Label ID="lblerrmessage" runat="server" Visible="False"></asp:Label>
            </div>
            <div>
                <asp:ValidationSummary ID="vgclient" runat="server" ValidationGroup="vgclient" CssClass="valsummary" />
                <asp:ValidationSummary ID="vssummary" runat="server" ValidationGroup="vgsum" CssClass="valsummary" />
            </div>
            <div class="preloadcenter">
                <asp:UpdateProgress ID="UpdateProgress1" runat="server">
                    <ProgressTemplate>
                        <img src="../../Images/Icons/Loading.gif" alt="" />
                    </ProgressTemplate>
                </asp:UpdateProgress>
            </div>
            <asp:Panel ID="pnlstickyarea" runat="server" Visible="false">
                <div class="sticky-note">
                    <span class="sticky-note-close" onclick="this.parentElement.style.display='none';">&times;</span>
                    <asp:Literal ID="ltlpagenote" runat="server"></asp:Literal>
                </div>
                <div class="sticky-remark">
                    <span class="sticky-note-close" onclick="this.parentElement.style.display='none';">&times;</span>
                    <asp:Literal ID="ltlpageremark" runat="server"></asp:Literal>
                </div>
                <div class="sticky-info">
                    <span class="sticky-note-close" onclick="this.parentElement.style.display='none';">&times;</span>
                    <asp:Literal ID="ltlpageinfo" runat="server"></asp:Literal>
                </div>
            </asp:Panel>
            <div class="search-area search-area-bgcolor">
                <div class="search-area-boxleft">
                    <table class="td100">
                        <tr>
                            <td class="td50 leftalign">
                                <asp:DropDownList ID="ddllocations" runat="server" CssClass="location-icon icon-group ddlbox">
                                </asp:DropDownList>
                            </td>
                            <td>
                                <asp:RequiredFieldValidator ID="rfusername" runat="server" ErrorMessage="Location is required."
                                    ControlToValidate="ddllocations" ForeColor="Red" ValidationGroup="vgsum">*</asp:RequiredFieldValidator>
                            </td>
                            <td class="td50 rightalign">
                                <asp:TextBox ID="demo" runat="server" class="icon-group calendar-icon inputgridbox" onfocus="showCal()" placeholder="Check in - Check out" ClientIDMode="Static"></asp:TextBox>
                            </td>
                            <td>
                                <asp:RequiredFieldValidator ID="rfdate" runat="server" ErrorMessage="Dates are invalid."
                                    ControlToValidate="demo" ForeColor="Red" ValidationGroup="vgsum">*</asp:RequiredFieldValidator>
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="search-area-boxcenter">
                    <table class="td100">
                        <tr>
                            <td class="td30 leftalign">
                                <asp:TextBox ID="txtadults" runat="server" class="adults-icon icon-group inputgridbox" placeholder="Adults"></asp:TextBox>
                            </td>
                            <td>
                                <asp:RequiredFieldValidator ID="rfadults" runat="server" ErrorMessage="No of adults are invalid."
                                    ControlToValidate="txtadults" ForeColor="Red" ValidationGroup="vgsum">*</asp:RequiredFieldValidator>
                                <asp:CompareValidator ID="cfadults" runat="server" ErrorMessage="No of adults must be a value"
                                    ControlToValidate="txtadults" Operator="DataTypeCheck" Type="Integer" ForeColor="Red"
                                    ValidationGroup="vgsum">*</asp:CompareValidator>
                            </td>
                            <td class="td30 centeralign">
                                <asp:TextBox ID="txtchildren" runat="server" class="icon-group children-icon inputgridbox" placeholder="Children"></asp:TextBox>
                            </td>
                            <td>
                                <asp:RequiredFieldValidator ID="rfchildren" runat="server" ErrorMessage="No of children are invalid."
                                    ControlToValidate="txtchildren" ForeColor="Red" ValidationGroup="vgsum">*</asp:RequiredFieldValidator>
                                <asp:CompareValidator ID="cfchildren" runat="server" ErrorMessage="No of children must be a value"
                                    ControlToValidate="txtchildren" Operator="DataTypeCheck" Type="Integer" ForeColor="Red"
                                    ValidationGroup="vgsum">*</asp:CompareValidator>
                            </td>
                            <td class="td30 rightalign">
                                <asp:TextBox ID="txtrooms" runat="server" class="rooms-icon icon-group inputgridbox" placeholder="Rooms"></asp:TextBox>
                            </td>
                            <td>
                                <asp:RequiredFieldValidator ID="rfroom" runat="server" ErrorMessage="No of rooms are invalid."
                                    ControlToValidate="txtrooms" ForeColor="Red" ValidationGroup="vgsum">*</asp:RequiredFieldValidator>
                                <asp:CompareValidator ID="cfroom" runat="server" ErrorMessage="No of rooms must be a value"
                                    ControlToValidate="txtrooms" Operator="DataTypeCheck" Type="Integer" ForeColor="Red"
                                    ValidationGroup="vgsum">*</asp:CompareValidator>
                            </td>
                        </tr>
                    </table>
                    <label id="result1"></label>
                </div>
                <div class="search-area-boxright">
                    <table class="td100">
                        <tr>
                            <td class="td100">
                                <asp:Button ID="btnadd" runat="server" Text="Search" CssClass="btn" ValidationGroup="vgsum" OnClick="btnadd_Click" />
                            </td>
                        </tr>
                    </table>
                </div>
            </div>

            <asp:Panel ID="pnlroomDetail" runat="server" Visible="false">

                <div id="divdetailarea" class="detail-area">
                    <div class="detail-area-leftbox">
                        <asp:GridView CssClass="room-reservationGV" ID="roomReservationGV" runat="server" AutoGenerateColumns="false" BackColor="White"
                            CellPadding="5" HeaderStyle-CssClass="room-reservationHd" BorderColor="#CCCCCC" BorderStyle="Solid" BorderWidth="2px"
                            RowStyle-CssClass="room-reservationrw" OnDataBound="reservationGV_DataBound" OnRowDataBound="reservationGV_row_DataBound" EmptyDataText="No Rooms Available" OnRowCommand="roomReservationGV_rmexopt_RowCommand">
                            <Columns>
                                <asp:TemplateField HeaderText="ROOM TYPES" ItemStyle-CssClass="gv-vertical-alignroomtyp">
                                    <ItemTemplate>
                                        <asp:Label ID="roomtypeID" runat="server" Text='<%# Eval("ROOMTYPE") %>' CssClass="tblroom-roomtype"></asp:Label><br />
                                        <br />

                                        <asp:Image runat="server" ImageUrl='<%# Eval("IMAGEPATH") %>' /><br />

                                        <br />
                                        <asp:Label runat="server" Text='<%# Eval("TOTAVAILABLEROOMDESCRIP")%> ' CssClass="tblroom-available"> </asp:Label><br />
                                        <br />
                                        <asp:Label runat="server" CssClass="tblroom-free">no of adults </asp:Label>
                                        <asp:Label runat="server" Text='<%# Eval("NOOFADULTS") %>' CssClass="tblroom-free"></asp:Label>
                                        <br />
                                        <asp:Label runat="server" CssClass="tblroom-free">no of children </asp:Label>
                                        <asp:Label runat="server" Text='<%# Eval("NOOFCHILDREN") %>' CssClass="tblroom-free"></asp:Label>
                                        <br />
                                        <asp:Label runat="server" CssClass="tblroom-free">maximum occupancy </asp:Label>
                                        <asp:Label runat="server" ID="maxoccupancyId" Text='<%# Eval("MAXOCCUPANCY") %>' CssClass="tblroom-free"></asp:Label>
                                        <br />
                                        <asp:Label ID="roomAvNoID" runat="server" Text='<%# Eval("TOTAVAILABLEROOMS")%>' Visible="false"></asp:Label>
                                        <asp:Label ID="roomtypeCodeID" runat="server" Text='<%# Eval("ROOMTYPECODE") %>' Visible="false"></asp:Label><br />
                                        <asp:Label runat="server" Text='<%# Eval("ROOMFACILITY") %>' CssClass="tblroom-fac"></asp:Label>
                                        <%--<asp:Button  runat="server" Text="Search" CssClass="btn" ValidationGroup="vgsum" OnClick="Images_Load" />--%>
                                        <asp:HyperLink ID="lnkroomimages" href="#popupRmImg" runat="server">View Room Images</asp:HyperLink>
                                        <br />
                                    </ItemTemplate>
                                    <HeaderStyle HorizontalAlign="Left" />
                                    <ItemStyle HorizontalAlign="Left" Width="40%" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="RATE" ItemStyle-CssClass="gv-vertical-align" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                                    <ItemTemplate>
                                        <asp:Label ID="roomrtID" runat="server" Text='<%# Eval("ROOMRATE") %>' CssClass="tblroom-rate"></asp:Label><br />
                                        <asp:Label ID="lblcurrency" runat="server" Text='<%# Eval("PREFCURRENCY") %>' CssClass="tblroom-rate"></asp:Label><br />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="MEAL PLANS" ItemStyle-CssClass="gv-vertical-align">
                                    <ItemTemplate>
                                        <asp:Label ID="roomrtCdID" runat="server" Text='<%# Eval("ROOMRATECODE") %>' Visible="false"></asp:Label>
                                        <asp:Label ID="taxCodeID" runat="server" Text='<%# Eval("ISTAXINCLUDE") %>' Visible="false"></asp:Label>
                                        <asp:Label ID="lblexrmrate" runat="server" Text="0" Visible="false"></asp:Label>
                                        <asp:Label ID="lblmeal" runat="server" Text='<%# Eval("MEALPLAN") %>' CssClass="tblroomOptions-meal"></asp:Label><br />
                                        <asp:Label ID="lblpayment" runat="server" Text='<%# Eval("PAYOPTION") %>' CssClass="tblroomOptions-payments"></asp:Label><br />
                                        <asp:Label ID="lblcancel" runat="server" Text='<%# Eval("PAYCANCELOPTION") %>' CssClass="tblroomOptions-cancel"></asp:Label><br />
                                        <asp:Label ID="lbltax" runat="server" Text='<%# Eval("TAXDETAILS") %>' CssClass="tblroomOptions-tax"></asp:Label><br />
                                        <br />
                                        <asp:Panel ID="roomexpnl" runat="server" Visible="false">
                                           
                                            <div class="ex-roomopt">

                                                <asp:Button ID="btnClose" runat="server" Text="X" CommandName="Show" CommandArgument='<%# Container.DataItemIndex%>' CssClass="Cssactionbtn" />
                                                <asp:GridView runat="server" ID="roomexoptionGV" AutoGenerateColumns="False" AutoGenerateDeleteButton="False" BackColor="White" Width="100%"
                                                    BorderColor="#CCCCCC" BorderStyle="Solid" BorderWidth="2px" CellPadding="3" HeaderStyle-CssClass="room-reservationHd"
                                                    RowStyle-CssClass="room-exoptionrw" EmptyDataText="" AutoGenerateEditButton="False" OnRowDataBound="roomexoptionGV_RowDataBound">
                                                    <Columns>
                                                        <asp:BoundField DataField="ROOMEXOPTIONCODE" ItemStyle-CssClass="hidden" HeaderStyle-CssClass="hidden" />
                                                        <asp:BoundField DataField="ROOMEXOPTIONPLANCODE" ItemStyle-CssClass="hidden" HeaderStyle-CssClass="hidden" />
                                                        <asp:BoundField DataField="ROOMEXOPTION" HeaderText="ROOM EXTRA OPTION" ItemStyle-CssClass="tblroom-exopt" />
                                                        <asp:BoundField DataField="NOOFUNITS" HeaderText="No of Units" ItemStyle-CssClass="hidden" HeaderStyle-CssClass="hidden" />
                                                        <asp:BoundField DataField="ExRate" HeaderText="UNIT PRICE" ItemStyle-CssClass="tblroom-exoptright" />
                                                        <asp:BoundField DataField="ROOMEXARATE" ItemStyle-CssClass="hidden" HeaderStyle-CssClass="hidden" />
                                                        <asp:BoundField DataField="FIXEDORVALUE" ItemStyle-CssClass="hidden" HeaderStyle-CssClass="hidden" />
                                                        <asp:BoundField DataField="ROOMTYPECODE" ItemStyle-CssClass="hidden" HeaderStyle-CssClass="hidden" />
                                                        <asp:TemplateField HeaderText="NO OF UNITS" ItemStyle-CssClass="gv-vertical-align">
                                                            <ItemTemplate>
                                                                <asp:DropDownList ID="ddlroomexopt" runat="server" AutoPostBack="True" CssClass="ddlexoptgridbox" OnSelectedIndexChanged="ddlroomexopt_SelectedIndexChanged"></asp:DropDownList>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                    </Columns>
                                                </asp:GridView>
                                            </div>
                                             
                                        </asp:Panel>
                                    </ItemTemplate>
                                    <HeaderStyle HorizontalAlign="Left" />
                                    <ItemStyle HorizontalAlign="Left" Width="40%" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="NO. OF ROOMS" HeaderStyle-Width="20%" ItemStyle-CssClass="gv-vertical-align">
                                    <ItemTemplate>
                                        <asp:DropDownList ID="ddlTotRooms" runat="server" AutoPostBack="True" Width="100%" CssClass="ddlgridbox" OnSelectedIndexChanged="ddlTotRooms_SelectedIndexChanged"></asp:DropDownList>
                                        <br />
                                        <asp:Button ID="btnShow" runat="server" Text="Show Extra Options" CommandName="Show" OnClientClick="onbtnshowclick()" CommandArgument='<%# Container.DataItemIndex%>' CssClass="btnshowstyle" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                            <FooterStyle BackColor="White" ForeColor="#000066" />
                            <HeaderStyle BackColor="#29b8e5" Font-Bold="True" ForeColor="White" />
                            <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
                            <RowStyle ForeColor="#000066" />
                            <SelectedRowStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
                            <SortedAscendingCellStyle BackColor="#F1F1F1" />
                            <SortedAscendingHeaderStyle BackColor="#007DBB" />
                            <SortedDescendingCellStyle BackColor="#CAC9C9" />
                            <SortedDescendingHeaderStyle BackColor="#00547E" />
                        </asp:GridView>
                    </div>

                    <asp:Panel ID="pnlratedetails" runat="server" Visible="false">
                        <div class="detail-area-rightbox">
                            <div style="margin-bottom: 5%">
                                <table class="total-amounttb">
                                    <tr>
                                        <td>
                                            <asp:Label ID="totalAmntId" runat="server" CssClass="total-amount"></asp:Label>
                                            <asp:Label runat="server" CssClass="total-amountlbl" ID="totalamountTaxLabel"> with tax included</asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:Button ID="reserveBtn" runat="server" CssClass="btnreserve" Text="Reserve" OnClick="reserveBtn_Click"/>
                                        </td>
                                    </tr>
                                </table>
                            </div>

                            <asp:GridView CssClass="room-reservationGV" ID="roomSelectedGV" runat="server" AutoGenerateColumns="false" BackColor="White"
                                BorderColor="#CCCCCC" BorderStyle="Solid" BorderWidth="2px" CellPadding="3" HeaderStyle-CssClass="room-reservationHd"
                                RowStyle-CssClass="room-reservationrw" EmptyDataText="" OnRowDataBound="roomSelectedGV_RowDataBound" OnDataBound="roomSelectedGV_DataBound">
                                <Columns>
                                    <asp:TemplateField HeaderText="RESERVED ROOMS" ItemStyle-CssClass="tblroom-type" ItemStyle-Width="50%">
                                        <ItemTemplate>
                                            <asp:Label ID="selectRmTypId" runat="server" Text='<%# Eval("selectRmTyp") %>'></asp:Label><br />
                                            <asp:Label ID="selectRmTypcd" Visible="false" runat="server" Text='<%# Eval("selectRmTypcd") %>'></asp:Label>
                                            <asp:Label ID="selectroomratecd" Visible="false" runat="server" Text='<%# Eval("selectroomratecd") %>'></asp:Label>
                                            <asp:Label ID="maxNumReachId" runat="server" Text='<%# Eval("maxNumReach") %>' CssClass="tblroom-free"></asp:Label><br />
                                            <asp:GridView runat="server" ID="selectedroomexoptionGV" AutoGenerateColumns="False" AutoGenerateDeleteButton="False" BackColor="White"
                                                BorderColor="white" BorderStyle="Solid" BorderWidth="2px" CellPadding="3" HeaderStyle-CssClass="room-reservationHd"
                                                RowStyle-CssClass="room-exoptionrw" EmptyDataText="" AutoGenerateEditButton="False">
                                                <Columns>
                                                    <asp:BoundField DataField="ROOMEXOPTIONCODE" ItemStyle-CssClass="hidden" HeaderStyle-CssClass="hidden" />
                                                    <asp:BoundField DataField="ROOMEXOPTIONPLANCODE" ItemStyle-CssClass="hidden" HeaderStyle-CssClass="hidden" />
                                                    <asp:TemplateField HeaderStyle-CssClass="hidden" ItemStyle-BorderStyle="None" ItemStyle-Width="50%">
                                                        <ItemTemplate>
                                                            <asp:Label runat="server" ItemStyle-CssClass="tblroom-free" Text='<%# Eval("ROOMEXOPTION") %>'></asp:Label>
                                                            <asp:Label runat="server" ItemStyle-CssClass="tblroom-free" Text='<%# Eval("NOOFUNITS") + "x"+ Eval("UNITRATE")%>'></asp:Label>
                                                            <asp:Label runat="server" ItemStyle-CssClass="tblroom-free" Text='<%# Eval("EXOPTRATE") %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                    <asp:BoundField DataField="FIXEDORVALUE" ItemStyle-CssClass="hidden" HeaderStyle-CssClass="hidden" />
                                                    <asp:BoundField DataField="ROOMTYPECODE" ItemStyle-CssClass="hidden" HeaderStyle-CssClass="hidden" />
                                                </Columns>
                                            </asp:GridView>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="selectRoomCount" HeaderText="ROOM COUNT" HeaderStyle-Width="20%" ItemStyle-CssClass="tblroom-rate" />
                                    <asp:BoundField DataField="selectRoomrt" HeaderText="RATE" HeaderStyle-Width="20%" DataFormatString="{0:N2}" ItemStyle-CssClass="tblroom-rate" />
                                </Columns>
                            </asp:GridView>
                        </div>
                    </asp:Panel>

                    <asp:Panel ID="custInfoDisplay" runat="server" Visible="false">
                        <div class="popclientdet">
                            <table class="td100">
                                <tr>
                                    <td class="td60">
                                        <div class="paymentdetailarea">
                                            <div class="headertitle-area">
                                                <asp:Label runat="server" Text="Client Details" Font-Bold="true" Font-Size="Medium"></asp:Label>
                                            </div>
                                            <table class="td100">
                                                <tr>
                                                    <td class="tdleft td50">NIC / PP No. </td>
                                                    <td>
                                                        <asp:TextBox ID="txtppno" runat="server" AutoPostBack="True" ClientIDMode="Static" CssClass="inputtextbox" OnTextChanged="txtppno_TextChanged"></asp:TextBox>
                                                    </td>
                                                    <td>
                                                        <asp:RequiredFieldValidator ID="rfppno" runat="server" ControlToValidate="txtppno" ErrorMessage="PP No. / NIC is required." ForeColor="Red" ValidationGroup="vgclient">*</asp:RequiredFieldValidator>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>Title </td>
                                                    <td>
                                                        <asp:DropDownList ID="ddltitle" runat="server" CssClass="custddlbox">
                                                            <asp:ListItem Selected="True">Mr.</asp:ListItem>
                                                            <asp:ListItem>Mrs.</asp:ListItem>
                                                            <asp:ListItem>Ms.</asp:ListItem>
                                                        </asp:DropDownList>
                                                    </td>
                                                    <td>
                                                        <asp:RequiredFieldValidator ID="rftitle" runat="server" ControlToValidate="ddltitle" ErrorMessage="Title is required." ForeColor="Red" ValidationGroup="vgsum">*</asp:RequiredFieldValidator>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>First Name </td>
                                                    <td>
                                                        <asp:TextBox ID="txtfirstname" runat="server" CssClass="inputtextbox"></asp:TextBox>
                                                    </td>
                                                    <td>
                                                        <asp:RequiredFieldValidator ID="rffirstname" runat="server" ControlToValidate="txtfirstname" ErrorMessage="Firstname is required." ForeColor="Red" ValidationGroup="vgclient">*</asp:RequiredFieldValidator>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>Last Name </td>
                                                    <td>
                                                        <asp:TextBox ID="txtlastname" runat="server" CssClass="inputtextbox"></asp:TextBox>
                                                    </td>
                                                    <td>
                                                        <asp:RequiredFieldValidator ID="rflastname" runat="server" ControlToValidate="txtlastname" ErrorMessage="Lastname is required." ForeColor="Red" ValidationGroup="vgclient">*</asp:RequiredFieldValidator>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>Contact No. </td>
                                                    <td>
                                                        <asp:TextBox ID="txtmobile" runat="server" CssClass="inputtextbox" type="tel"></asp:TextBox>
                                                    </td>
                                                    <td>
                                                        <asp:RequiredFieldValidator ID="rfhotline" runat="server" ControlToValidate="txtmobile" ErrorMessage="Contact No. is required." ForeColor="Red" ValidationGroup="vgclient">*</asp:RequiredFieldValidator>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>Country </td>
                                                    <td>
                                                        <asp:DropDownList ID="ddlcountry" runat="server" CssClass="custddlbox">
                                                        </asp:DropDownList>
                                                    </td>
                                                    <td>
                                                        <asp:RequiredFieldValidator ID="rfcountry" runat="server" ControlToValidate="ddlcountry" ErrorMessage="Country is required." ForeColor="Red" ValidationGroup="vgclient">*</asp:RequiredFieldValidator>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>Email</td>
                                                    <td>
                                                        <asp:TextBox ID="txtemail" runat="server" CssClass="inputtextbox"></asp:TextBox>
                                                    </td>
                                                    <td>
                                                        <asp:RequiredFieldValidator ID="rfemail" runat="server" ControlToValidate="txtemail" ErrorMessage="Email is invalid." ForeColor="Red" ValidationGroup="vgclient">*</asp:RequiredFieldValidator>
                                                        <asp:RegularExpressionValidator ID="revemail" runat="server" ControlToValidate="txtemail" ErrorMessage="Invalid email format." ForeColor="Red" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ValidationGroup="vgclient">*</asp:RegularExpressionValidator>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>Email Confirmation </td>
                                                    <td>
                                                        <asp:TextBox ID="txtemailconfirm" runat="server" CssClass="inputtextbox"></asp:TextBox>
                                                    </td>
                                                    <td>
                                                        <asp:RequiredFieldValidator ID="rfemailconfirm" runat="server" ControlToValidate="txtemailconfirm" ErrorMessage="Email is invalid." ForeColor="Red" ValidationGroup="vgclient">*</asp:RequiredFieldValidator>
                                                        <asp:RegularExpressionValidator ID="revemailconfirm" runat="server" ControlToValidate="txtemailconfirm" ErrorMessage="Invalid email format." ForeColor="Red" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ValidationGroup="vgclient">*</asp:RegularExpressionValidator>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="tdleft td50">Pay Date</td>
                                                    <td>
                                                        <asp:Label ID="payDate" runat="server" CssClass="tblroom-free"></asp:Label>
                                                    </td>
                                                    <td></td>
                                                </tr>
                                                <tr>
                                                    <td>Pay Mode</td>
                                                    <td>
                                                        <asp:DropDownList ID="ddlpaymentoptions" runat="server" CssClass="custddlbox">
                                                        </asp:DropDownList>
                                                    </td>
                                                    <td>
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="ddlpaymentoptions" ErrorMessage="Payment option is required." ForeColor="Red" ValidationGroup="vgsum">*</asp:RequiredFieldValidator>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td></td>
                                                </tr>
                                            </table>
                                        </div>
                                    </td>
                                    <td class="td40">
                                        <table class="td100">
                                            <div class="headertitle-area">
                                                <asp:Label runat="server" Text="Other Options" Font-Bold="true" Font-Size="Medium"></asp:Label>
                                            </div>
                                            <tr style="margin-top: 10%">
                                                <td>
                                                    <asp:CheckBox ID="chkbxpickup" runat="server" AutoPostBack="true" Checked="false" OnCheckedChanged="chkbxpickup_CheckedChanged" Text=" Airport PickUp" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <asp:Panel ID="pnlairportpickup" CssClass="pnloverflow" runat="server" Visible="false">
                                                    <asp:GridView ID="airportpickupGV" runat="server" AutoGenerateColumns="False" AutoGenerateDeleteButton="False" AutoGenerateEditButton="False" BackColor="White" BorderColor="#CCCCCC" BorderStyle="Solid" BorderWidth="2px" CellPadding="3" EmptyDataText="No Options Available" HeaderStyle-CssClass="room-reservationHd" Height="10%"
                                                        OnRowDataBound="airportpickupGV_RowDataBound" RowStyle-CssClass="airport-pickuprw" Width="100%">
                                                        <Columns>
                                                            <asp:BoundField DataField="TRANSPORTMODECODE"  HeaderStyle-CssClass="hidden"  ItemStyle-CssClass="hidden" />
                                                            <asp:BoundField DataField="TRANSPORTMODE"  HeaderText="TRANSPORT MODE"  ItemStyle-CssClass="tblroom-exopt" />
                                                            <asp:BoundField DataField="TRANSPORTRATE" HeaderText="TRANSPORT RATE"  ItemStyle-CssClass="tblroom-exopt" />
                                                            <asp:BoundField DataField="DESCRIP" HeaderText="DESCRIPTION" ItemStyle-CssClass="tblroom-exopt" />
                                                            <asp:BoundField DataField="NOOFUNITS" HeaderStyle-CssClass="hidden" ItemStyle-CssClass="hidden" />
                                                            <asp:TemplateField HeaderText="NO OF UNITS" ItemStyle-CssClass="gv-vertical-align">
                                                                <ItemTemplate>
                                                                    <asp:DropDownList ID="ddlairportpickup" runat="server" AutoPostBack="True" CssClass="ddlexoptgridbox" OnSelectedIndexChanged="ddlairportpickup_SelectedIndexChanged">
                                                                    </asp:DropDownList>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                        </Columns>
                                                    </asp:GridView>
                                                    <div class="td100" style="margin:5%">
                                                         <asp:Label runat="server" Font-Bold="true" Text="Pick Up Charges (Pay at arrival) - "></asp:Label>
                                                <asp:Label runat="server" Font-Bold="true" ID="lblpickupcharges" Text="0.00"></asp:Label>    
                                                <asp:Label runat="server" Font-Bold="true" ID="lblpickupcurrency" ></asp:Label>    
                                                    </div>
                                                </asp:Panel>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>

                            <table class="td100">
                                <tr>
                                    <td class="td70">
                                        <div class="headertitle-area">
                                            <asp:Label runat="server" Text="Payment Breakdown -" Font-Bold="true" Font-Size="Medium"></asp:Label>&nbsp;
                                               <asp:Label runat="server" ID="lblprefcurrency" Font-Bold="true" Font-Size="Medium"></asp:Label>
                                        </div>
                                        <asp:GridView ID="taxtableGV" runat="server" AutoGenerateColumns="false" BackColor="White" BorderStyle="None" CellPadding="0" CssClass="taxtbGV" EmptyDataText="" GridLines="None" Width="100%">
                                            <Columns>
                                                <asp:TemplateField HeaderStyle-Width="5%" ItemStyle-CssClass="tblroom-type">
                                                    <ItemTemplate>
                                                        <asp:Label runat="server" Text='<%# Eval("TAXTYPE") %>'></asp:Label>
                                                        <asp:Label ID="maxNumReachId" runat="server" CssClass="tblroom-type" Text='<%# Eval("TAXRATE") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="TAXAMOUNT" DataFormatString="{0:N2}" HeaderStyle-Width="5%" ItemStyle-CssClass="taxtbl-rate" />
                                            </Columns>
                                        </asp:GridView>
                                    </td>
                                    <td>
                                        <div class="info">
                                            <asp:CheckBox runat="server" />
                                            <asp:Label runat="server">I have read and accept the <span style="color:blue;">terms and conditions</span> for this booking.</asp:Label>
                                        </div>
                                        <div class="centeralign">
                                            <asp:Button ID="btnsave" runat="server" CssClass="btn" OnClick="confirm_Click" OnClientClick="getclientDateTime()" Text="Confirm" ValidationGroup="vgclient" />
                                            <asp:Button ID="btnclientclose" runat="server" CssClass="btn" OnClick="btnclientclose_Click" Text="Close" />
                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </asp:Panel>
                </div>
            </asp:Panel>

            <div id="popupRmImg" class="overlay">
                <div class="popup">
                    <h2>Room Images</h2>
                    <a class="close" href="#">&times;</a>
                    <div class="content">
                        <asp:GridView ID="roomimagesGv" runat="server" AutoGenerateColumns="false" BackColor="White"
                            BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="0" CellPadding="3"
                            EmptyDataText="" OnRowDataBound="roomimagesGv_RowDataBound" OnDataBound="roomimagesGv_DataBound">
                            <Columns>
                                <asp:TemplateField ItemStyle-CssClass="tblroom-type" ItemStyle-Width="50%">
                                    <ItemTemplate>
                                        <asp:Label ID="roomimagetypecdId" runat="server" Text='<%# Eval("ROOMTYPECODE") %>' Visible="false"></asp:Label>
                                        <asp:Label runat="server" Text='<%# Eval("IMAGEPATH") %>' Visible="false"></asp:Label>
                                        <asp:Label ID="roomimagetypeId" runat="server" Text='<%# Eval("ROOMTYPE") %>'></asp:Label>
                                        <asp:DataList ID="roomimagesdl" runat="server" RepeatDirection="Horizontal" Width="100%" Height="120px">
                                            <ItemTemplate>
                                                <table>
                                                    <tr>
                                                        <img src='<%#"../../Resources/Roomimages/"+Eval("Img") %>' height="100" width="200" alt="Alternate Text" />
                                                    </tr>
                                                </table>
                                            </ItemTemplate>
                                        </asp:DataList>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                    </div>
                </div>
            </div>

            <asp:HiddenField ID="hffromdate" runat="server" ClientIDMode="Static" />
            <asp:HiddenField ID="hftodate" runat="server" ClientIDMode="Static" />

            <script type="text/javascript">
                function showCal() {
                    new lightPick({
                        field: document.getElementById('demo'),
                        lang: 'en',
                        numberOfMonths: 2,
                        numberOfColumns: 3,
                        singleDate: false,
                        onSelect: function (start, end) {
                            document.getElementById('<%= hffromdate.ClientID %>').value = start.format('YYYY-MM-DD');
                            document.getElementById('<%= hftodate.ClientID %>').value = end.format('YYYY-MM-DD');
                        }
                    });
                }
            </script>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
