﻿using BusinessLogicDataHandler.Reservation;
using ConstantsDataHandler;
using System;

namespace ISECBENG.Modules.Results
{
    public partial class errorpage : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                string RESCODE = "";
                if (Request.QueryString["RESCODE"] != null)
                {
                    RESCODE = Request.QueryString["RESCODE"].ToString();
                }
                bool isupdate = false;
                if (RESCODE != null || RESCODE != "")
                {
                    isupdate = updatereservation(RESCODE);
                }
                getmessage(isupdate);
            }
        }

        private bool updatereservation(string RESCODE)
        {
            ReservationDataHandler reservationDataHandler = new ReservationDataHandler();
            Boolean isupdate = false;
            try
            {
                isupdate = reservationDataHandler.updatereservation(RESCODE, Constants.CON_STATUS_DELETED);
            }
            catch (Exception ex)
            {
                Constants.CON_MESSAGE_STRING = ex.Message;
                SysErrors.AppError.PopulateError(Constants.MESSAGE_WARNING, Constants.CON_MESSAGE_STRING, lblerrmessage);
            }
            finally
            {
                reservationDataHandler = null;
            }

            return isupdate;
        }

        private void getmessage(bool isupdate)
        {
            if (isupdate)
            {
                txtcancelreservation.Text = "Reservation cancelled successfully";
            }

            txtdescription.Text = (string)Session[Constants.SYS_SESSION_ERROR];
        }

        protected void btnconfitm_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/Modules/Reservations/reservation.aspx", false);
        }
    }
}