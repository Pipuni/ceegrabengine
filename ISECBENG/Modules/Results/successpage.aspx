﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master/Reservation.Master" AutoEventWireup="true" CodeBehind="successpage.aspx.cs" Inherits="ISECBENG.Modules.Results.successpage" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="CP1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <div class="errorbanner">
                <asp:Label ID="lblerrmessage" runat="server" Visible="False"></asp:Label>
            </div>
            <div class="preloadcenter">
                <asp:UpdateProgress ID="UpdateProgress1" runat="server">
                    <ProgressTemplate>
                        <img src="../../Images/Icons/Loading.gif" alt="" />
                    </ProgressTemplate>
                </asp:UpdateProgress>
            </div>
            <div align="center">
                <div class="message-area">
                    <p>Your Reservation is Completed ! </p>
                    <p>We are pleased to confirm your lodging reservation and look forward to your visit. Please review the information carefully and contact our reservation department immediately of any changes.</p>
                    <p>
                        Please check your email to insure information on your reservation details.
                    </p>
                    <p>
                        Thank You.
                    </p>
                </div>
                <asp:Button ID="btnconfitm" runat="server" Text="Goto Reservation" CssClass="btn" OnClick="btnconfitm_Click" />
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>