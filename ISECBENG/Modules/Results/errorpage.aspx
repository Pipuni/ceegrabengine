﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master/Reservation.Master" AutoEventWireup="true" CodeBehind="errorpage.aspx.cs" Inherits="ISECBENG.Modules.Results.errorpage" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="CP1" runat="server">
    <div class="errorbanner">
        <asp:Label ID="lblerrmessage" runat="server" Visible="False"></asp:Label>
    </div>
    <div class="errortxtarea">
        <asp:TextBox ID="txtcancelreservation" runat="server" CssClass="errordispaly" ReadOnly="true"></asp:TextBox>
        <asp:TextBox ID="txtdescription" runat="server" TextMode="MultiLine" CssClass="errordispaly" ReadOnly="true"></asp:TextBox>
    </div>
    <div align="center">
        <asp:Button ID="btnconfitm" runat="server" Text="Goto Reservation" CssClass="btn" OnClick="btnconfitm_Click" />
    </div>
</asp:Content>