﻿using BusinessLogicDataHandler.Email;
using BusinessLogicDataHandler.Reservation;
using ConstantsDataHandler;
using iText.Html2pdf;
using System;
using System.Data;
using System.IO;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ISECBENG.Modules.Results
{
    public partial class resconfirmation : System.Web.UI.Page
    {
        private static string emailtext = "";

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                string ENRESCODE = "SQAL1RV200120";
                //gerReservationDetails(ENRESCODE);
                //string ENRESCODE = "";
                if (Request.QueryString["RESCODE"] != null)
                {
                    ENRESCODE = Request.QueryString["RESCODE"].ToString();
                }
                gerReservationDetails(ENRESCODE);
            }
        }

        private void displaylayout(string COMPCODE, string LOCCODE)
        {
            ReservationDataHandler eReservationDataHandler = new ReservationDataHandler();
            DataTable dtlayout = new DataTable();

            try
            {
                dtlayout = eReservationDataHandler.getPageTemplate(COMPCODE, LOCCODE);
                if (dtlayout.Rows.Count > 0)
                {
                    for (int i = 0; i < dtlayout.Rows.Count; i++)
                    {
                        byte[] IMAGEFILE = (byte[])(dtlayout.Rows[i]["PROPIMAGE"]);

                        string filedisplocation = dtlayout.Rows[i]["DISPLAYLOCATION"].ToString();
                        if (filedisplocation == Constants.CON_TOP_IMAGEFILE)
                        {
                            imgbottom.Visible = false;
                            imgtop.Visible = true;
                            imgtop.ImageUrl = String.Format("data:image/jpg;base64,{0}", Convert.ToBase64String(IMAGEFILE));
                        }

                        if (filedisplocation == Constants.CON_RIGHT_IMAGEFILE)
                        {
                            //string idname = i.ToString() + "Right";
                            Image imgr = new Image();
                            imgr.ImageUrl = String.Format("data:image/jpg;base64,{0}", Convert.ToBase64String(IMAGEFILE));
                            imgr.CssClass = "resimage";
                            pnlrightimages.Controls.Add(imgr);
                            pnlrightimages.Controls.Add(new LiteralControl("<br />"));

                            //imrstrRight += "<img id='" + idname + "' alt=''  class='gridimg' src='" + dtlayout.Rows[i]["IMAGEPATH"].ToString().Replace("~", "") + "' /><br/>";
                        }

                        if (filedisplocation == Constants.CON_LEFT_IMAGEFILE)
                        {
                            //string idname = i.ToString() + "Left";
                            Image imgl = new Image();
                            imgl.ImageUrl = String.Format("data:image/jpg;base64,{0}", Convert.ToBase64String(IMAGEFILE));
                            imgl.CssClass = "resimage";
                            pnlleftimages.Controls.Add(imgl);
                            pnlleftimages.Controls.Add(new LiteralControl("<br />"));
                            //imrstrLeft += "<img id='" + idname + "' alt='' class='gridimg'  src='" + dtlayout.Rows[i]["IMAGEPATH"].ToString().Replace("~", "") + "' /><br/>";
                        }

                        if (filedisplocation == Constants.CON_BOTTOM_IMAGEFILE)
                        {
                            imgtop.Visible = false;
                            imgbottom.Visible = true;
                            imgbottom.ImageUrl = String.Format("data:image/jpg;base64,{0}", Convert.ToBase64String(IMAGEFILE));
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Constants.CON_MESSAGE_STRING = ex.Message;
                SysErrors.AppError.PopulateError(Constants.MESSAGE_WARNING, Constants.CON_MESSAGE_STRING, lblerrmessage);
            }
            finally
            {
                eReservationDataHandler = null;
                dtlayout.Dispose();
                dtlayout = null;
            }
        }

        private void gerReservationDetails(string ENRESCODE)
        {
            ReservationDataHandler eReservationDataHandler = new ReservationDataHandler();
            DataTable dtroom = new DataTable();
            ltldetails.Text = "";
            ltlsummery.Text = "";
            ltlmessage.Text = "";

            try
            {
                string RESCODE = ENRESCODE;
                //string RESCODE = eReservationDataHandler.DecryptString(ENRESCODE);
                hfrescode.Value = RESCODE.ToString();
                DataRow dtres = eReservationDataHandler.getReservationDetails(RESCODE);
                if (dtres != null)
                {
                    string COMPCODE = dtres["COMPCODE"].ToString();
                    string LOCCODE = dtres["LOCCODE"].ToString();
                    string LOCATION = dtres["LOCATION"].ToString();
                    string COUNTRY = dtres["COUNTRY"].ToString();
                    string CONTACTPERSON = dtres["CONTATCPERSON"].ToString();
                    string HOTLINE = dtres["HOTLINE"].ToString();
                    string GUESTNAME = dtres["GUESTNAME"].ToString();
                    lblrescode.Text = RESCODE.ToString();
                    lbllocation.Text = dtres["LOCATION"].ToString();
                    lbllocaddress1.Text = dtres["LOCADDRESS1"].ToString();
                    lbllocaddress2.Text = dtres["LOCADDRESS2"].ToString();
                    lbllocaddress3.Text = dtres["LOCADDRESS3"].ToString();
                    lbllochotline.Text = dtres["HOTLINE"].ToString();
                    lbllocfax.Text = dtres["FAX"].ToString();
                    lbllocemail.Text = dtres["LOEMAIL"].ToString();
                    lbltrno.Text = dtres["TRNO"].ToString();
                    lblcontatcno.Text = dtres["CONTACTNO"].ToString();
                    lblemail.Text = dtres["EMAIL"].ToString();
                    lblnicpp.Text = dtres["NICPP"].ToString();
                    lblguestname.Text = dtres["GUESTNAME"].ToString();
                    lblbookeddate.Text = dtres["LOCDATE"].ToString();
                    lbladults.Text = dtres["ADULTS"].ToString();
                    lblchildren.Text = dtres["CHILDREN"].ToString();
                    lblrooms.Text = dtres["NOOFROOMS"].ToString();
                    lbldays.Text = dtres["NOOFDAYS"].ToString();
                    lblpaymode.Text = dtres["PAYMENTMODE"].ToString();
                    double GROSSAMT = double.Parse(dtres["GROSSAMT"].ToString());
                    double TAXAMT = double.Parse(dtres["TAXAMT"].ToString());
                    double NETAMT = double.Parse(dtres["NETAMT"].ToString());
                    string PREFCURRENCY = dtres["PREFCURRENCY"].ToString();
                    lblinvoicetotal.Text = PREFCURRENCY + " " + Math.Round(NETAMT).ToString("#,##0.00");
                    //imglogo.ImageUrl = (string)Session[Constants.SYS_SESSION_COMPANYLOGO];
                    idguestname.InnerText = dtres["GUESTNAME"].ToString();
                    idagerestrction.InnerText = dtres["AGEPOLICY"].ToString();
                    idarrival.InnerText = dtres["CHECKINDATE"].ToString();
                    idcancellation.InnerText = dtres["PAYCANCELPOLICY"].ToString();
                    idcheckintime.InnerText = dtres["CHECKINTIME"].ToString();
                    idcheckouttime.InnerText = dtres["CHECKOUTTIME"].ToString();
                    iddays.InnerText = dtres["NOOFDAYS"].ToString();
                    iddepature.InnerText = dtres["CHECKOUTDATE"].ToString();
                    idguest.InnerText = dtres["GUESTNAME"].ToString();
                    idpets.InnerText = dtres["PETPOLICY"].ToString();
                    idrescode.InnerText = dtres["RESCODE"].ToString();
                    idsmoking.InnerText = dtres["SMOKINGPOLICY"].ToString();

                    byte[] ICONFILE = (byte[])(dtres["ICONFILE"]);
                    imgpay.ImageUrl = String.Format("data:image/jpg;base64,{0}", Convert.ToBase64String(ICONFILE));

                    int NOOFDAYS = int.Parse(dtres["NOOFDAYS"].ToString());

                    displaylayout(COMPCODE, LOCCODE);

                    //ltlmessage.Text = eReservationDataHandler.ViewResMessage(COMPCODE,LOCCODE, GUESTNAME, LOCATION, HOTLINE, CONTACTPERSON, COUNTRY);
                    ltldetails.Text = eReservationDataHandler.ViewResdetailsForInvoice(RESCODE, NOOFDAYS, PREFCURRENCY);
                    ltlexroomopt.Text = eReservationDataHandler.ViewExoptionsForInvoice(RESCODE, PREFCURRENCY);
                    ltlsummery.Text = eReservationDataHandler.ViewSummaryForInvoice(RESCODE, GROSSAMT, TAXAMT, NETAMT, PREFCURRENCY);
                    idrescon.InnerHtml = eReservationDataHandler.getPageDocDetails(COMPCODE, LOCCODE, Constants.CON_DOC_RESCONFIRMATION);
                    ltlmessage.Text = eReservationDataHandler.getPageDocDetails(COMPCODE, LOCCODE, Constants.CON_DOC_MESSAGE);
                    emailtext = eReservationDataHandler.getPageDocDetails(COMPCODE, LOCCODE, Constants.CON_DOC_EMAIL);
                }
            }
            catch (Exception ex)
            {
                Constants.CON_MESSAGE_STRING = ex.Message;
                SysErrors.AppError.PopulateError(Constants.MESSAGE_WARNING, Constants.CON_MESSAGE_STRING, lblerrmessage);
            }
            finally
            {
                eReservationDataHandler = null;
            }
        }

        protected void btnconfitm_Click(object sender, EventArgs e)
        {
            EmailHandler eEmailHandler = new EmailHandler();
            string HtmlSting = "";
            string Htmlrescon = "";

            try
            {
                HiddenField UTCtime = (HiddenField)Page.Master.FindControl("hfutcdatetime");
                lblutcdatetime.Text = UTCtime.Value.ToString();

                string RESCODE = hfrescode.Value.ToString();
                string sHtmlFilename = Constants.CON_RESINVOICE_FILENAME + RESCODE + ".html";
                string sPdfFilename = Constants.CON_RESINVOICE_FILENAME + RESCODE + ".pdf";
                string sHtmlResFilename = Constants.CON_RESCON_FILENAME + RESCODE + ".html";
                string sPdfResFilename = Constants.CON_RESCON_FILENAME + RESCODE + ".pdf";
                var sb = new StringBuilder();
                var sbcon = new StringBuilder();
                var dir = Server.MapPath(Constants.CON_DOC_PATH);

                HtmlSting += "<html><head>";
                HtmlSting += "<title>" + lbllocation.ToString() + "</title>";
                HtmlSting += "</head>";
                HtmlSting += "<body style='font-family:Lucida Grande,Segoe UI,Arial,Helvetica,Verdana,sans-serif;background-color:#FFF;font-size:.8em;border:5px solid #ff6a00;padding:2%'>";
                guestinvoice.RenderControl(new HtmlTextWriter(new StringWriter(sb)));
                HtmlSting += sb.ToString();
                HtmlSting += "</body></html>";

                Htmlrescon += "<html><head>";
                Htmlrescon += "<title>" + lbllocation.ToString() + "</title>";
                Htmlrescon += "</head>";
                Htmlrescon += "<body style='font-family:Lucida Grande,Segoe UI,Arial,Helvetica,Verdana,sans-serif;background-color:#FFF;font-size:.8em;padding:2%'>";
                resconfirm.RenderControl(new HtmlTextWriter(new StringWriter(sbcon)));
                Htmlrescon += sbcon.ToString();
                Htmlrescon += "</body></html>";

                string FILEPATH = dir + sHtmlFilename;
                var SFILE = Path.Combine(dir, sHtmlFilename);
                var SFILEPDF = Path.Combine(dir, sPdfFilename);

                File.WriteAllText(SFILE, HtmlSting.ToString());
                HtmlConverter.ConvertToPdf(new FileInfo(FILEPATH),
                    new FileInfo(dir + sPdfFilename));

                string FILEPATHCON = dir + sHtmlResFilename;
                var SFILECON = Path.Combine(dir, sHtmlResFilename);
                var SFILECONPDF = Path.Combine(dir, sPdfResFilename);

                File.WriteAllText(SFILECON, Htmlrescon.ToString());
                HtmlConverter.ConvertToPdf(new FileInfo(FILEPATHCON),
                    new FileInfo(dir + sPdfResFilename));

                Boolean issent = eEmailHandler.SendReservationConfirmationEmail(RESCODE, SFILECONPDF, SFILEPDF, emailtext, lbllocation.Text.ToString(), lblguestname.Text.ToString(), lblemail.Text.ToString());
                if (issent)
                {
                    Response.Redirect("~/Modules/Results/successpage.aspx?RESCODE=" + RESCODE.ToString(), false);
                }
                else
                {
                    Session[Constants.SYS_SESSION_ERROR] = "We are unable to complete the reservation. Please contact " + lbllocation.Text.ToString() + " hotel management. " + Environment.NewLine + "Your reservation code is " + lblrescode.Text.ToString();
                    Response.Redirect("~/Modules/Results/errorpage.aspx", false);
                }
            }
            catch (Exception ex)
            {
                Session[Constants.SYS_SESSION_ERROR] = ex.Message.ToString() + Environment.NewLine + "We are unable to complete the reservation. Please contact " + lbllocation.Text.ToString() + " hotel management. ";
                Response.Redirect("~/Modules/Results/errorpage.aspx", false);
            }
            finally
            {
                eEmailHandler = null;
            }
        }
    }
}