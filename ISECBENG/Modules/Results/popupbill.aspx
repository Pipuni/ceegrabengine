﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="popupbill.aspx.cs" Inherits="ISECBENG.Modules.Results.popupbill" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Secvision - Hotel Reservation System</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Design & Developed by Secvision">
    <meta name="author" content="Secvision (Private) Limited.">
    <link href="../../Styles/StyleReservation.css" rel="stylesheet" />
    <link href="../../Styles/StyleError.css" rel="stylesheet" />
    <script src="../../Scripts/jquery-3.1.0.min.js"></script>
    <script type="text/javascript" src="<%= ResolveUrl ("~/Scripts/UTCdatetime.js") %>"></script>
    <script type="text/javascript">
        function getclientDateTime() {
            var utcdatetime = getUTCdatetime();
            document.getElementById('<%= hfutctime.ClientID %>').value = utcdatetime.toString();

        }

        function LoadUTCdatetime() {
            var today = new Date();
            document.getElementById('divUTCtime').innerText = today.toString();
            var t = setTimeout(LoadUTCdatetime, 500);
        }
    </script>
</head>
<body style="margin: 0px; background-color: #e1dede" onload="LoadUTCdatetime()">
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server">
        </asp:ScriptManager>
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
                <div class="errorbanner">
                    <asp:Label ID="lblerrmessage" runat="server" Visible="False"></asp:Label>
                </div>
                <div class="errorbox">
                    <asp:ValidationSummary ID="vssummary" runat="server" ValidationGroup="vgsum" CssClass="valsummary" />
                </div>
                <div class="preloadcenter">
                    <asp:UpdateProgress ID="UpdateProgress1" runat="server">
                        <ProgressTemplate>
                            <img src="../../Images/Icons/Loading.gif" alt="" />
                        </ProgressTemplate>
                    </asp:UpdateProgress>
                </div>
                <div class="tdcenter">
                    <asp:Button ID="btnprint" runat="server" Text="Print Invoice" CssClass="btn" />
                </div>
                <div class="billdiv">
                    <table class="billtb">
                        <tr>
                            <td class="td50">
                                <asp:Label ID="lblinvoice" runat="server" Text="INVOICE" CssClass="titlefont"></asp:Label>
                            </td>
                            <td class="td10 tdright" rowspan="2">
                                <asp:Image ID="imglogo" runat="server" CssClass="imglogo" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <table>
                                    <tr>
                                        <td class="td20">INVOICE NUMBER
                                        </td>
                                        <td class="td70">
                                            <asp:Label ID="lbltrno" runat="server" Text="" CssClass="fontbold"> </asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="td20">DATE OF ISSUE
                                        </td>
                                        <td class="td70">
                                            <div id="divUTCtime"></div>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                    <div>
                        <hr />
                    </div>
                    <table class="billtb">
                        <tr>
                            <td class="td50 fontbold">BILL TO </td>
                            <td class="td50">
                                <asp:Label ID="lbllocation" runat="server" Text="" CssClass="fontbold"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="lblnicpp" runat="server" Text=""></asp:Label></td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="lblguestname" runat="server" Text=""></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="lblemail" runat="server" Text=""></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="lblcontatcno" runat="server" Text=""></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td>Reservation Code :
                                <asp:Label ID="lblrescode" runat="server" Text="" CssClass="fontbold fontred"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td>Reserved Date :
                                <asp:Label ID="lblbookeddate" runat="server" Text="" CssClass="fontbold"></asp:Label>
                            </td>
                        </tr>
                    </table>
                    <div>
                        <table class="billtb">
                            <tr>
                                <td colspan="2" style="vertical-align: top">
                                    <asp:Literal ID="ltldetails" runat="server"></asp:Literal>
                                </td>
                            </tr>
                            <tr>

                                <td class="td50" style="vertical-align: top">
                                    <asp:Literal ID="ltlexroomopt" runat="server"></asp:Literal></td>
                                <td class="td50" style="vertical-align: top">
                                    <asp:Literal ID="ltlsummery" runat="server"></asp:Literal></td>
                            </tr>
                            <tr>
                                <td class="td70">
                                    <p>
                                        Days :
                                        <asp:Label ID="lbldays" runat="server" Text="" CssClass="fontbold"></asp:Label>
                                        Rooms :
                                        <asp:Label ID="lblrooms" runat="server" Text="" CssClass="fontbold"></asp:Label>
                                        Adults :
                                        <asp:Label ID="lbladults" runat="server" Text="" CssClass="fontbold"></asp:Label>
                                        Children :
                                        <asp:Label ID="lblchildren" runat="server" Text="" CssClass="fontbold"></asp:Label>
                                    </p>
                                    <p class="fontbold">Invoice Total </p>
                                    <p>
                                        <asp:Label ID="lblinvoicetotal" runat="server" Text="" CssClass="billtotamt"></asp:Label>
                                    </p>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <p style="vertical-align: middle">
                                        <asp:Image ID="imgpay" runat="server" ImageAlign="Middle" />
                                        <asp:Label ID="lblpaymode" runat="server" Text="" CssClass="fontbold fontred"></asp:Label>
                                    </p>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
                <asp:HiddenField ID="hfutctime" ClientIDMode="Static" runat="server" />
            </ContentTemplate>
        </asp:UpdatePanel>
    </form>
</body>
</html>