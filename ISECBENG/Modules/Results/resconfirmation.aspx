﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master/Reservation.Master" AutoEventWireup="true" CodeBehind="resconfirmation.aspx.cs" Inherits="ISECBENG.Modules.Results.resconfirmation" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        function openWindow() {
            var RESCODE = document.getElementById('<%= lblrescode.ClientID %>').innerText
            params = 'width=' + (screen.width - 200);
            params += ', height=' + (screen.height - 200);
            params += ', top=50, left=100';
            params += ', fullscreen=yes,toolbar=0,location=0, directories=0, status=0,location=no,menubar=0,resizable=no';

            window.open('/Modules/Results/popupbill.aspx?RESCODE=' + RESCODE, 'Display > Invoice ', params);
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="CP1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <div class="errorbanner">
                <asp:Label ID="lblerrmessage" runat="server" Visible="False"></asp:Label>
            </div>
            <div class="preloadcenter">
                <asp:UpdateProgress ID="UpdateProgress1" runat="server">
                    <ProgressTemplate>
                        <img src="../../Images/Icons/Loading.gif" alt="" />
                    </ProgressTemplate>
                </asp:UpdateProgress>
            </div>
            <div align="center">
                <div class="message-area">
                    <asp:Literal ID="ltlmessage" runat="server"></asp:Literal>
                    <div class="tdleft">
                        <table>
                            <tr>
                                <td class="td50 fontred fontbold">* Click on "Confirm Booking" to send notifications.<br />
                                    * Click on "view Incoice" to view your bill.
                                </td>
                                <td class="td50 tdright">
                                    <asp:Button ID="btnconfitm" runat="server" Text="Confirm Booking" CssClass="btn" OnClientClick="getclientDateTime()" OnClick="btnconfitm_Click" />
                                    &nbsp;<asp:Button ID="btnprint" runat="server" Text="View Invoice" CssClass="btn" OnClientClick="openWindow(); return false();" />
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>

            <div class="hideDiv">
                <div class="billdiv" id="guestinvoice" runat="server">
                    <table style="border-collapse: collapse; border: none; width: 100%; margin-bottom: 2%">
                        <tr>
                            <td style="width: 50%; vertical-align: top; font-weight: bold; font-size: 5em; color: #1090c3">
                                <asp:Label ID="lblinvoice" runat="server" Text="INVOICE"></asp:Label>
                            </td>
                            <td style="width: 50%; vertical-align: top; text-align: right" rowspan="2">
                                <span style="width: 50%; height: 50%">
                                    <asp:Image ID="imglogo" runat="server" /></span>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <table style="border-collapse: collapse; border: none; width: 100%; margin-bottom: 2%">
                                    <tr>
                                        <td style="width: 40%">INVOICE NUMBER
                                        </td>
                                        <td style="width: 60%; vertical-align: top; font-weight: bold">
                                            <asp:Label ID="lbltrno" runat="server" Text=""> </asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 40%">DATE
                                        </td>
                                        <td>
                                            <asp:Label ID="lblutcdatetime" runat="server" Text=""></asp:Label>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                    <div>
                        <hr style="border: 2px solid #1090c3" />
                    </div>
                    <table style="border-collapse: collapse; border: none; width: 90%; margin-bottom: 2%;">
                        <tr>
                            <td style="width: 50%; font: bold">BILL TO </td>
                            <td style="width: 50%; font-weight: bold">
                                <asp:Label ID="lbllocation" runat="server" Text=""></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="lblnicpp" runat="server" Text=""></asp:Label></td>
                            <td rowspan="6">
                                <table style="border-collapse: collapse; border: none; width: 100%; margin-bottom: 2%">
                                    <tr>
                                        <td style="width: 30%">Location</td>
                                        <td style="width: 50%">
                                            <asp:Label ID="lbllocaddress1" runat="server" Text=""></asp:Label></td>
                                    </tr>
                                    <tr>
                                        <td>Address</td>
                                        <td>
                                            <asp:Label ID="lbllocaddress2" runat="server" Text=""></asp:Label></td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td>
                                            <asp:Label ID="lbllocaddress3" runat="server" Text=""></asp:Label></td>
                                    </tr>
                                    <tr>
                                        <td>hotline</td>
                                        <td>
                                            <asp:Label ID="lbllochotline" runat="server" Text=""></asp:Label></td>
                                    </tr>
                                    <tr>
                                        <td>Email</td>
                                        <td>
                                            <asp:Label ID="lbllocemail" runat="server" Text=""></asp:Label></td>
                                    </tr>
                                    <tr>
                                        <td>Fax No.</td>
                                        <td>
                                            <asp:Label ID="lbllocfax" runat="server" Text=""></asp:Label></td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="lblguestname" runat="server" Text=""></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="lblemail" runat="server" Text=""></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="lblcontatcno" runat="server" Text=""></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td>Reservation Code :
                              <span style="font-weight: bold; color: red">
                                  <asp:Label ID="lblrescode" runat="server" Text=""></asp:Label></span>
                            </td>
                        </tr>
                        <tr>
                            <td>Reserved Date :
                                <span style="font-weight: bold">
                                    <asp:Label ID="lblbookeddate" runat="server" Text=""></asp:Label></span>
                            </td>
                        </tr>
                    </table>
                    <div>
                        <table style="border-collapse: collapse; border: none; width: 100%; margin-bottom: 2%">
                            <tr>
                                <td colspan="2" style="vertical-align: top">
                                    <asp:Literal ID="ltldetails" runat="server"></asp:Literal>
                                </td>
                            </tr>
                            <tr>

                                <td style="width: 50%; vertical-align: top">
                                    <asp:Literal ID="ltlexroomopt" runat="server"></asp:Literal></td>
                                <td style="width: 50%; vertical-align: top">
                                    <asp:Literal ID="ltlsummery" runat="server"></asp:Literal></td>
                            </tr>
                            <tr>
                                <td style="width: 50%">

                                    <p>
                                        Days :
                                        <span style="font-weight: bold">
                                            <asp:Label ID="lbldays" runat="server" Text=""></asp:Label></span>
                                        Rooms :
                                        <span style="font-weight: bold">
                                            <asp:Label ID="lblrooms" runat="server" Text=""></asp:Label></span>
                                        Adults :
                                        <span style="font-weight: bold">
                                            <asp:Label ID="lbladults" runat="server" Text=""></asp:Label></span>
                                        Children :
                                        <span style="font-weight: bold">
                                            <asp:Label ID="lblchildren" runat="server" Text=""></asp:Label></span>
                                    </p>
                                    <p style="font-weight: bold">Invoice Total </p>
                                    <p style="font-size: 3em; color: #1090c3">
                                        <asp:Label ID="lblinvoicetotal" runat="server" Text="" CssClass="billtotamt"></asp:Label>
                                    </p>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <p style="vertical-align: middle">
                                        <asp:Image ID="imgpay" runat="server" ImageAlign="Middle" />
                                        <span style="font-weight: bold; color: red">
                                            <asp:Label ID="lblpaymode" runat="server" Text=""></asp:Label></span>
                                    </p>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>

            <div class="hideDiv">
                <div class="billdiv" style="border: 5px solid #ff6a00; padding: 0px" id="resconfirm" runat="server">
                    <table style="border-collapse: collapse; border: none; width: 100%; margin-bottom: 2%">
                        <tr>
                            <td style="text-align: center; background-color: #04254d; padding: 2%">
                                <span style="text-align: center; font-size: 3.0em; font-weight: bold; text-align: center; color: #ffffff">RESERVATION INFORMATION</span>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Image ID="imgtop" runat="server" CssClass="resimage" Width="100%" />
                            </td>
                        </tr>
                    </table>
                    <table style="border-collapse: collapse; border: none; width: 100%; padding-right: 0px">
                        <tr>
                            <td style="width: auto; max-width: 30%;">
                                <asp:Panel ID="pnlleftimages" runat="server"></asp:Panel>
                            </td>
                            <td style="width: 70%">
                                <table style="border-collapse: collapse; border: none; width: 100%; margin-bottom: 2%">
                                    <tr>
                                        <td style="padding: 2%; padding-bottom: 0px">
                                            <p style="font-weight: bold" id="idguestname" runat="server"></p>
                                            <p>
                                                <span id="idrescon" runat="server"></span>
                                            </p>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="padding: 2%; padding-bottom: 0px">
                                            <table>
                                                <tr>
                                                    <td style="width: 30%">Confirmation Number</td>
                                                    <td style="width: 30%; font-weight: bold; color: red"><span id="idrescode" runat="server"></span></td>
                                                </tr>
                                                <tr>
                                                    <td>Guest Name</td>
                                                    <td><span id="idguest" runat="server"></span></td>
                                                </tr>
                                                <tr>
                                                    <td>Arrival Date</td>
                                                    <td><span id="idarrival" runat="server"></span></td>
                                                </tr>
                                                <tr>
                                                    <td>Departure Date</td>
                                                    <td><span id="iddepature" runat="server"></span></td>
                                                </tr>
                                                <tr>
                                                    <td>Number of Days</td>
                                                    <td><span id="iddays" runat="server"></span></td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td style="padding: 2%; padding-bottom: 0px;">
                                            <p style="font-weight: bold">
                                                We are eagerly anticipating your arrival and would like to advise you of the following in order to help you with your trip planning:
                                            </p>
                                        </td>
                                    </tr>
                                </table>
                                <table style="border-collapse: collapse; border: none; width: 96%; margin: 2%; padding: 2%">
                                    <tr>
                                        <td colspan="2" style="font-weight: bold; padding-bottom: 2%;">HOTEL POLICIES
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 20%">Check-in time:
                                        </td>
                                        <td style="width: 80%">
                                            <span id="idcheckintime" runat="server"></span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Check-Out time:
                                        </td>
                                        <td>
                                            <span id="idcheckouttime" runat="server"></span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Payment Cancellation:
                                        </td>
                                        <td>
                                            <span id="idcancellation" runat="server"></span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Age Restrictions:
                                        </td>
                                        <td>
                                            <span id="idagerestrction" runat="server"></span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Pets:
                                        </td>
                                        <td>
                                            <span id="idpets" runat="server"></span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Smoking:
                                        </td>
                                        <td>
                                            <span id="idsmoking" runat="server"></span>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <td style="width: auto; max-width: 30%;">
                                <asp:Panel ID="pnlrightimages" runat="server"></asp:Panel>
                            </td>
                        </tr>
                    </table>
                    <table style="border-collapse: collapse; border: none; width: 100%; margin-bottom: 2%">
                        <tr>
                            <td>
                                <asp:Image ID="imgbottom" runat="server" CssClass="resimage" Width="100%" />
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
            <asp:HiddenField ID="hfrescode" runat="server" />
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>