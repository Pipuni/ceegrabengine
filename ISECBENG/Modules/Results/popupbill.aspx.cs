﻿using BusinessLogicDataHandler.Reservation;
using ConstantsDataHandler;
using System;
using System.Data;

namespace ISECBENG.Modules.Results
{
    public partial class popupbill : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                string RESCODE = Request.QueryString["RESCODE"].ToString();
                gerReservationDetails(RESCODE);
            }
        }

        private void gerReservationDetails(string RESCODE)
        {
            ReservationDataHandler eReservationDataHandler = new ReservationDataHandler();
            DataTable dtroom = new DataTable();
            ltldetails.Text = "";
            ltlsummery.Text = "";

            try
            {
                DataRow dtres = eReservationDataHandler.getReservationDetails(RESCODE);
                if (dtres != null)
                {
                    lblrescode.Text = RESCODE.ToString();
                    lbllocation.Text = dtres["LOCATION"].ToString();
                    lbllocaddress1.Text = dtres["LOCADDRESS1"].ToString();
                    lbllocaddress2.Text = dtres["LOCADDRESS2"].ToString();
                    lbllocaddress3.Text = dtres["LOCADDRESS3"].ToString();
                    lbllochotline.Text = dtres["HOTLINE"].ToString();
                    lbllocfax.Text = dtres["FAX"].ToString();
                    lbllocemail.Text = dtres["LOEMAIL"].ToString();
                    lbltrno.Text = dtres["TRNO"].ToString();
                    lblcontatcno.Text = dtres["CONTACTNO"].ToString();
                    lblemail.Text = dtres["EMAIL"].ToString();
                    lblnicpp.Text = dtres["NICPP"].ToString();
                    lblguestname.Text = dtres["GUESTNAME"].ToString();
                    lblbookeddate.Text = dtres["LOCDATE"].ToString();
                    lbladults.Text = dtres["ADULTS"].ToString();
                    lblchildren.Text = dtres["CHILDREN"].ToString();
                    lblrooms.Text = dtres["NOOFROOMS"].ToString();
                    lbldays.Text = dtres["NOOFDAYS"].ToString();
                    lblpaymode.Text = dtres["PAYMENTMODE"].ToString();
                    double GROSSAMT = double.Parse(dtres["GROSSAMT"].ToString());
                    double TAXAMT = double.Parse(dtres["TAXAMT"].ToString());
                    double NETAMT = double.Parse(dtres["NETAMT"].ToString());
                    string PREFCURRENCY = dtres["PREFCURRENCY"].ToString();
                    lblinvoicetotal.Text = PREFCURRENCY + " " + Math.Round(NETAMT).ToString("#,##0.00");
                    //imglogo.ImageUrl = (string)Session[Constants.SYS_SESSION_COMPANYLOGO];
                    byte[] ICONFILE = (byte[])(dtres["ICONFILE"]);
                    imgpay.ImageUrl = String.Format("data:image/jpg;base64,{0}", Convert.ToBase64String(ICONFILE));
                    int NOOFDAYS = int.Parse(dtres["NOOFDAYS"].ToString());

                    ltldetails.Text = eReservationDataHandler.ViewResdetailsForInvoice(RESCODE, NOOFDAYS, PREFCURRENCY);
                    ltlexroomopt.Text = eReservationDataHandler.ViewExoptionsForInvoice(RESCODE, PREFCURRENCY);
                    ltlsummery.Text = eReservationDataHandler.ViewSummaryForInvoice(RESCODE, GROSSAMT, TAXAMT, NETAMT, PREFCURRENCY); ;
                }
            }
            catch (Exception ex)
            {
                Constants.CON_MESSAGE_STRING = ex.Message;
                SysErrors.AppError.PopulateError(Constants.MESSAGE_WARNING, Constants.CON_MESSAGE_STRING, lblerrmessage);
            }
            finally
            {
                eReservationDataHandler = null;
            }
        }
    }
}