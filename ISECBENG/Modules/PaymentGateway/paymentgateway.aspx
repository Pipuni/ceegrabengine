﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master/PaymentGateway.Master" AutoEventWireup="true" CodeBehind="paymentgateway.aspx.cs" Inherits="ISECBENG.Modules.PaymentGateway.paymentgateway" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="CP1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <div class="errorbanner">
                <asp:Label ID="lblerrmessage" runat="server" Visible="False"></asp:Label>
            </div>
            <div>
                <asp:ValidationSummary ID="vssummary" runat="server" ValidationGroup="vgsum" CssClass="valsummary" />
            </div>
            <div class="preloadcenter">
                <asp:UpdateProgress ID="UpdateProgress1" runat="server">
                    <ProgressTemplate>
                        <img src="../../Images/Icons/Loading.gif" alt="" />
                    </ProgressTemplate>
                </asp:UpdateProgress>
            </div>

            <div class="carddetails-area">
                <table class="paymentdetails">
                    <tr>
                        <td>
                            <asp:Label ID="lblpay" runat="server" Text="Payment Details"></asp:Label>
                        </td>
                        <td>
                            <asp:Label ID="lblamount" runat="server" Text="15,500.00"></asp:Label>
                            <asp:Label ID="lblcurrency" runat="server" Text="LKR"></asp:Label>
                        </td>
                </table>
                <table class="carddetails">
                    <tr>
                        <td colspan="2" class="td30">Card Type
                        </td>
                        <td>
                            <asp:TextBox ID="txtcardtype" runat="server" Width="100%" CssClass="inputtextbox" placeholder="Card Type" />
                        </td>
                        <td>
                            <asp:RequiredFieldValidator ID="rfcardtype" runat="server" ErrorMessage="Card type is invalid."
                                ControlToValidate="txtcardtype" ForeColor="Red" ValidationGroup="vgsum">*</asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">Name on Card
                        </td>
                        <td>
                            <asp:TextBox ID="txtName" runat="server" Width="100%" CssClass="inputtextbox" placeholder="Card Holder Name" />
                        </td>
                        <td>
                            <asp:RequiredFieldValidator ID="rfname" runat="server" ErrorMessage="Card holder name is invalid."
                                ControlToValidate="txtName" ForeColor="Red" ValidationGroup="vgsum">*</asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">Card Number
                        </td>
                        <td>
                            <table style="width: 100%; text-align: left; border-collapse: collapse">
                                <tr>
                                    <td>
                                        <asp:TextBox ID="txtCardNumber1" runat="server" Width="90%" CssClass="inputtextbox" MaxLength="4" placeholder="0000" />
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtCardNumber2" runat="server" Width="90%" CssClass="inputtextbox" MaxLength="4" placeholder="0000" />
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtCardNumber3" runat="server" Width="90%" CssClass="inputtextbox" MaxLength="4" placeholder="0000" />
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtCardNumber4" runat="server" Width="90%" CssClass="inputtextbox" MaxLength="4" placeholder="0000" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td>
                            <asp:RequiredFieldValidator ID="rfcardnumber" runat="server" ErrorMessage="Card number is invalid."
                                ControlToValidate="txtCardNumber1" ForeColor="Red" ValidationGroup="vgsum">*</asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">Card Expiry Date
                        </td>
                        <td>
                            <table style="width: 50%; text-align: left; border-collapse: collapse">
                                <tr>
                                    <td>
                                        <asp:DropDownList ID="ddlmonth" runat="server" CssClass="inputtextbox"></asp:DropDownList>
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="ddlyear" runat="server" CssClass="inputtextbox"></asp:DropDownList>
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td>
                            <asp:RequiredFieldValidator ID="rfexpdatee" runat="server" ErrorMessage="Expiry date is invalid."
                                ControlToValidate="ddlmonth" ForeColor="Red" ValidationGroup="vgsum">*</asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">Card CVC
                        </td>
                        <td>
                            <asp:TextBox ID="txtCVC" runat="server" CssClass="inputtextbox" Width="20%" MaxLength="3" placeholder="CVC" />
                        </td>
                        <td>
                            <asp:RequiredFieldValidator ID="rfcvc" runat="server" ErrorMessage="CVC is invalid."
                                ControlToValidate="txtCVC" ForeColor="Red" ValidationGroup="vgsum">*</asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4"></td>
                        <table style="text-align: center; width: 100%">
                            <tr>
                                <td>
                                    <span>
                                        <img src="../../Images/Icons/Amazon-icon.png" />
                                        <img src="../../Images/Icons/American-Express-icon.png" />
                                        <img src="../../Images/Icons/Google-Wallet-icon.png" />
                                        <img src="../../Images/Icons/Master-Card-icon.png" />
                                        <img src="../../Images/Icons/Paypal-icon.png" />
                                    </span>
                                </td>
                                <td class="td30">
                                    <asp:Button ID="Button_ProceedPay" runat="server" Text="Proceed Payment" CssClass="btn" ValidationGroup="vgsum" OnClientClick="return validate()" OnClick="btnproceed_Click" />
                                </td>
                            </tr>
                        </table>
                        </td>
                    </tr>
                </table>
            </div>
            <asp:HiddenField ID="hfcurrcode" runat="server" />
            <asp:HiddenField ID="hfpaymode" runat="server" />
            <asp:HiddenField ID="hfrescode" runat="server" />
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>