﻿using BusinessLogicDataHandler.PaymentGateway;
using BusinessLogicDataHandler.Reservation;
using ConstantsDataHandler;
using System;
using System.Xml;

namespace ISECBENG.Modules.PaymentGateway.NTBPaymentGateway
{
    public partial class ntbresponse : System.Web.UI.Page
    {
        private string PTReceipt = "";
        private int intResult = 0;
        private string strErrMsg = "";

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.Form.Count > 0)
            {
                if ((Request.Form["encryptedReceiptPay"] != null) && (Request.Form["encryptedReceiptPay"].Length > 0))
                    DecrypteData(Request.Form["encryptedReceiptPay"]);
            }
        }

        private void DecrypteData(string strEncryptedReceipt)
        {
            try
            {
                //IShroff objIShroff = new IShroff();
                IShroff objIShroff = new IShroff();

                //Un comment and edit below 2 lines if your security keys are present in a custom folder
                //other than <iPay Client Service>\keys

                //string strKeySet_1 = WebConfigurationManager.AppSettings["IPGKeyPath_1"];
                //objIShroff.setSecurityKeysPath(strKeySet_1);

                bool bResult = objIShroff.setEncryptedReceipt(strEncryptedReceipt);
                if (bResult)
                {
                    PTReceipt = objIShroff.getPlainTextReceipt();
                    DisplayInoviceDetails(PTReceipt);

                    //encryptedReceipt.Text = strEncryptedReceipt;
                    //txtPlainReceipt.Text = PTReceipt;
                }
                else
                {
                    Response.Write("Error Code:" + objIShroff.getErrorCode());
                    Response.Write("<BR> Error Description :" + objIShroff.getErrorMessage());
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        private void DisplayInoviceDetails(string strReceiptDetail)
        {
            //string strhtml = "<table width=600>";
            if (intResult < 0)
            {
                //strhtml += "<tr>";
                //strhtml += "<td width=250>Error Code</td>";
                //strhtml += "<td width=5>:</td>";
                //strhtml += "<td width=350>" + intResult.ToString() + "</td>";
                //strhtml += "</tr>";
                //strhtml += "<tr>";
                //strhtml += "<td width=250>Error Message</td>";
                //strhtml += "<td width=5>:</td>";
                //strhtml += "<td width=350>" + strErrMsg + "</td>";
                //strhtml += "</tr>";
                Session[Constants.SYS_SESSION_ERROR] = intResult.ToString() + strErrMsg + Environment.NewLine + " Unable to complete the reservation. Please contact hotel management.";
                Response.Redirect("~/Modules/Results/errorpage.aspx", false);
            }
            else
            {
                string action = "";
                string maskedaccno = "";
                string bankrefid = "";
                string currcode = "";
                string IPGtransacid = "";
                string langcode = "";
                string merchantrefid = "";
                string merchanvar1 = "";
                string merchanvar2 = "";
                string merchanvar3 = "";
                string merchanvar4 = "";
                string transactionamt = "";
                string transactionsts = "";
                string failedreason = "";
                string cardholname = "";

                XmlDocument xmlDocx = new XmlDocument();

                xmlDocx.LoadXml(strReceiptDetail);

                XmlNodeList oNL = xmlDocx.FirstChild.ChildNodes;

                foreach (XmlNode oNode in oNL)
                {
                    switch (oNode.Name)
                    {
                        case "action":

                            //strhtml += "<tr>";
                            //strhtml += "<td width=250>Value of 'action' tag</td>";
                            //strhtml += "<td width=5>:</td>";
                            //strhtml += "<td width=350>" + oNode.InnerText + "</td>";
                            //strhtml += "</tr>";
                            action = oNode.InnerText;

                            break;

                        case "acc_no":

                            //strhtml += "<tr>";
                            //strhtml += "<td width=250>Masked Account Number</td>";
                            //strhtml += "<td width=5>:</td>";
                            //strhtml += "<td width=350>" + oNode.InnerText + "</td>";
                            //strhtml += "</tr>";
                            maskedaccno = oNode.InnerText;

                            break;

                        case "bank_ref_id":
                            //strhtml += "<tr>";
                            //strhtml += "<td width=250>Bank Reference Id</td>";
                            //strhtml += "<td width=5>:</td>";
                            //strhtml += "<td width=350>" + oNode.InnerText + "</td>";
                            //strhtml += "</tr>";
                            bankrefid = oNode.InnerText;
                            break;

                        case "cur":
                            //strhtml += "<tr>";
                            //strhtml += "<td width=250>Currency Code</td>";
                            //strhtml += "<td width=5>:</td>";
                            //strhtml += "<td width=350>" + oNode.InnerText + "</td>";
                            //strhtml += "</tr>";
                            currcode = oNode.InnerText;
                            break;

                        case "ipg_txn_id":
                            //strhtml += "<tr>";
                            //strhtml += "<td width=250>IPG Transaction ID</td>";
                            //strhtml += "<td width=5>:</td>";
                            //strhtml += "<td width=350>" + oNode.InnerText + "</td>";
                            //strhtml += "</tr>";
                            IPGtransacid = oNode.InnerText;
                            break;

                        case "lang":
                            //strhtml += "<tr>";
                            //strhtml += "<td width=250>Language Code</td>";
                            //strhtml += "<td width=5>:</td>";
                            //strhtml += "<td width=350>" + oNode.InnerText + "</td>";
                            //strhtml += "</tr>";
                            langcode = oNode.InnerText;
                            break;

                        case "mer_txn_id":
                            //strhtml += "<tr>";
                            //strhtml += "<td width=250>Merchant Reference ID</td>";
                            //strhtml += "<td width=5>:</td>";
                            //strhtml += "<td width=350>" + oNode.InnerText + "</td>";
                            //strhtml += "</tr>";
                            merchantrefid = oNode.InnerText;
                            break;

                        case "mer_var1":
                            //strhtml += "<tr>";
                            //strhtml += "<td width=250>Merchant Variable 1</td>";
                            //strhtml += "<td width=5>:</td>";
                            //strhtml += "<td width=350>" + oNode.InnerText + "</td>";
                            //strhtml += "</tr>";
                            merchanvar1 = oNode.InnerText;
                            break;

                        case "mer_var2":
                            //strhtml += "<tr>";
                            //strhtml += "<td width=250>Merchant Variable 2</td>";
                            //strhtml += "<td width=5>:</td>";
                            //strhtml += "<td width=350>" + oNode.InnerText + "</td>";
                            //strhtml += "</tr>";
                            merchanvar2 = oNode.InnerText;
                            break;

                        case "mer_var3":
                            //strhtml += "<tr>";
                            //strhtml += "<td width=250>Merchant Variable 3</td>";
                            //strhtml += "<td width=5>:</td>";
                            //strhtml += "<td width=350>" + oNode.InnerText + "</td>";
                            //strhtml += "</tr>";
                            merchanvar3 = oNode.InnerText;
                            break;

                        case "mer_var4":
                            //strhtml += "<tr>";
                            //strhtml += "<td width=250>Merchant Variable 4</td>";
                            //strhtml += "<td width=5>:</td>";
                            //strhtml += "<td width=350>" + oNode.InnerText + "</td>";
                            //strhtml += "</tr>";
                            merchanvar4 = oNode.InnerText;
                            break;

                        case "name":
                            //strhtml += "<tr>";
                            //strhtml += "<td width=250>Card Holder's Name</td>";
                            //strhtml += "<td width=5>:</td>";
                            //strhtml += "<td width=350>" + oNode.InnerText + "</td>";
                            //strhtml += "</tr>";
                            cardholname = oNode.InnerText;
                            break;

                        case "reason":
                            //strhtml += "<tr>";
                            //strhtml += "<td width=250>Failed Reason</td>";
                            //strhtml += "<td width=5>:</td>";
                            //strhtml += "<td width=350>" + oNode.InnerText + "</td>";
                            //strhtml += "</tr>";
                            failedreason = oNode.InnerText;
                            break;

                        case "txn_amt":
                            //strhtml += "<tr>";
                            //strhtml += "<td width=250>Transaction Amount</td>";
                            //strhtml += "<td width=5>:</td>";
                            //strhtml += "<td width=350>" + oNode.InnerText + "</td>";
                            //strhtml += "</tr>";
                            transactionamt = oNode.InnerText;
                            break;

                        case "txn_status":
                            //strhtml += "<tr>";
                            //strhtml += "<td width=250>Transaction Status</td>";
                            //strhtml += "<td width=5>:</td>";
                            //strhtml += "<td width=350>" + oNode.InnerText + "</td>";
                            //strhtml += "</tr>";
                            transactionsts = oNode.InnerText;
                            break;
                    }
                }

                PaymentGatewayDataHandler paymentGatewayDataHandler = new PaymentGatewayDataHandler();

                Boolean iscreate = paymentGatewayDataHandler.CreateResResponse(merchantrefid, action, maskedaccno, bankrefid, currcode, IPGtransacid, langcode, merchantrefid,
             merchanvar1, merchanvar2, merchanvar3, merchanvar4, transactionamt, transactionsts, failedreason, cardholname);

                if (iscreate)
                {
                    if (transactionsts.Equals("ACCEPTED"))
                    {
                        ReservationDataHandler eReservationDataHandler = new ReservationDataHandler();
                        string enRESCODE = eReservationDataHandler.EnryptString(merchantrefid);
                        Response.Redirect("~/Modules/Results/resconfirmation.aspx?RESCODE=" + enRESCODE.ToString(), false);
                    }
                    else
                    {
                        Session[Constants.SYS_SESSION_ERROR] = failedreason + Environment.NewLine + " Unable to complete the reservation. Please contact hotel management.";
                        Response.Redirect("~/Modules/Results/errorpage.aspx?RESCODE=" + merchantrefid.ToString(), false);
                    }
                }
            }
            //strhtml += "</table>";

            //tdResult.InnerHtml = strhtml;
        }
    }
}