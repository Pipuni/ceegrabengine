﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ntbreqmessage.aspx.cs" Inherits="ISECBENG.Modules.PaymentGateway.NTBPaymentGateway.ntbreqmessage" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <script src="../../../Scripts/jquery.min.js" type="text/javascript"></script>
</head>
<body>
    <form id="formEncrypted" method="post" action="<%= this.GatewayUrl %>">
        <script type="text/javascript">
                            $(function () {
                                $('#formEncrypted').submit();
                            });
        </script>
    </form>
</body>
</html>