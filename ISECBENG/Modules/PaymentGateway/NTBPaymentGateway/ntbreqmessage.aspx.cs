﻿using BusinessLogicDataHandler.PaymentGateway;
using BusinessLogicDataHandler.Reservation;
using ConstantsDataHandler;
using System;
using System.Configuration;
using System.Data;

namespace ISECBENG.Modules.PaymentGateway.NTBPaymentGateway
{
    public partial class ntbreqmessage : System.Web.UI.Page
    {
        public string GatewayUrl = "";

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                string RESCODE = Request.QueryString["RESCODE"].ToString();
                string PAYMENTCODE = Request.QueryString["PAYMENTCODE"].ToString();
                string TRANSACTIONAMOUNT = Request.QueryString["TRANSACTIONAMOUNT"].ToString();
                merchantDetails(RESCODE, PAYMENTCODE, TRANSACTIONAMOUNT);
            }
        }

        private void merchantDetails(string RESCODE, string PAYMENTCODE, string TRANSACTIONAMOUNT)
        {
            try
            {
                string COMPCODE = (string)Session[Constants.SYS_SESSION_COMPANYCODE];
                string LOCCODE = (string)Session[Constants.SYS_SESSION_LOCATIONCODE];
                string STS = Constants.CON_STATUS_ACTIVE;
                PaymentGatewayDataHandler paymentGatewayDataHandler = new PaymentGatewayDataHandler();
                ReservationDataHandler eReservationDataHandler = new ReservationDataHandler();

                DataRow merchantrw = paymentGatewayDataHandler.getmerchantdetails(COMPCODE, LOCCODE, STS, PAYMENTCODE);
                if (merchantrw != null)
                {
                    string MERCHANTID = merchantrw["MERCHANTID"].ToString();
                    string CURRCODE = merchantrw["CURRCODE"].ToString();
                    string LANGCODE = "eng";
                    NTBPaymentGatewayDataHandler nTBPaymentGatewayDataHandler = new NTBPaymentGatewayDataHandler(Constants.CON_ITRANS_TYPE, MERCHANTID, decimal.Parse(TRANSACTIONAMOUNT), CURRCODE, RESCODE, LANGCODE
                      , "/Modules/PaymentGateway/NTBPaymentGateway/ntbresponse.aspx", RESCODE, "", "", "",
                      ConfigurationManager.AppSettings["IPGClientService_IP"], ConfigurationManager.AppSettings["IPGClientService_Port"]);
                    string uri = nTBPaymentGatewayDataHandler.EncryptData();
                    GatewayUrl = ConfigurationManager.AppSettings["IPGServer"] + uri;
                }
            }
            catch (Exception ex)
            {
                Session[Constants.SYS_SESSION_ERROR] = ex.Message.ToString() + Environment.NewLine + " Unable to complete the reservation. Please contact hotel management.";
                Response.Redirect("~/Modules/Results/errorpage.aspx?RESCODE=" + RESCODE.ToString(), false);
            }
        }
    }
}