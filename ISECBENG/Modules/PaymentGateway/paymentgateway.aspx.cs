﻿using BusinessLogicDataHandler.PaymentGateway;
using ConstantsDataHandler;
using System;
using System.Collections.Generic;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ISECBENG.Modules.PaymentGateway
{
    public partial class paymentgateway : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                //string RESCODE = Request.QueryString["RESCODE"].ToString();
                //string PAYMODE = Request.QueryString["PAYMODE"].ToString();
                //string CURRCODE = Request.QueryString["CURRCODE"].ToString();
                //hfrescode.Value = RESCODE.ToString();
                //hfpaymode.Value = RESCODE.ToString();
                //hfcurrcode.Value = RESCODE.ToString();
                getYearnMonth();
                populateCompanyName();
            }
        }

        private void getYearnMonth()
        {
            PaymentGatewayDataHandler ePaymentGatewayDataHandler = new PaymentGatewayDataHandler();

            try
            {
                Dictionary<string, string> yearlist = new Dictionary<string, string>();
                yearlist = ePaymentGatewayDataHandler.getYears();
                ddlyear.DataSource = yearlist;
                ddlyear.DataTextField = "Key";
                ddlyear.DataValueField = "Value";
                ddlyear.DataBind();

                Dictionary<string, string> monthlist = new Dictionary<string, string>();
                monthlist = ePaymentGatewayDataHandler.getMonths();
                ddlmonth.DataSource = monthlist;
                ddlmonth.DataTextField = "Key";
                ddlmonth.DataValueField = "Value";
                ddlmonth.DataBind();
            }
            catch (Exception ex)
            {
                Constants.CON_MESSAGE_STRING = ex.Message;
                SysErrors.AppError.PopulateError(Constants.MESSAGE_WARNING, Constants.CON_MESSAGE_STRING, lblerrmessage);
            }
            finally
            {
                ePaymentGatewayDataHandler = null;
            }
        }

        private void populateCompanyName()
        {
            PaymentGatewayDataHandler paymentGatewayDataHandler = new PaymentGatewayDataHandler();

            try
            {
                // method will use the sessions and get the company code to query and display the company name
                string companyCode = (string)Session[Constants.SYS_SESSION_COMPANYCODE];
                if (companyCode != null)
                {
                    DataRow dataRow = paymentGatewayDataHandler.getcompany(companyCode, Constants.CON_STATUS_ACTIVE);
                    if (dataRow != null)
                    {
                        //Label_CompanyName.Text = "Welcome to " + dataRow["COMPNAME"].ToString();
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                paymentGatewayDataHandler = null;
            }
        }

        protected void btnproceed_Click(object sender, EventArgs e)
        {
            PaymentGatewayDataHandler paymentGatewayDataHandler = new PaymentGatewayDataHandler();

            try
            {
                string TRNO = Request.QueryString["trno"].ToString();
                string CARDNUMBER = txtCardNumber1.Text.ToString();
                string CARDHOLDERNAME = txtName.Text.ToString();
                string CARDEXPIRY = ddlmonth.SelectedItem.Value.ToString();
                string CARDCVC = txtCVC.Text.ToString();
                HiddenField UTCtime = (HiddenField)Page.Master.FindControl("hfutctime");
                string LOCDATE = UTCtime.Value.ToString();
                string STS = Constants.CON_STATUS_ACTIVE;

                Boolean iscreate = paymentGatewayDataHandler.createCardDetail(TRNO, CARDNUMBER, CARDHOLDERNAME, CARDEXPIRY, CARDCVC, LOCDATE, STS);
                if (iscreate)
                {
                    // Send the detail to the bank using APIs before displaying any success message

                    Constants.CON_MESSAGE_STRING = "Payment Accepted";
                    SysErrors.AppError.PopulateError(Constants.MESSAGE_SUCCESS, Constants.CON_MESSAGE_STRING, lblerrmessage);
                }
                else
                {
                    Constants.CON_MESSAGE_STRING = "An error occurred while processing the transaction. Please contact system admin";
                    SysErrors.AppError.PopulateError(Constants.MESSAGE_WARNING, Constants.CON_MESSAGE_STRING, lblerrmessage);
                }
            }
            catch (Exception ex)
            {
                Constants.CON_MESSAGE_STRING = ex.Message;
                SysErrors.AppError.PopulateError(Constants.MESSAGE_WARNING, Constants.CON_MESSAGE_STRING, lblerrmessage);
            }
            finally
            {
                paymentGatewayDataHandler = null;
            }
        }
    }
}