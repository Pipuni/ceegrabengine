﻿using System.Web.UI;
using System.Web.UI.WebControls;

namespace ISECBENG.SysErrors
{
    public class AppError
    {
        public static void ClearError(Control control)
        {
            Label lblerr = null;

            if (control.GetType() == typeof(Label))
            {
                lblerr = (Label)control;
                lblerr.Visible = false;
                lblerr.Text = "";
            }
        }

        public static void PopulateError(string ErrorNature, string ErrorText, Control control)
        {
            Label lblerr = null;
            if (control.GetType() == typeof(Label))
            {
                lblerr = (Label)control;

                if (ErrorNature == "0")
                {
                    lblerr.Style.Add(HtmlTextWriterStyle.BackgroundImage, "/Images/Error/error.png");
                    lblerr.Text = ErrorText;
                    lblerr.CssClass = "error";
                    lblerr.Visible = true;
                }
                else if (ErrorNature == "1")
                {
                    lblerr.Text = ErrorText;
                    lblerr.Style.Add(HtmlTextWriterStyle.BackgroundImage, "/Images/Error/success.png");
                    lblerr.CssClass = "success";
                    lblerr.Visible = true;
                }
                else if (ErrorNature == "2")
                {
                    lblerr.Text = ErrorText;
                    lblerr.Style.Add(HtmlTextWriterStyle.BackgroundImage, "/Images/Error/info.png");
                    lblerr.CssClass = "info";
                    lblerr.Visible = true;
                }
                else if (ErrorNature == "3")
                {
                    lblerr.CssClass = "warning";
                    lblerr.Text = ErrorText;
                    lblerr.Style.Add(HtmlTextWriterStyle.BackgroundImage, "/Images/Error/warning.png");
                    lblerr.Visible = true;
                }
            }
        }
    }
}